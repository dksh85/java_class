import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import model.vo.UserVO;

public class JsonMain {

	public static void main(String[] args) {
//		jsonObj();
		jsonAry();
	}
	public static void jsonObj(){
		UserVO user = new UserVO("kdn", "kdn", "한전KDB");
		JSONObject jObj = new JSONObject(user) ;
		System.out.println(jObj); // 이렇게 들어오는 걸 handling
	}
	public static void jsonAry(){
		ArrayList<UserVO> list = new ArrayList<UserVO>();
		list.add(new UserVO("kdn", "kdn", "한전 KDN"));
		list.add(new UserVO("jslim", "jslim", "섭섭해"));
		list.add(new UserVO("google", "go", "alpha-go"));
		
		JSONArray jAry = new JSONArray(list);
		System.out.println(jAry);
	}
}
