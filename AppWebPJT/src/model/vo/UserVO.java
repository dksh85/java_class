package model.vo;

public class UserVO {
	private String lat, lng, content;
	
	public UserVO(){
		super();
	}

	public UserVO(String lat, String lng, String content) {
		super();
		this.lat = lat;
		this.lng = lng;
		this.content = content;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
