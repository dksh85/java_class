package com.kdn.goods.dao;

import java.sql.*;
import java.util.List;

import com.kdn.goods.domain.Goods;
import com.kdn.goods.domain.PageBean;
public interface GoodsDao {
	/**
	 * 
	 * 상품정보를 DB에 저장하는 기능
	 * @param con
	 * @param goods 상품정보
	 * @throws SQLException
	 */
	
	public void add(Connection con , Goods goods) throws SQLException; 
	/**
	 * 상품번호(gno)에 해당하는 상품의 정보를 수정 
	 * 상품번호를 제외한 모든 정보를 수정
	 * @param con
	 * @param goods
	 * @throws SQLException
	 */
	public void update(Connection con , Goods goods) throws SQLException; 
	/**
	 * 상품번호에 해당하는 상품을 삭제하는 기능
	 * @param con
	 * @param gno	삭제할 상품 번호
	 * @throws SQLException
	 */
	public void remove(Connection con , String gno) throws SQLException; 
	/**
	 * 상품번호에 해당하는 상품을 찾아 상품정보를 반환
	 * @param con
	 * @param gno	찾을 상품 번호
	 * @return		찾은 상품 정보
	 * @throws SQLException
	 */
	public Goods search(Connection con , String gno) throws SQLException; 
	/**
	 * 조회조건에 따른 상품 목록을 반환하는 기능
	 * @param con
	 * @param bean	조회 조건 :	상품이름, 제조사, 상품 분류
	 * 						페이지 정보(페이지 번호) <br/>
	 * @return		조회 조건에 해당하는 상품 목록
	 * @throws SQLException
	 */
	public List<Goods> searchAll(Connection con , PageBean bean) throws SQLException; 
	/**
	 * 조회 조건에 따른 상품의 개수를 반환하는 기능
	 * @param con
	 * @param bean	조회 조건 :	상품이름, 제조사, 상품 분류
	 * 						페이지 정보(페이지 번호) <br/>
	 * @return		조회 조건에 해당하는 상품 개수
	 * @throws SQLException
	 */
	public int getCount(Connection con , PageBean bean) throws SQLException; 
	//서비스에서 sqlexception 을 해줘야 하기때문
}
