package com.kdn.goods.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kdn.goods.domain.Goods;
import com.kdn.goods.domain.PageBean;
import com.kdn.goods.util.DBUtil;

public class GoodsDaoImpl implements GoodsDao{
	/**
	 * 
	 * 상품정보를 DB에 저장하는 기능
	 * @param con
	 * @param goods 상품정보
	 * @throws SQLException
	 */
	
	public void add(Connection con , Goods goods) throws SQLException{ 
		PreparedStatement stmt = null;
		try{
			String sql = " insert into goods(gno, brand, price,maker, image, cno) " 
					+ "values( ?,?,?,?,?,?)";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, Integer.parseInt(goods.getGno()));
			stmt.setString(2, goods.getBrand());
			stmt.setInt(3, goods.getPrice());
			stmt.setString(4, goods.getMaker());
			stmt.setString(5, goods.getImage());
			stmt.setString(6, goods.getCno());
			stmt.executeQuery();
		}finally{
			DBUtil.close(stmt);
		}
	}
	/**
	 * 상품번호(gno)에 해당하는 상품의 정보를 수정 
	 * 상품번호를 제외한 모든 정보를 수정
	 * @param con
	 * @param goods
	 * @throws SQLException
	 */
	public void update(Connection con , Goods goods) throws SQLException{ 
		PreparedStatement stmt = null;
		try{
			String sql = " update goods set brand=?, price=?, maker=?, image=?, cno=? "
					+ " where gno=?	";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, goods.getBrand());
			stmt.setInt(2, goods.getPrice());
			stmt.setString(3, goods.getMaker());
			stmt.setString(4, goods.getImage());
			stmt.setString(5, goods.getCno());
			stmt.setInt(6, Integer.parseInt(goods.getGno()));
			stmt.executeUpdate();
		}finally{
			DBUtil.close(stmt);
		}
	}
	/**
	 * 상품번호에 해당하는 상품을 삭제하는 기능
	 * @param con
	 * @param gno	삭제할 상품 번호
	 * @throws SQLException
	 */
	public void remove(Connection con , String gno) throws SQLException{ 
		PreparedStatement stmt = null;
		try{
			String sql = " delete from goods where gno=? ";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, Integer.parseInt(gno));
			stmt.executeUpdate();
		}finally{
			DBUtil.close(stmt);
		}
	}
	/**
	 * 상품번호에 해당하는 상품을 찾아 상품정보를 반환
	 * @param con
	 * @param gno	찾을 상품 번호
	 * @return		찾은 상품 정보
	 * @throws SQLException
	 */
	public Goods search(Connection con , String gno) throws SQLException{ 
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try{
			String sql = " select * from goods where gno=? ";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, Integer.parseInt(gno));
			rs = stmt.executeQuery();
			if (rs.next()){
				return new Goods(gno, rs.getString("brand"), rs.getInt("price")
						, rs.getString("maker"), rs.getString("image"), rs.getString("cno"));
			}
		}finally{
			DBUtil.close(rs);
			DBUtil.close(stmt);
		}
		return null;
	}
	/**
	 * 조회조건에 따른 상품 목록을 반환하는 기능
	 * @param con
	 * @param bean	조회 조건 :	상품이름, 제조사, 상품 분류
	 * 						페이지 정보(페이지 번호) <br/>
	 * @return		조회 조건에 해당하는 상품 목록
	 * @throws SQLException
	 */
	public List<Goods> searchAll(Connection con , PageBean bean) throws SQLException{ 
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try{
			String key = bean.getKey();
			String word = bean.getWord();
			StringBuilder sql = new StringBuilder(100);			
			sql.append("SELECT a.*											\n");
			sql.append("FROM (SELECT ROW_NUMBER() over(ORDER BY gno desc) ro, \n");
			sql.append(		"	gno, brand, price, image							\n");
			sql.append("	FROM GOODS									\n");
		    sql.append("   WHERE 1 = 1								\n");
			if ( key != null && !key.equals("all") && word != null && !word.equals("")){
				if( key.equals("brand")){
					sql.append(" 	and brand like ? 	");
				}
				else if( key.equals("maker")){
					sql.append("		and maker like ? 	");
				}
				else if (key.equals("cno")){
					sql.append("			and cno = ? 	");
				}
			}
			sql.append("        ) a 	\n");
			sql.append("		WHERE ro BETWEEN ? AND ? \n");
			
			System.out.println(sql);
			stmt = con.prepareStatement(sql.toString());
			int idx = 1;	//조건에 따라 ? 개수가 달라지므로 ?에 대한 index
			if ( key != null && !key.equals("all") && word != null && !word.trim().equals("")){
				if(key.equals("cno")){
					stmt.setInt(idx++, Integer.parseInt(word));
				}else{
					stmt.setString(idx++, "%"+word+"%");
				}
			}
			stmt.setInt(idx++, bean.getStart());
			stmt.setInt(idx++, bean.getEnd()); //조회할 페이지의 대한 번호
			rs = stmt.executeQuery();
			ArrayList<Goods> goods = new ArrayList<Goods>(bean.getInterval());
			while (rs.next()){
				goods.add(new Goods(rs.getString("gno")
								, rs.getString("brand")
								, rs.getInt("price")
								, rs.getString("image")));
			}
			return goods;
		}finally{
			DBUtil.close(stmt);
			DBUtil.close(rs);
		}
	}
	/**
	 * 조회 조건에 따른 상품의 개수를 반환하는 기능
	 * @param con
	 * @param bean	조회 조건 :	상품이름, 제조사, 상품 분류
	 * 						페이지 정보(페이지 번호) <br/>
	 * @return		조회 조건에 해당하는 상품 개수
	 * @throws SQLException
	 */
	public int getCount(Connection con , PageBean bean) throws SQLException{
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try{
			String key = bean.getKey();
			String word = bean.getWord();
			StringBuilder sql = new StringBuilder(100);			
			sql.append(" SELECT count(*) cnt	\n");
			sql.append("	FROM GOODS			\n");
		    sql.append("   WHERE 1 = 1			\n");
			if ( !key.equals("all") && !word.equals("")){
				if( key.equals("brand")){
					sql.append(" 	and brand like ? 	");
				}
				else if( key.equals("maker")){
					sql.append("		and maker like ? 	");
				}
				else if (key.equals("cno")){
					sql.append("			and cno = ? 	");
				}
			}
			System.out.println(sql);
			stmt = con.prepareStatement(sql.toString());
			int idx = 1;	//조건에 따라 ? 개수가 달라지므로 ?에 대한 index
			if ( !key.equals("all") && !word.trim().equals("")){
				if(key.equals("cno")){
					stmt.setInt(idx++, Integer.parseInt(word));
				}else{
					stmt.setString(idx++, "%"+word+"%");
				}
			}
			rs = stmt.executeQuery();
			while (rs.next()){
				return rs.getInt("cnt");
			}
		}finally{
			DBUtil.close(stmt);
			DBUtil.close(rs);
		}
		return 0;
	}
	//서비스에서 sqlexception 을 해줘야 하기때문
}
