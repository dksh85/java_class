package com.kdn.goods;

public class GoodsException extends RuntimeException {
	public GoodsException(String msg){
		super(msg);
	}
}
