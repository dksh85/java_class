package com.kdn.goods;

import com.kdn.goods.dao.GoodsDao;
import com.kdn.goods.dao.GoodsDaoForList;
import com.kdn.goods.domain.Goods;

public class Main {
	public static void main(String[] args) {
		GoodsDao dao = new GoodsDaoForList();
		dao.addGoods(new Goods("1", "오곡쿠키", 3000, "5cookie.jpg"));
		try{
			dao.addGoods(new Goods("1", "오곡쿠키", 3000, "5cookie.jpg"));
		}catch(Exception e){
			e.printStackTrace();
		}
		dao.addGoods(new Goods("2", "검은콩두유", 1000, "bean.jpg"));
		dao.addGoods(new Goods("3", "보드마카", 1000, "maker.jpg"));
		try{
			dao.sell("1", 10);
		}
		catch(Exception e){
			System.out.printf("오류 메세지 : %s \n", e.getMessage());
		}
		System.out.println("=============================");
		dao.buy("1", 30);
		dao.sell("1", 10);

		System.out.println("=============================");
		System.out.println(dao.searchAll());
		System.out.println(dao.searchGoods("1"));
		
		try{
			System.out.println(dao.searchGoods("4"));
		} catch (Exception e){
			e.printStackTrace();
		}
	}
}
