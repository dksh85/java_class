package com.kdn.goods.dao;

import java.util.List;

import com.kdn.goods.domain.Goods;

public interface GoodsDao {
	public void close();
	public List<Goods> searchAll();
	public void addGoods(Goods g);
	public void updateGoods(Goods g);
	public void removeGoods(String gno);
	public Goods searchGoods(String gno);
	public void sell(String gno, int amount);
	public void buy(String gno, int amount);
}
