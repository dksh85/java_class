package com.kdn.goods.dao;

import java.lang.reflect.Array;
import java.util.LinkedList;

import com.kdn.goods.GoodsException;
import com.kdn.goods.domain.Goods;

public class GoodsDaoForList implements GoodsDao{
	private LinkedList<Goods> goodsList;
	
	

	public GoodsDaoForList(LinkedList<Goods> goodsList) {
		goodsList = new LinkedList<Goods>();
	}

	public GoodsDaoForList() {
		this.goodsList = new LinkedList<Goods>();
	}

	@Override
	public void close() {
		System.exit(0); //JVM 종료
	}
	/*
	 * 상품을 판매하는 기능
	 */

	@Override
	public LinkedList<Goods> searchAll() {
		return goodsList;
	}

	@Override
	public void addGoods(Goods g) {
		if(g!=null){
			int index = goodsList.indexOf(g);
			if(index > -1){
				String msg = String.format("상품번호 %s번은 이미 등록되어 있습니다", g.getGno());
				throw new GoodsException(msg);
			}
			goodsList.add(g);
		}else{
			throw new GoodsException("등록할 상품 정보를 입력하세요");
		}
	}

	@Override
	public void updateGoods(Goods g) {
		if(g != null){
			int index = goodsList.indexOf(g);
			if(index < 0){
				String msg = String.format("상품번호 %s번을 찾을수 없습니다. ", g.getGno());
				throw new GoodsException(msg);
			}
			//set(int index, Object o) : index번째에 setting
			goodsList.set(index, g);
		}else{
			throw new GoodsException("수정할 정보를 입력하세요!!!");
		}
	}

	public int findIndex(String gno){
		if(gno!=null){
			int size = goodsList.size();
			for(int i = 0; i< size; i++){
				Goods g = goodsList.get(i);
				if(gno.equals(g.getGno())){ //상품 번호에 해당하는 상품이 있는 경우
					return i;	//상품 번호에 해당하는 index를 반환
				}
			}
			// 동일한 상품을 못 찾은 경우
			String msg = String.format("상품번호 %s번을 찾을 수 없습니다.", gno);
			throw new GoodsException(msg);
		}
			String msg = String.format("상품 정보를 입력하세요");
			throw new GoodsException(msg);
	}

	@Override
	public void removeGoods(String gno) {
		goodsList.remove(findIndex(gno));
	}

	@Override
	public Goods searchGoods(String gno) {
		return goodsList.get(findIndex(gno));
		
	}

	public void sell(String gno, int amount){
		int index = findIndex(gno); //exception 이 떨어지니까 이후에는 index가 있다는 의미
		//상품번호에 해당하는 상품을 못찾으면 Exception이 발생하므로
		//상품번호에 해당하느느 상품이 있는 경우
		Goods find = goodsList.get(index);
		int quantity = find.getQuantity();
		//재고 수량이 판매 수량보다 많거나 같으면 판매 가능
		if( quantity >= amount) find.setQuantity(quantity - amount);
		else throw new GoodsException("재고 수량이 부족합니다. ");
	}

	public void buy(String gno, int amount){	
		int index = findIndex(gno);
		Goods find = goodsList.get(index);
		find.setQuantity(find.getQuantity() + amount);
	}
}
