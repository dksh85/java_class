package example.com.kdngcm;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.NfcF;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import example.com.kdngcm.NdefMessageParser;
import example.com.kdngcm.ParsedRecord;
import example.com.kdngcm.TextRecord;
import example.com.kdngcm.UriRecord;
import example.com.kdngcm.http.HttpUtil;
import example.com.kdngcm.http.NfcHttpUtil;


public class NfcReadActivity extends AppCompatActivity {
    private TextView readResult;
    private Button btn;
    private String recordStr = null;

    private NfcAdapter mAdapter;
    private PendingIntent mPendingIntent;
    private IntentFilter[] mFilters;
    private String[][] mTechLists;




    public static final int TYPE_TEXT = 1;
    public static final int TYPE_URI = 2;

    private Context context;

    private EditText read_edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc_read);

        context = this;
        btn= (Button)findViewById(R.id.read_btn);

        read_edit = (EditText) findViewById(R.id.read_edit);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();
        }

        readResult = (TextView) findViewById(R.id.readResult);

        // NFC 관련 객체 생성
        mAdapter = NfcAdapter.getDefaultAdapter(this);
        Intent targetIntent = new Intent(this, NfcReadActivity.class);
        targetIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mPendingIntent = PendingIntent.getActivity(this, 0, targetIntent, 0);

        IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndef.addDataType("*/*");
        } catch (Exception e) {
            throw new RuntimeException("fail", e);
        }

        mFilters = new IntentFilter[] { ndef, };

        mTechLists = new String[][] { new String[] { NfcF.class.getName() } };

        Intent passedIntent = getIntent();
        if (passedIntent != null) {
            String action = passedIntent.getAction();
            if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)) {
                processTag(passedIntent);
            }
        }
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(NfcReadActivity.this, MainActivity.class);
//                startActivity(intent);



            }
        });
    }

    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_read, container,
                    false);
            return rootView;
        }
    }

    /************************************
     * 여기서부턴 NFC 관련 메소드
     ************************************/
    public void onResume() {
        super.onResume();

        if (mAdapter != null) {
            mAdapter.enableForegroundDispatch(this, mPendingIntent, mFilters,
                    mTechLists);
        }
    }

    public void onPause() {
        super.onPause();

        if (mAdapter != null) {
            mAdapter.disableForegroundDispatch(this);
        }
    }

    // NFC 태그 스캔시 호출되는 메소드
    public void onNewIntent(Intent passedIntent) {
        // NFC 태그
        Tag tag = passedIntent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        if (tag != null) {
            byte[] tagId = tag.getId();
//            readResult.append("태그 ID : " + toHexString(tagId) + "\n"); // TextView에 태그 ID 덧붙임

        }

        if (passedIntent != null) {
            processTag(passedIntent); // processTag 메소드 호출
        }
    }

    // NFC 태그 ID를 리턴하는 메소드
    public static final String CHARS = "0123456789ABCDEF";
    public static String toHexString(byte[] data) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.length; ++i) {
            sb.append(CHARS.charAt((data[i] >> 4) & 0x0F)).append(
                    CHARS.charAt(data[i] & 0x0F));
        }
        return sb.toString();
    }

    // onNewIntent 메소드 수행 후 호출되는 메소드
    private void processTag(Intent passedIntent) {
        Parcelable[] rawMsgs = passedIntent
                .getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        if (rawMsgs == null) {
            return;
        }

        // 참고! rawMsgs.length : 스캔한 태그 개수
        //Toast.makeText(getApplicationContext(), "스캔 성공!", Toast.LENGTH_SHORT).show();

        NdefMessage[] msgs;
        if (rawMsgs != null) {
            msgs = new NdefMessage[rawMsgs.length];
            for (int i = 0; i < rawMsgs.length; i++) {
                msgs[i] = (NdefMessage) rawMsgs[i];
                showTag(msgs[i]); // showTag 메소드 호출
                httpweb();
            }
        }
    }

    // NFC 태그 정보를 읽어들이는 메소드
    private int showTag(NdefMessage mMessage) {
        //String recordStr = ""; // NFC 태그로부터 읽어들인 텍스트 값
        List<ParsedRecord> records = NdefMessageParser.parse(mMessage);
        final int size = records.size();
        for (int i = 0; i < size; i++) {
            ParsedRecord record = records.get(i);

            int recordType = record.getType();
            if (recordType == ParsedRecord.TYPE_TEXT) {
                //recordStr = "id:sss,pwd:kdn,hobby:abc"; //이 형식으로 읽어와야 함.
                recordStr = ((TextRecord) record).getText();
            } else if (recordType == ParsedRecord.TYPE_URI) {
                recordStr = "URI : " + ((UriRecord) record).getUri().toString();
            }

            //readResult.append(recordStr + "\n"); // 읽어들인 텍스트 값을 TextView에 덧붙임 //잠시지움
            read_edit.setText(recordStr.toString());
        }

        return size;
    }

    public void httpweb(){
        String SERVER_URL ="http://10.220.50.198:8088/AndroidWEB2/list.kdn";
        NfcHttpUtil nfchu = new NfcHttpUtil(context);
        StringTokenizer token = new StringTokenizer(recordStr, ",");
        String[] params = {SERVER_URL, token.nextToken()};
        //String[] params = {SERVER_URL, "sendMsg:"+recordStr};
        nfchu.execute(params);
    }

    public void printToast(String msg){
        Toast.makeText(NfcReadActivity.this, msg, Toast.LENGTH_SHORT).show();
    }

    public void transActivity(JSONArray jsonArray){
     //   Intent intent = new Intent(NfcReadActivity.this, MainActivity.class);
        ArrayList<String> list = new ArrayList<String>();
        try{
            for(int i = 0; i < jsonArray.length() ; i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                //list.add(jsonObject.getString("id")+":"+jsonObject.getString("pwd")+":"+jsonObject.getString("name"));
                list.add(jsonObject.getString("auth_code"));
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        //intent.putExtra("ary", list);
        //startActivity(intent);

    }



}
