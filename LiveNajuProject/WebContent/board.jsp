<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
function count_like(board_no) {
	//alert(100);
	var url = "count_like.json";
	var param = {board_no:board_no}
	$.post(url, param, load_count_like);
}

function load_count_like(result) {
	if(result.code == "fail") {
		alert("카운트 오류: "+ result.msg);
		return;
	}
	location.href="board_main.do";
}

function test(i){
	all(i);
}
</script>
</head>
<body>
 <section id="content" class="content-sidebar bg-white">
  
    <!-- .sidebar -->
    <section class="main">
      <div class="padder">
        <div class="row">
       
        </div>
      </div>
      <div>
      	<div>
      		<ul class="nav nav-tabs m-b-none no-radius">
       			<li class="active"><a href="#all" onclick="test(1);" data-toggle="tab">전체 게시물</a></li>
        		<li><a href="#clothes" onclick="clothes(1);" data-toggle="tab">의(공동구매, 중고나라)</a></li>
        		<li><a href="#eat" onclick="eat(1);" data-toggle="tab">식(주변 맛집)</a></li>
        		<li><a href="#home" onclick="home(1);" data-toggle="tab">주(룸메, 방, 카풀)</a></li>
     		</ul>
      	</div>
      </div>
    
      <div class="tab-content">
        <div class="tab-pane active" id="all">
          <ul class="list-group list-normal">
            
           <!--  <li class="list-group-item">
              <a href="#" class="thumb-small pull-left m-r-small"><img src="images/avatar.jpg" class="img-circle"></a>
              <div>
                <small class="pull-right text-muted">3분 전</small>
                <strong><a href="#" data-toggle="modal" data-target="#myModal">한전KDN</a></strong> 공동구매 :
                <a href="form.html">고데기 공동구매 합니다.</a>
                <div class="text-small">
                  <a href="#">추천</a> <a href="#">댓글달기</a>
                </div>
              </div> 
            </li> -->
            <c:forEach var="list" items="${list}">
            <li class="list-group-item">
              <a href="#" class="thumb-small pull-left m-r-small"><img src="images/avatar.jpg" class="img-circle"></a>
              <div>
                <small class="pull-right text-muted">${list.board_date}</small>
                <div class="btn-group">
                	<a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="modal(${list.member_no});"><strong>${list.member_name}</strong> </a>
                    <ul class="dropdown-menu">
                    	<li><a href="#" data-toggle='modal' data-target='#myModal'>회원정보 보기</a></li>
                        <li><a href="myBoard.do?member_no=${list.member_no}">게시글 보기</a></li>
                        <!-- <li class="divider"></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">Separated link</a></li> -->
                  	</ul>
                	${list.category_name} 
               		<a href="board_detail.do?board_no= ${list.board_no}">${list.board_title}</a>
               	</div>
               	<c:if test="${empty session_id}">
               	<div>
               		<br>
               	</div>
				</c:if>
               	<c:if test="${!empty session_id}">
                <div class="text-small">
                  <a href="#" onclick="count_like(${list.board_no});">좋아요</a>&nbsp;${list.board_like}&nbsp;&nbsp;&nbsp; <a href="board_reply.do?board_no= ${list.board_no}">댓글달기</a>
                </div>
                </c:if>
              </div>
            </li>
            </c:forEach>
           </ul>
          </div>
           
        <div class="tab-pane" id="clothes">
        </div>
        <div class="tab-pane" id="eat">
        </div>
        <div class="tab-pane" id="home">
        </div>
      </div>
      <c:if test="${empty session_id}">
      <footer class="panel-heading bg bg-default">
	  	 <select class="form-control select-float" id="key" style="width: auto;"><option value="nothing">분류선택</option>
			<option value="category">카테고리</option>
			<option value="title">제목</option>
			<option value="content">내용</option>
			<option value="name">작성자</option></select>	
       	 	
          <div class="input-group">
            <input type="text" class="input-sm form-control" id="word" placeholder="로그인 후 이용해주세요.">
              <span class="input-group-btn">
               <input type="button" class="btn btn-sm btn-white" value="검색">
              </span>                
          </div>
      </footer>
      </c:if>
      <c:if test="${not empty session_id}">
      <footer class="panel-heading bg bg-default">
	  	 <select class="form-control select-float" id="key" style="width: auto;"><option value="nothing">분류선택</option>
			<option value="category">카테고리</option>
			<option value="title">제목</option>
			<option value="content">내용</option>
			<option value="name">작성자</option></select>	
       	 	
          <div class="input-group">
            <input type="text" class="input-sm form-control" id="word" placeholder="검색어를 입력해주세요">
              <span class="input-group-btn">
               <input type="button" class="btn btn-sm btn-white" value="검색" onclick="searchAll();">
              </span>                
          </div>
      </footer>
      <br>
      	<div class="right-float">
      		<p><a href="board_form.do" class="btn-sm btn-white">글쓰기</a></p>
      	</div> 
      <br>
      <br>
     </c:if>
    <div class="text-center">
            <ul class="pagination pagination" id="page">
              <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
              <c:forEach var="index" items="${page}" >
              	<li><a href="board_main_page.do?pageNo=${index}" value="${index}">${index}</a></li>
              </c:forEach>
              <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
            </ul>
          </div>
        
    </section>
    
    <!-- /.sidebar -->
    
  </section>
	
  <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" 
   aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" 
               aria-hidden="true">×
            </button>
            <h4 class="modal-title" id="myModalLabel">
               	회원정보 보기 
            </h4>
         </div>
         <div class="modal-body" id="memberDetail">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" 
               data-dismiss="modal">
               	닫기
            </button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
	function modal(member_no) {
		var url="modal_detail.json";
		//alert(member_no);
		var param ={member_no:member_no};
		$.post(url, param, load_modal);
	}
	
	function load_modal(result) {
		$("#memberDetail").empty();
		var s = '회원이름 : ' + result.member.member_name + '<br>';
		s += '회사 : ' + result.member.member_company + '<br>';
		s += '회원ID : ' + result.member.member_id + '<br>';
		$("#memberDetail").append(s);
	}

	function clothes(pageNo) {
		//alert(100);
		var url = "board_main_clothes.json";
		var param = {pageNo:pageNo}
		$.post(url, param, load_clothes);
	}
	
	function load_clothes(result) {
		if(result.code == "fail") {
			alert("서버오류 : " + result.msg);
			return;
		}
		//alert(400);
		refresh_list_clothes(result.list);
	}
	
	function refresh_list_clothes(list) {
		//alert(300);
		/* $("#clothes").append("안녕"); */
		$("#clothes").empty();
		//$("#clothes tr:gt(0)").empty();
		var s = '<ul class="list-group list-normal">';
		for(var i=0; i < list.length; i++) {
        s += '<li class="list-group-item">';
        s += '<a href="#" class="thumb-small pull-left m-r-small"><img src="images/avatar.jpg" class="img-circle"></a>';
        s += ' <div>';
        s += ' <small class="pull-right text-muted">'+ list[i].board_date +'</small>';
        s += ' <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="modal('+list[i].member_no+');"><strong>'+ list[i].member_name + '</strong></a>'; 
        s += '	<ul class="dropdown-menu">';
    	s += '		<li><a href="#" data-toggle="modal" data-target="#myModal">회원정보 보기</a></li>';
        s += '		<li><a href="myBoard.do?member_no='+list[i].member_no+'">게시글 보기</a></li>';
        s += '	</ul>' +list[i].category_name;
        s += ' <a href="board_detail.do?board_no=' + list[i].board_no +'">'+ list[i].board_title +'</a>';
        s += '  <div class="text-small">';
        s += '    <a href="#" onclick="count_like('+list[i].board_no+');">좋아요</a>&nbsp;' + list[i].board_like + '&nbsp;&nbsp;&nbsp; <a href="board_reply.do?board_no= '+list[i].board_no+'">댓글달기</a>';
        s += '  </div>';
        s += ' </div>';
        s += '</li>';
		}
      	s += '</ul>';
      	$("#clothes").append(s);
      	
      	 pre_clothes();
	}
	
	 function pre_clothes() {
		var url = "board_clothes_page.json";
      	var param = {pageNo:1};
      	$.post(url, param, load_clothes_page);
	}
	
	function load_clothes_page(result) {
		//alert(500);
		//alert("test : "+result.pageBean);
		$("#page").empty();
		var str ='<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>';
        var value = 0;
		for(var i = 1 ; i <= result.pageBean; i++) {
        	str += '<li><a href="#" onclick="clothes('+ i +');" value='+ i +'>'+ i +'</a></li>';
        }
        str += '<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>';
        
        $("#page").append(str);
	}
	 
	function eat(pageNo) {
		//alert(100);
		var url = "board_main_eat.json";
		var param = {pageNo:pageNo};
		$.post(url, param, load_eat);
	}
	
	function load_eat(result) {
		if(result.code == "fail") {
			alert("서버오류 : " + result.msg);
			return;
		}
		//alert(400);
		refresh_list_eat(result.list);
	}
	
	function refresh_list_eat(list) {
		//alert(500);
		$("#eat").empty();
		var s = '<ul class="list-group list-normal">';
		for(var i=0; i < list.length; i++) {
        s += '<li class="list-group-item">';
        s += '<a href="#" class="thumb-small pull-left m-r-small"><img src="images/avatar.jpg" class="img-circle"></a>';
        s += ' <div>';
        s += ' <small class="pull-right text-muted">'+ list[i].board_date +'</small>';
        s += ' <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="modal('+list[i].member_no+');"><strong>'+list[i].member_name+'</strong></a>'; 
        s += '	<ul class="dropdown-menu">';
    	s += '		<li><a href="#" data-toggle="modal" data-target="#myModal">회원정보 보기</a></li>';
        s += '		<li><a href="myBoard.do?member_no='+list[i].member_no+'">게시글 보기</a></li>';
        s += '	</ul>' +list[i].category_name;
        s += ' <a href="board_detail.do?board_no=' + list[i].board_no +'">'+ list[i].board_title +'</a>';
        s += '  <div class="text-small">';
        s += '    <a href="#"onclick="count_like('+list[i].board_no+');">좋아요</a>&nbsp;' + list[i].board_like + '&nbsp;&nbsp;&nbsp; <a href="board_reply.do?board_no= '+list[i].board_no+'">댓글달기</a>';
        s += '  </div>';
        s += ' </div>';
        s += '</li>';
		}
      	s += '</ul>';
      	$("#eat").append(s);
      	
     	pre_eat();
	}
	
	function pre_eat() {
		var url = "board_eat_page.json";
      	var param = {pageNo:1};
      	$.post(url, param, load_eat_page);
	}
	
	function load_eat_page(result) {
		//alert(500);
		//alert("test : "+result.pageBean);
		$("#page").empty();
		var str ='<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>';
        var value = 0;
		for(var i = 1 ; i <= result.pageBean; i++) {
        	str += '<li><a href="#" onclick="eat('+ i +');" value='+ i +'>'+ i +'</a></li>';
        }
        str += '<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>';
        
        $("#page").append(str);
	}
	
	function home(pageNo) {
		//alert(100);
		var url = "board_main_home.json";
		var param = {pageNo:pageNo};
		$.post(url, param, load_home);
	}
	
	function load_home(result) {
		if(result.code == "fail") {
			alert("서버오류 : " + result.msg);
			return;
		}
		//alert(400);
		refresh_list_home(result.list);
	}
	
	function refresh_list_home(list) {
		$("#home").empty();
		var s = '<ul class="list-group list-normal">';
		for(var i=0; i < list.length; i++) {
        s += '<li class="list-group-item">';
        s += '<a href="#" class="thumb-small pull-left m-r-small"><img src="images/avatar.jpg" class="img-circle"></a>';
        s += ' <div>';
        s += ' <small class="pull-right text-muted">'+ list[i].board_date +'</small>';
        s += ' <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="modal('+list[i].member_no+');"><strong>'+ list[i].member_name + '</strong></a>';
        s += '	<ul class="dropdown-menu">';
    	s += '		<li><a href="#" data-toggle="modal" data-target="#myModal">회원정보 보기</a></li>';
        s += '		<li><a href="myBoard.do?member_no='+list[i].member_no+'">게시글 보기</a></li>';
        s += '	</ul>' +list[i].category_name;
        s += ' <a href="board_detail.do?board_no=' + list[i].board_no +'">'+ list[i].board_title +'</a>';
        s += '  <div class="text-small">';
        s += '    <a href="#"onclick="count_like('+list[i].board_no+');">좋아요</a>&nbsp;' + list[i].board_like + '&nbsp;&nbsp;&nbsp; <a href="board_reply.do?board_no= '+list[i].board_no+'">댓글달기</a>';
        s += '  </div>';
        s += ' </div>';
        s += '</li>';
		}
      	s += '</ul>';
      	$("#home").append(s);
      	
      	pre_home();
	}
	
	function pre_home() {
		var url = "board_home_page.json";
      	var param = {pageNo:1};
      	$.post(url, param, load_home_page);
	}
	
	function load_home_page(result) {
		//alert(500);
		//alert("test : "+result.pageBean);
		$("#page").empty();
		var str ='<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>';
        var value = 0;
		for(var i = 1 ; i <= result.pageBean; i++) {
        	str += '<li><a href="#" onclick="home('+ i +');" value='+ i +'>'+ i +'</a></li>';
        }
        str += '<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>';
        
        $("#page").append(str);
	}
	
	function searchAll() {
		var url="board_searchAll.json";
		var key = $("#key").val();
		var word = $("#word").val();
		checkForm(key, word);
		var param = {key:key, word:word};
		$.post(url, param, load_searchAll);
	}
	
	function checkForm(key, word) {
		if(key == "") {
			alert("분류를 선택해주세요.");
			return;
		}
		if(word == "") {
			alert("검색어를 입력해주세요");
			return;
		}
	}
	
	function load_searchAll(result) {
		if(result.code=="fail") {
			alert("검색오류 : " + result.msg);
			return;
		}
		if(result.msg_ok == "yes") {
			alert("검색 결과가 없습니다.");
			return;
		}
		board_list(result.list);
	}
	
	function board_list(list) {
		$("#all").empty();
		var s = '<ul class="list-group list-normal">';
		for(var i=0; i < list.length; i++) {
        s += '<li class="list-group-item">';
        s += '<a href="#" class="thumb-small pull-left m-r-small"><img src="images/avatar.jpg" class="img-circle"></a>';
        s += ' <div>';
        s += ' <small class="pull-right text-muted">'+ list[i].board_date +'</small>';
        s += ' <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="modal('+list[i].member_no+');"><strong>'+ list[i].member_name + '</strong></a>'; 
        s += '	<ul class="dropdown-menu">';
    	s += '		<li><a href="#" data-toggle="modal" data-target="#myModal">회원정보 보기</a></li>';
        s += '		<li><a href="myBoard.do?member_no='+list[i].member_no+'">게시글 보기</a></li>';
        s += '	</ul>' +list[i].category_name;        
        s += ' <a href="board_detail.do?board_no=' + list[i].board_no +'">'+ list[i].board_title +'</a>';
        s += '  <div class="text-small">';
        s += '    <a href="#"onclick="count_like('+list[i].board_no+');">좋아요</a>&nbsp;' + list[i].board_like + '&nbsp;&nbsp;&nbsp; <a href="board_reply.do?board_no= '+list[i].board_no+'">댓글달기</a>';
        s += '  </div>';
        s += ' </div>';
        s += '</li>';
		}
      	s += '</ul>';
      	$("#all").append(s);
      	//alert(800);
      	pre_all();
	} 
	
	function all(pageNo) {
		//alert(100);
		var url = "board_main_all.json";
		var param = {pageNo:pageNo};
		$.post(url, param, load_all);
	}
	
	function load_all(result) {
		if(result.code == "fail") {
			alert("서버오류 : " + result.msg);
			return;
		}
		//alert(400);
		board_list(result.list);
	}
	
	function pre_all() {
		//alert(700);
		var url = "board_all_page.json";
      	var param = {pageNo:1};
      	$.post(url, param, load_all_page);
	}
	
	function load_all_page(result) {
		//alert(500);
		//alert("test : "+result.pageBean);
		$("#page").empty();
		var str ='<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>';
        var value = 0;
		for(var i = 1 ; i <= result.pageBean; i++) {
        	str += '<li><a href="#" onclick="test('+ i +');" value='+ i +'>'+ i +'</a></li>';
        }
        str += '<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>';
        
        $("#page").append(str);
	}
</script>
</body>
</html>