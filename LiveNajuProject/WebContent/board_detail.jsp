<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
  <section id="content">
    <section class="main padder">
      <div class="clearfix">
        <h4><i class="fa fa-edit"></i>게시글 상세보기</h4>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <section class="panel">
            <div class="panel-body">
              <form class="form-horizontal" method="post" data-validate="parsley">   
                <c:if test="${session_id == board.member_no}">
                <div class="form-group" align="right">
                  <div class="col-lg-12 ">                      
                    <%-- <button onclick="location.href='detail_updateform.do?board_no=${board.board_no}'" class="btn btn-primary">수정</button>
                    <button onclick="location.href='detail_removeform.do?board_no=${board.board_no}'" class="btn btn-white">삭제</button> --%>
                  	<input type="button" value = "수정" class="btn btn-primary" onclick="location.href='detail_updateform.do?board_no=${board.board_no}'">
                  	<input type="button" value = "삭제" class="btn btn-white" onclick="detail_remove(${board.board_no});">
                  </div>
                </div>
                </c:if>
                <div class="form-group">
                  <label class="col-lg-3 control-label">카테고리</label>
                  <div class="col-lg-8">
                    	 ${board.category_name}                 
				  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">회원 이름</label><!-- 나중에 id나 닉네임으로 박아둘 것 -->
                  <div class="col-lg-8">
                   		${board.member_name}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">직장명</label>
                  <div class="col-lg-8">
                    	${board.member_company}
                  	<div class="line line-dashed m-t-large"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">제목</label>
                  <div class="col-lg-8">
                   		${board.board_title}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">내용</label>
                  <div class="col-lg-8">
                   		${board.board_content}
                  </div>
                </div>
               </section>
               <section>
               	<c:if test="${not empty reply}">
	     			<jsp:include page="${reply}">
	     				<jsp:param value="${board.board_no}" name="board_no"/>
	     				<jsp:param value="${board.member_no}" name="member_no"/>
	     			</jsp:include>
				</c:if>
                </section>
                 </form>
        	</div>
           </section>
         
          </div>
          </div>
	</section>
</section>
          
          <script type="text/javascript">
          function detail_remove(board_no) {
        	  var url = "detail_remove.json";
        	  var param = {board_no:board_no};
        	  $.post(url, param, load_detail_remove);
          }
          
          function load_detail_remove(result) {
        	  if(result.code=="fail") {
        		  alert("삭제 오류: " + result.msg);
        		  return;
        	  }
        	  alert("삭제되었습니다.");
        	  location.href="board_main.do";
          }
          </script>
</body>
</html>