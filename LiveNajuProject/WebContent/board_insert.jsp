<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
 <section id="content">
    <section class="main padder">
      <div class="clearfix">
        <h4><i class="fa fa-edit"></i>게시글 작성</h4>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <section class="panel">
            <div class="panel-body">
              <form class="form-horizontal" method="post" data-validate="parsley" onsubmit="checkForm; return false;">      
                <input type="hidden" name="member_no" id="member_no" value="${session_id}">
                <div class="form-group">
                  <label class="col-lg-3 control-label">카테고리</label> 
                  <div class="col-lg-8">
                    <select class="form-control select-float" name="category" id="category" data-required="true" style="width: auto;"><option value="">카테고리를 선택해주세요</option>
						<option value="10">의(공동구매)</option>
						<option value="20">의(중고나라)</option>
						<option value="30">식(주변 맛집)</option>
						<option value="40">주(카풀)</option>
						<option value="50">주(룸메)</option>
					</select>	                  
				</div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">회원이름</label><!-- 나중에 id나 닉네임으로 박아둘 것 -->
                  <div class="col-lg-8">
						${session_name}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">직장명</label>
                  <div class="col-lg-8">
                  		${session_company}
                  	<div class="line line-dashed m-t-large"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">제목</label>
                  <div class="col-lg-8">
                    <input type="text" name="title" id="title" placeholder="제목을 입력하세요" class="bg-focus form-control" data-required="true">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">비밀번호</label>
                  <div class="col-lg-8">
                    <input type="password" name="password" id="password" placeholder="비밀번호를 입력해주세요" class="bg-focus form-control" data-required="true">
                  	<div class="line line-dashed m-t-large"></div>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-lg-3 control-label">내용</label>
                  <div class="col-lg-8">
                    <textarea placeholder="내용을 입력하세요" rows="9" class="form-control" name="contents" id="contents" data-required="true" data-trigger="keyup" data-rangelength="[10,200]"></textarea>
                  </div>
                </div>
                <div class="form-group" align="center">
                  <div class="col-lg-12 ">                      
                    <button onclick="board_insert();" class="btn btn-primary">등록</button>
                    <button type="reset" class="btn btn-white">취소</button>
                  </div>
                </div>
                 </form>
        	</div>
            	
          </section>
          </div>
          </div>
          </section>
          </section>     
    <script type="text/javascript">
    	function board_insert() {
    		var url="board_insert.json";
    		var category_no=$("#category").val();
    		//var member_name=$("#name").val();
    		//var member_company=$("#officename").val();
    		var board_title=$("#title").val();
    		var board_password=$("#password").val();
    		var board_content=$("#contents").val();
    		var member_no = $("#member_no").val();
    		checkForm(category_no, board_title, board_password, board_content);
    		var param = {board_title:board_title, member_no:member_no, board_content:board_content, board_password:board_password, category_no:category_no};
    		$.post(url, param, load_board_insert);
    		
    	}
    	
    	function load_board_insert(result) {
    		if(result.code=="fail") {
    			alert("서버오류 : " + result.msg);
    			return;
    		}
    		location.href="board_main.do";
    	}
    
    	function checkForm(category_no, member_name, member_company, board_title, board_password, board_content) {
			if(category_no == "") {
				alert("카테고리를 입력해주세요.");
				return false;
			}    		
			if(member_name == "") {
				alert("이름을 입력해주세요.");
				return false;
			}    		
			if(member_company == "") {
				alert("회사명을 입력해주세요.");
				return false;
			}    		
			if(board_title == "") {
				alert("제목을 입력해주세요.");
				return false;
			}    		
			if(board_password == "") {
				alert("비밀번호를 입력해주세요.");
				return false;
			}    		
			if(board_content == "") {
				alert("내용을 입력해주세요.");
				return false;
			}    		
			return true;
    	}
    </script>
</body>
</html>