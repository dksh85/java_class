<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
    $(function () {
      	//alert(100);
      	reply_list();
      });
    
     function reply_list() {
    	var url = "reply_list.json";
    	var board_no = $("#board_no").val();
    	var param = {board_no:board_no};
       	$.post(url, param, load_reply);
     }
      	
      function load_reply(result) {
      	if(result.code == "fail") {
      		alert("서버오류 : " + result.msg);
      		return;
      	}
      	//alert(400);
      	refresh_reply_list(result.list);
      }
      	
      function refresh_reply_list(list) {
      	//alert(300);
      	/* $("#clothes").append("안녕"); */
      	//alert(list);
      	var session_id = $("#session_id").val();
      	var member_no = $("#member_no").val();
      	$("#reply").empty();
      	var s = '<ul class="list-group list-normal">';
      	for(var i=0; i < list.length; i++) {
            s += '<li class="list-group-item">';
            s += '<a href="#" class="thumb-small pull-left m-r-small"><img src="images/avatar.jpg" class="img-circle"></a>';
            s += ' <div>';
            s += ' <small class="pull-right text-muted">'+ list[i].reply_date +'</small>';
            s += ' <a href="#"><strong>'+ list[i].member_name + '</strong></a>';
            s += '&nbsp;&nbsp;' +list[i].reply_content;
            s += '  <div class="text-small">';
            s += '    <a href="#" onclick="reply_count_like('+list[i].reply_no+');">좋아요</a>&nbsp;' + list[i].reply_like;
            if(session_id == list[i].member_no){
            	s += '	  <a href="#" class="right-float" onclick="reply_remove('+list[i].reply_no+');">삭제</a><a href="#" class="right-float" onclick="reply_update('+list[i].reply_no+');">수정</a>';
            }
            s += '  </div>';
            s += ' </div>';
            s += '</li>';
      	}
        s += '</ul>';
        $("#reply").append(s);
      }
      
      function reply() {
    	  var url="reply_insert.json";
    	  var board_no = $("#board_no").val();
    	  var reply_content=$("#reply_content").val();
    	  checkForm(reply_content);
    	  var param = {board_no:board_no, reply_content:reply_content};
    	  $.post(url, param, success_reply);
      }
      
      function success_reply(result) {
    	  if(result.code == "fail") {
    		  alert("댓글 등록 오류 : " + result.msg);
    		  return;
    	  }
    	  reply_list();
      }
      
       function checkForm(reply_content) {
    	  if(reply_content == "") {
    		  alert("댓글 내용을 입력해주세요");
    		  return false;
    	  }
    	  return true;
      } 
       
       function reply_count_like(reply_no) {
       	//alert(100);
       	var url = "reply_like.json";
       	var param = {reply_no:reply_no}
       	$.post(url, param, load_reply_like);
       }

       function load_reply_like(result) {
       	if(result.code == "fail") {
       		alert("카운트 오류: "+ result.msg);
       		return;
       	}
       	reply_list();
       }
       
       function reply_remove(reply_no) {
     	  var url = "reply_remove.json";
     	  var param = {reply_no:reply_no};
     	  $.post(url, param, load_reply_remove);
       }
       
       function load_reply_remove(result) {
     	  if(result.code=="fail") {
     		  alert("삭제 오류: " + result.msg);
     		  return;
     	  }
     	  alert("삭제되었습니다.");
     	 reply_list();
       }
</script>
<title>Insert title here</title>
</head>
<body>
  <section id="content">
    <section class="main">
      <div class="clearfix">
        <h4><i class="fa fa-comments"></i>댓글</h4>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <section class="panel">
            <div class="panel-body">
              <form class="form-horizontal" method="post" data-validate="parsley" onsubmit="checkForm;">      
              <input type="hidden" name="board_no" id = "board_no" value="${param.board_no}">
              <input type="hidden" name="member_no" id = "member_no" value="${param.member_no}">
              <input type="hidden" name="session_id" id = "session_id" value="${session_id}">
              <div id="reply">
        	  </div>
        	  <c:if test="${empty session_id}">
        	  <div class="input-group">
            	<input type="text" class="input-sm form-control" id = "reply_content" placeholder="로그인 후 이용해주세요.">
              	<span class="input-group-btn">
               		<input type="button" class="btn btn-sm btn-primary" value="등록">
             	</span>                
          	  </div>
          	  </c:if>
        	  <c:if test="${!empty session_id}">
        	  <div class="input-group">
            	<input type="text" class="input-sm form-control" id = "reply_content" placeholder="댓글을 입력해주세요">
              	<span class="input-group-btn">
               		<input type="button" class="btn btn-sm btn-primary" value="등록" onclick="reply();">
             	</span>                
          	  </div>
          	  </c:if>
            </form>
        	</div>
          </section>
            	<input type="button" value = "목록으로" class="btn btn-white right-float" onclick="location.href='board_main.do'">
          </div>
          </div>
          </section>
          </section>
</body>
</html>