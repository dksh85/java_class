<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
 <section id="content">
    <section class="main padder">
      <div class="clearfix">
        <h4><i class="fa fa-edit"></i>게시글 수정</h4>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <section class="panel">
            <div class="panel-body">
              <form class="form-horizontal" method="post" data-validate="parsley" action="detail_update.do">      
                <input type="hidden" name="board_no" value="${board.board_no}">
                <input type="hidden" name="member_no" value="${board.member_no}">
                <input type="hidden" name="member_name" value="${board.member_name}">
                <input type="hidden" name="member_company" value="${board.member_company}">
                <div class="form-group">
                  <label class="col-lg-3 control-label">카테고리</label>
                  <div class="col-lg-8">
                    <select class="form-control select-float" name="category" id="category" data-required="true" style="width: auto;"><option value="">카테고리를 선택해주세요</option>
						<option value="10">의(공동구매)</option>
						<option value="20">의(중고나라)</option>
						<option value="30">식(주변 맛집)</option>
						<option value="40">주(카풀)</option>
						<option value="50">주(룸메)</option>
					</select>	                  
				</div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">회원 닉네임</label><!-- 나중에 id나 닉네임으로 박아둘 것 -->
                  <div class="col-lg-8">
                    ${board.member_name}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">직장명</label>
                  <div class="col-lg-8">
                  	${board.member_company}
                  	<div class="line line-dashed m-t-large"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">제목</label>
                  <div class="col-lg-8">
                    <input type="text" name="title" id="title" value="${board.board_title}" class="bg-focus form-control" data-required="true">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">비밀번호</label>
                  <div class="col-lg-8">
                    <input type="password" name="password" id="password" placeholder="비밀번호를 입력해주세요" class="bg-focus form-control" data-required="true">
                  	<div class="line line-dashed m-t-large"></div>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-lg-3 control-label">내용</label>
                  <div class="col-lg-8">
                    <textarea rows="9" class="form-control" name="contents" id="contents" data-required="true" data-trigger="keyup" data-rangelength="[10,200]">${board.board_content}</textarea>
                  </div>
                </div>
                <div class="form-group" align="center">
                  <div class="col-lg-12 ">                      
                    <button type="submit" class="btn btn-primary">수정</button>
                    <button type="reset" class="btn btn-white">취소</button>
                  </div>
                </div>
                 </form>
        	</div>
            	
          </section>
          </div>
          </div>
          </section>
          </section>     
  
 

</body>
</html>