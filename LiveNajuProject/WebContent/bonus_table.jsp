<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

  <section id="content">
    <section class="main padder">
      <div class="clearfix">
       </div>
     

         <div class="col-lg-6">
        <div class="row">
        <div class="col-xs-6">
          <section class="panel">
            <header class="panel-heading">
                회사별 활동 기부 현황  <i class="fa fa-heart fa-lg"></i>
            </header>
            <div>
              <table class="table table-striped m-b-none text-small">
                <thead>
                  <tr>
                    <th width="100">회사명</th>
                    <th>활동율</th>                    
                 </tr>
                </thead>
                <tbody>
                  <tr>   
                   <td>한전KDN</td>                 
                   <td>
                     <div class="progress progress-small progress-striped active m-t-mini m-b-none">
                       <div class="progress-bar progress-bar-success" data-toggle="tooltip" data-original-title="${main.kdn}%" style="width: ${main.kdn}%"></div>
                     </div>
                   </td>
                    <!-- <td class="text-right"></td> -->
                  </tr>
                  <tr>   
                   <td>한전</td>                 
                   <td>
                     <div class="progress progress-small progress-striped active m-t-mini m-b-none">
                       <div class="progress-bar progress-bar-warning" data-toggle="tooltip" data-original-title="${main.kepco}%" style="width: ${main.kepco}%"></div>
                     </div>
                   </td>
                    <!-- <td class="text-right"></td> -->
                  </tr>
                  <tr>   
                   <td>한전KPS</td>                 
                   <td>
                     <div class="progress progress-small progress-striped active m-t-mini m-b-none">
                       <div class="progress-bar progress-bar-danger" data-toggle="tooltip" data-original-title="${main.kps}%" style="width: ${main.kps}%"></div>
                     </div>
                   </td>
                    <!-- <td class="text-right"></td> -->
                  </tr>
                  <tr>   
                   <td>전력거래소</td>                 
                   <td>
                     <div class="progress progress-small progress-striped active m-t-mini m-b-none">
                       <div class="progress-bar progress-bar-info" data-toggle="tooltip" data-original-title="${main.kpx}%" style="width: ${main.kpx}%"></div>
                     </div>
                   </td>
                    <!-- <td class="text-right"></td> -->
                  </tr>
                  <tr>   
                   <td>농어촌공사</td>                 
                   <td>
                     <div class="progress progress-small progress-striped active m-t-mini m-b-none">
                       <div class="progress-bar progress-bar-success" data-toggle="tooltip" data-original-title="${main.ekr}%" style="width: ${main.ekr}%"></div>
                     </div>
                   </td>
                    <!-- <td class="text-right"></td> -->
                  </tr>
                  <tr>   
                   <td>AT공사</td>                 
                   <td>
                     <div class="progress progress-small progress-striped active m-t-mini m-b-none">
                       <div class="progress-bar progress-bar-warning" data-toggle="tooltip" data-original-title="${main.at}%" style="width: ${main.at}%"></div>
                     </div>
                   </td>
                    <!-- <td class="text-right"></td> -->
                  </tr>
                  <tr>   
                   <td>전파진흥원</td>                 
                   <td>
                     <div class="progress progress-small progress-striped active m-t-mini m-b-none">
                       <div class="progress-bar progress-bar-danger" data-toggle="tooltip" data-original-title="${main.kca}%" style="width: ${main.kca}%"></div>
                     </div>
                   </td>
                    <!-- <td class="text-right"></td> -->
                  </tr>
                
                  </tbody>
              </table>
            </div>
          </section>
        </div>
        
    
    </section>
  </section>


</body>
</html>