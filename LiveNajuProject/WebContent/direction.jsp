<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Directions service</title>
    <style type="text/css">
        html, body {
          height: 100%;
          margin: 0;
          padding: 0;
        }
 
        #map-canvas, #map_canvas {
          height: 100%;
        }
 
        @media print {
          html, body {
            height: auto;
          }
 
          #map_canvas {
            height: 650px;
          }
        }
 
        #panel {
          position: absolute;
          top: 5px;
          left: 50%;
          margin-left: -180px;
          z-index: 5;
          background-color: #fff;
          padding: 5px;
          border: 1px solid #999;
        }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script>
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();
    var map;
    var klocation = new google.maps.LatLng(35.021885, 126.783119);
    var MarkMe;
 
    function initialize() {
      directionsDisplay = new google.maps.DirectionsRenderer();
      //var chicago = new google.maps.LatLng(41.850033, -87.6500523);
      var mapOptions = {
        zoom:7,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: klocation
      }
      
      map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

      MarkMe = new google.maps.Marker({
				position : klocation,
				map : map,
				icon : './icon/man.png'
			});

      directionsDisplay.setMap(map);
      calcRoute();
      
      google.maps.event.addListener(map, 'click', function(event) {
				placeMarkMe(event.latLng);
			});
    }
 
    google.maps.event.addDomListener(window, 'load', initialize);

    function calcRoute() {
 
      var request = {
          origin: MarkMe.getPosition(),
          destination:new google.maps.LatLng(
				 35.0239972,	
				 126.7144487),
          travelMode: eval("google.maps.DirectionsTravelMode.TRANSIT")
      };
      directionsService.route(request, function(response, status) {
        //alert(status);  // È®ÀÎ¿ë Alert..¹Ì»ç¿ë½Ã »èÁ¦
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
      });
    }

  

      function placeMarkMe(location) {
			MarkMe.setPosition(location);
		}
      
    </script>
    </head>
    <body>
        <div id="panel"  >
            <div>
                <strong>목적지까지 경로 보기 : </strong>
                <input type="button" value="길찾기" onclick="Javascript:calcRoute();" />
            </div>
        </div>
        <div id="map-canvas" style="width:750px; height:650px;"></div>
    </body>
</html>
