<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

</head>
<body>
 <section id="content" class="content-sidebar bg-white">
  
    <!-- .sidebar -->
    <section class="main">
      <div class="padder">
        <div class="row">
       
        </div>
      </div>
      <div>
      	<div>
      		<ul class="nav nav-tabs m-b-none no-radius">
       			<li class="active"><a href="#all" data-toggle="tab"><i class="fa fa-bullhorn fa-lg"></i>&nbsp;공지사항</a></li>
        	</ul>
      	</div>
      </div>
    
      <div class="tab-content">
        <div class="tab-pane active" id="all">
          <ul class="list-group list-normal">
            <c:forEach var="list" items="${list}">
            <li class="list-group-item">
              <div>
                <small class="pull-right text-muted">${list.info_date}</small>
                <a href="info_detail.do?info_no= ${list.info_no}">${list.info_title}</a>
             </div>
            </li>
            </c:forEach>
           </ul>
          </div>
          </div>
          
		<c:if test="${session_id == 101}">
      <br>
      	<div class="right-float">
      		<p><a href="info_form.do" class="btn-sm btn-white">글쓰기</a></p>
      	</div> 
      </c:if>
        
    </section>
    
    <!-- /.sidebar -->



    
  </section>
	
  <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" 
   aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" 
               aria-hidden="true">×
            </button>
            <h4 class="modal-title" id="myModalLabel">
               This Modal title
            </h4>
         </div>
         <div class="modal-body">
            Click on close button to check Event functionality.
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" 
               data-dismiss="modal">
               Close
            </button>
            <button type="button" class="btn btn-primary">
               Submit changes
            </button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->





<script type="text/javascript">
	function clothes() {
		//alert(100);
		var url = "board_main_clothes.json";
		$.post(url, load_clothes);
	}
	
	function load_clothes(result) {
		if(result.code == "fail") {
			alert("서버오류 : " + result.msg);
			return;
		}
		//alert(400);
		refresh_list_clothes(result.list);
	}
	
	function refresh_list_clothes(list) {
		//alert(300);
		/* $("#clothes").append("안녕"); */
		$("#clothes ul:ge(0)").remove();
		var s = '<ul class="list-group list-normal">';
		for(var i=0; i < list.length; i++) {
        s += '<li class="list-group-item">';
        s += '<a href="#" class="thumb-small pull-left m-r-small"><img src="images/avatar.jpg" class="img-circle"></a>';
        s += ' <div>';
        s += ' <small class="pull-right text-muted">'+ list[i].board_date +'</small>';
        s += ' <a href="#"><strong>'+ list[i].member_name + '</strong></a>'+ list[i].category_name; 
        s += ' <a href="board_detail.do?board_no=' + list[i].board_no +'">'+ list[i].board_title +'</a>';
        s += '  <div class="text-small">';
        s += '    <a href="#" onclick="count_like('+list[i].board_no+');">좋아요</a>&nbsp;' + list[i].board_like + '&nbsp;&nbsp;&nbsp; <a href="#">댓글달기</a>';
        s += '  </div>';
        s += ' </div>';
        s += '</li>';
		}
      	s += '</ul>';
      	$("#clothes").append(s);
	}
	
	function eat() {
		//alert(100);
		var url = "board_main_eat.json";
		$.post(url, load_eat);
	}
	
	function load_eat(result) {
		if(result.code == "fail") {
			alert("서버오류 : " + result.msg);
			return;
		}
		//alert(400);
		refresh_list_eat(result.list);
	}
	
	function refresh_list_eat(list) {
		$("#eat ul:ge(0)").remove();
		var s = '<ul class="list-group list-normal">';
		for(var i=0; i < list.length; i++) {
        s += '<li class="list-group-item">';
        s += '<a href="#" class="thumb-small pull-left m-r-small"><img src="images/avatar.jpg" class="img-circle"></a>';
        s += ' <div>';
        s += ' <small class="pull-right text-muted">'+ list[i].board_date +'</small>';
        s += ' <a href="#"><strong>'+ list[i].member_name + '</strong></a>'+ list[i].category_name; 
        s += ' <a href="board_detail.do?board_no=' + list[i].board_no +'">'+ list[i].board_title +'</a>';
        s += '  <div class="text-small">';
        s += '    <a href="#"onclick="count_like('+list[i].board_no+');">좋아요</a>&nbsp;' + list[i].board_like + '&nbsp;&nbsp;&nbsp; <a href="#">댓글달기</a>';
        s += '  </div>';
        s += ' </div>';
        s += '</li>';
		}
      	s += '</ul>';
      	$("#eat").append(s);
	}
	
	function home() {
		//alert(100);
		var url = "info_main_home.json";
		$.post(url, load_home);
	}
	
	function load_home(result) {
		if(result.code == "fail") {
			alert("서버오류 : " + result.msg);
			return;
		}
		//alert(400);
		refresh_list_home(result.list);
	}
	
	function refresh_list_home(list) {
		$("#home ul:ge(0)").remove();
		var s = '<ul class="list-group list-normal">';
		for(var i=0; i < list.length; i++) {
        s += '<li class="list-group-item">';
        s += '<a href="#" class="thumb-small pull-left m-r-small"><img src="images/avatar.jpg" class="img-circle"></a>';
        s += ' <div>';
        s += ' <small class="pull-right text-muted">'+ list[i].board_date +'</small>';
        s += ' <a href="#"><strong>'+ list[i].member_name + '</strong></a>'+ list[i].category_name; 
        s += ' <a href="board_detail.do?board_no=' + list[i].board_no +'">'+ list[i].board_title +'</a>';
        s += '  <div class="text-small">';
        s += '    <a href="#"onclick="count_like('+list[i].board_no+');">좋아요</a>&nbsp;' + list[i].board_like + '&nbsp;&nbsp;&nbsp; <a href="#">댓글달기</a>';
        s += '  </div>';
        s += ' </div>';
        s += '</li>';
		}
      	s += '</ul>';
      	$("#home").append(s);
	}
	
</script>






</body>
</html>