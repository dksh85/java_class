<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
  <section id="content">
    <section class="main padder">
      <div class="clearfix">
        <h4><i class="fa fa-bell-o fa-lg"></i>&nbsp;공지사항 내용</h4>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <section class="panel">
            <div class="panel-body">
              <form class="form-horizontal" method="post" data-validate="parsley">    
                
              <c:if test="${session_id == 101}">  -이곳을 관리자의 세션 번호로 픽스시키자--%>
                <div class="form-group" align="right">
                  <div class="col-lg-12 ">                      
                    <%-- <button onclick="location.href='info_updateform.do?info_no=${info.info_no}'" class="btn btn-primary">수정</button>
                    <button onclick="location.href='info_removeform.do?info_no=${info.info_no}'" class="btn btn-white">삭제</button> --%>
                  	<input type="button" value = "수정" class="btn btn-info" onClick="location.href='info_updateform.do?info_no=${info.info_no}'">
                  	<input type="button" value = "삭제" class="btn btn-info" onClick="location.href='info_remove.do?info_no=${info.info_no}'">
                  	
                  	<!-- <input type="button" value = "삭제" class="btn btn-white"   onClick="info_remove.do(${info.info_no});"> -->
                  </div>
                 </div>
               </c:if>
                
                  <div class="form-group">
                  <label class="col-lg-3 control-label">제목</label>
                  
                  <div class="col-lg-8">
                   		${info.info_title}
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">공지 내용</label>
                
                  <div class="col-lg-8">
                   		${info.info_content}
                  </div>
                </div>
                <input type="button" value = "목록으로" class="btn btn-white right-float" onclick="location.href='info_main.do'">
                 </form>
        	</div>
            	
          </section>
          </div>
          </div>
          </section>
          </section>
          
<!--           <script type="text/javascript">
          function detail_remove(info_no) {
        	  var url = "detail_remove.json";
        	  var param = {info_no:info_no};
        	  $.post(url, param, load_detail_remove);
          }
          
          function load_detail_remove(result) {
        	  if(result.code=="fail") {
        		  alert("삭제 오류: " + result.msg);
        		  return;
        	  }
        	  alert("삭제되었습니다.");
        	  location.href="info_main.do";
          }
          </script> -->
</body>
</html>