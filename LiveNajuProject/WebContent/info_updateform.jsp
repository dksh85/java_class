<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
 <section id="content">
    <section class="main padder">
      <div class="clearfix">
        <h4><i class="fa fa-edit"></i>공지사항 수정</h4>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <section class="panel">
            <div class="panel-body">
              <form class="form-horizontal" method="post" data-validate="parsley" action="info_update.do">      
                <input type="hidden" name="info_no" value="${info.info_no}">
                
              
              
                  <div class="form-group">
                  <label class="col-lg-3 control-label">제목</label>
                  <div class="col-lg-8">
                    <input type="text" name="title" id="title" value="${info.info_title}" class="bg-focus form-control" data-required="true">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">비밀번호</label>
                  <div class="col-lg-8">
                    <input type="password" name="password" id="password" placeholder="비밀번호를 입력해주세요" class="bg-focus form-control" data-required="true">
                  	<div class="line line-dashed m-t-large"></div>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-lg-3 control-label">내용</label>
                  <div class="col-lg-8">
                    <textarea rows="9" class="form-control" name="contents" id="contents" data-required="true" data-trigger="keyup" data-rangelength="[10,200]">${info.info_content}</textarea>
                  </div>
                </div>
                <div class="form-group" align="center">
                  <div class="col-lg-12 ">                      
                    <button type="submit" class="btn btn-primary">수정</button>
                    <button type="reset" class="btn btn-white">취소</button>
                  </div>
                </div>
                 </form>
        	</div>
            	
          </section>
          </div>
          </div>
          </section>
          </section>     
  
 

</body>
</html>