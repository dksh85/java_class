<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

  <section id="content2">
      <div class="row">
        <div class="col-lg-4 col-lg-offset-4 m-t-large">
          <section class="panel">
            <header class="panel-heading text-center">
                    로그인
            </header>
            <form action="login.do" class="panel-body">
              <div class="block">
                <label class="control-label">이메일</label>
                <input type="text" name="member_id" id="member_id" placeholder="test@example.com" class="form-control">
              </div>
              <div class="block">
                <label class="control-label">비밀번호</label>
                <input type="password" name="member_pwd" id="member_pwd" placeholder="Password" class="form-control">
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox"> 로그인 상태 유지하기 
                </label>
              </div>
              <a href="#" class="pull-right m-t-mini"><small>비밀번호를 잊으셨습니까?</small></a>
              <button type="submit" class="btn btn-info">로그인</button>
              <div class="line line-dashed"></div>
              <a href="#" class="btn btn-facebook btn-block m-b-small"><i class="fa fa-facebook pull-left"></i>Sign in with Facebook</a>
              <a href="#" class="btn btn-twitter btn-block"><i class="fa fa-twitter pull-left"></i>Sign in with Twitter</a>
              <div class="line line-dashed"></div>
              <p class="text-muted text-center"><small>Do not have an account?</small></p>
              <a href="applyform.do" class="btn btn-white btn-block">회원가입하기</a>
            </form>
            
          </section>
        </div>
        
      </div>
  </section>
 
</body>
</html>