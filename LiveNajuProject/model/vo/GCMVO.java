package push.model.vo;

public class GCMVO {
	private String title = "제목입니다.";
    private String msg = "내용입니다.";
    private String typeCode = "코드입니다.";
    
    // push 결과 정보
    private String regId; // regId
    private boolean pushSuccessOrFailure; // 성공 여부
    private String msgId = ""; // 메시지 ID
    private String errorMsg = ""; // 에러메시지
	public GCMVO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public GCMVO(String title, String msg, String typeCode, String regId, boolean pushSuccessOrFailure, String msgId,
			String errorMsg) {
		super();
		this.title = title;
		this.msg = msg;
		this.typeCode = typeCode;
		this.regId = regId;
		this.pushSuccessOrFailure = pushSuccessOrFailure;
		this.msgId = msgId;
		this.errorMsg = errorMsg;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getTypeCode() {
		return typeCode;
	}
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public boolean isPushSuccessOrFailure() {
		return pushSuccessOrFailure;
	}
	public void setPushSuccessOrFailure(boolean pushSuccessOrFailure) {
		this.pushSuccessOrFailure = pushSuccessOrFailure;
	}
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
    
    
}
