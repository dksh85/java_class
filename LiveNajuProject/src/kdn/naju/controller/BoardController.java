package kdn.naju.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kdn.naju.service.BoardService;
import kdn.naju.service.MemberService;
import kdn.naju.service.ReplyService;
import kdn.naju.util.PageBean;
import kdn.naju.vo.Board;
import kdn.naju.vo.BoardException;
import kdn.naju.vo.Member;
import kdn.naju.vo.Reply;
/**
 * 게시판 목록(board_main.do), 게시글 등록폼(board_form.do), 게시글 등록 후 목록(board_insert.do)
 */
@Controller
public class BoardController {

	@Autowired
	private BoardService boardService;
	@Autowired
	private MemberService memberService;
	
	//회원 게시물 찾기
	@RequestMapping("myBoard.do")
	public String myBoard(
			@RequestParam("member_no") int member_no,
			Model model) {
		Member member = memberService.search(member_no);
		System.out.println("member 한명 : "+member);
		String memberNum = String.valueOf(member_no);
		PageBean pageBean = new PageBean("member_no", memberNum, 1);
		System.out.println(member.getMember_name());
		List<Board> board = boardService.SearchAll(pageBean);
		System.out.println(board);
		model.addAttribute("list", board);
		model.addAttribute("content", "/board.jsp");
		return "header_frame.jsp";
	}
	//전체 게시물 보기
	@RequestMapping("board_main.do")
	public String boardMain(Model model) {
		PageBean pageBean = new PageBean(null, null, 1);
		List<Board> board = boardService.SearchAll(pageBean);
		int allCount = pageBean.getTotal();
		System.out.println(allCount);
		int interval = 10;
		int page = 0;
		if(allCount % interval == 0){
			page = allCount / interval;
			System.out.println(page);
		}else {
			if(allCount < 10)
				page = 1;
			else
				page = (allCount / interval) + 1;
		}
		System.out.println("page:" + page);
		List<String> pageResult = new ArrayList<>();
		for(int i=0; i < page; i++) {
			pageResult.add(i, ""+(i+1)+"");
		}
		System.out.println(board);
		System.out.println(pageResult);
		model.addAttribute("page", pageResult);
		model.addAttribute("list", board);
		model.addAttribute("content", "/board.jsp");
		return "header_frame.jsp";
	}
	
	//전체 게시물 보기
	@RequestMapping("board_main_page.do")
	public String boardMainPage(
			@RequestParam("pageNo") int pageNo,
			Model model) {
		PageBean pageBean = new PageBean(null, null, pageNo);
		List<Board> board = boardService.SearchAll(pageBean);
		int allCount = pageBean.getTotal();
		System.out.println(allCount);
		int interval = 10;
		int page = 0;
		if(allCount % interval == 0){
			page = allCount / interval;
			System.out.println(page);
		}else {
			if(allCount < 10)
				page = 1;
			else
				page = (allCount / interval) + 1;
		}
		System.out.println("page:" + page);
		List<String> pageResult = new ArrayList<>();
		for(int i=0; i < page; i++) {
			pageResult.add(i, ""+(i+1)+"");
		}
		System.out.println(board);
		System.out.println(pageResult);
		model.addAttribute("pageBean", page);
		model.addAttribute("page", pageResult);
		model.addAttribute("list", board);
		model.addAttribute("content", "/board.jsp");
		return "header_frame.jsp";
	}
	
	//게시물 등록폼 보기
	@RequestMapping("board_form.do")
	public String boardForm(Model model) {
		model.addAttribute("content", "/board_insert.jsp");
		return "header_frame.jsp";
	}
	//게시물 상세내용 보기
	@RequestMapping("board_detail.do")
	public String board_detail(@RequestParam("board_no") int board_no, Model model) {
		//System.out.println(content);
		Board board = boardService.search(board_no);
		System.out.println("상세페이지:" + board);
		model.addAttribute("board", board);
		model.addAttribute("content", "/board_detail.jsp");
		model.addAttribute("reply", "/board_reply.jsp");
		return "header_frame.jsp";
	}
	//게시물 수정폼 보기
	@RequestMapping("detail_updateform.do")
	public String detail_updateform(
			@RequestParam("board_no") int board_no,
			Model model
			) {
		System.out.println("업데이트 폼");
		Board board = boardService.search(board_no);
		model.addAttribute("board", board);
		model.addAttribute("content", "/board_updateform.jsp");
		
		return "header_frame.jsp";
	}
	//게시물 수정
	@RequestMapping("detail_update.do")
	public String detail_update(
			@RequestParam("board_no") int board_no,
			@RequestParam("member_no") int member_no,
			@RequestParam("member_name") String member_name,
			@RequestParam("member_company") String member_company,
			@RequestParam("category") int category,
			@RequestParam("title") String title,
			@RequestParam("password") String password,
			@RequestParam("contents") String contents,
			Model model
			) {
		
		try {
			Board board = boardService.search(board_no);
			System.out.println("업데이트중:"+board_no);
			if(board == null)
				throw new BoardException("등록된 게시물이 없습니다.");
			Board nBoard = new Board(board_no, title, member_no, member_name, member_company, contents, password, category);
			boardService.update(nBoard);
			nBoard=boardService.search(board_no);
			model.addAttribute("board", nBoard);
			model.addAttribute("content", "/board_detail.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "header_frame.jsp";
	}

	
	///////////////////////////////////////////////////////
	//////////////////////////////////////////////////////
	//////////////////////////////////////////////////////
	
	//회원 정보 보기
		@RequestMapping("modal_detail.json")
		@ResponseBody
		public Map<String, Object> modal_detail(
				@RequestParam("member_no") int member_no) {
			Map<String, Object> result = new HashMap<String, Object>(); 
			try {
				System.out.println("member_no"+member_no);
				Member member = memberService.search(member_no);
				result.put("member", member);
				result.put("code", "ok");
			} catch (Exception e) {
				e.printStackTrace();
				result.put("code", "fail");
				result.put("msg", e.getMessage());
			}
			return result;
		}
	
	//게시물 등록
	@RequestMapping("board_insert.json")
	@ResponseBody
	public Map<String, Object> board_insert(Board board) {
		Map<String, Object> result = new HashMap<String, Object>(); 
		//System.out.println("category:" + category + ", name:" + name + ", company : " + company +", title:" + title + ", password: " + password + ", contents : " + contents);
		//Board board = new Board(title, name, company, contents, password, category);
		try {
			System.out.println(board);
			boardService.add(board);
			result.put("code", "ok");
			result.put("content", "/board_detail.jsp");
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "fail");
			result.put("msg", e.getMessage());
		}
		return result;
	}
	
	//의-게시물보기
	@RequestMapping("board_main_clothes.json")
	@ResponseBody
	public Map<String, Object> boardMainClothes(
			@RequestParam("pageNo") int pageNo) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			System.out.println("clothes");
			System.out.println("test:" + pageNo);
			PageBean pageBean = new PageBean(null, null, pageNo);
			List<Board> board = boardService.searchClothes(pageBean);
			System.out.println(board);
			result.put("code", "ok");
			result.put("list", board);
			/*model.addAttribute("list", board);
			model.addAttribute("content", "/board.jsp");*/
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "fail");
			result.put("msg", e.getMessage());
		}
	
		return result;
	}
	
	//의-페이지빈
		@RequestMapping("board_clothes_page.json")
		@ResponseBody
		public Map<String, Object> boardClothesPage(
				@RequestParam("pageNo") int pageNo) {
			Map<String, Object> result = new HashMap<String, Object>();
			PageBean pageBean = new PageBean(null, null, pageNo);
			List<Board> board = boardService.searchClothes(pageBean);
			int allCount = pageBean.getTotal();
			System.out.println("json:"+allCount);
			int interval = 10;
			int page = 0;
			if(allCount % interval == 0){
				page = allCount / interval;
			}else {
				if(allCount < 10)
					page = 1;
				else
					page = (allCount / interval) + 1;
			}
			System.out.println("page:" + page);
			List<String> pageResult = new ArrayList<>();
			for(int i=0; i < page; i++) {
				pageResult.add(i, ""+(i+1)+"");
			}
			System.out.println(board);
			System.out.println(pageResult);
			result.put("pageBean", page);
			return result;
		}
	//식-게시물보기
	@RequestMapping("board_main_eat.json")
	@ResponseBody
	public Map<String, Object> boardMainEat(
			@RequestParam("pageNo") int pageNo) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			System.out.println("eat");
			System.out.println("test:" + pageNo);
			PageBean pageBean = new PageBean(null, null, pageNo);
			List<Board> board = boardService.searchEat(pageBean);
			System.out.println(board);
			result.put("code", "ok");
			result.put("list", board);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "fail");
			result.put("msg", e.getMessage());
		}
	
		return result;
	}
	
	//식-페이지빈
		@RequestMapping("board_eat_page.json")
		@ResponseBody
		public Map<String, Object> boardEatPage(
				@RequestParam("pageNo") int pageNo) {
			Map<String, Object> result = new HashMap<String, Object>();
			PageBean pageBean = new PageBean(null, null, pageNo);
			List<Board> board = boardService.searchEat(pageBean);
			int allCount = pageBean.getTotal();
			System.out.println("json:"+allCount);
			int interval = 10;
			int page = 0;
			if(allCount % interval == 0){
				page = allCount / interval;
			}else {
				if(allCount < 10)
					page = 1;
				else
					page = (allCount / interval) + 1;
			}
			System.out.println("page:" + page);
			List<String> pageResult = new ArrayList<>();
			for(int i=0; i < page; i++) {
				pageResult.add(i, ""+(i+1)+"");
			}
			System.out.println(board);
			System.out.println(pageResult);
			result.put("pageBean", page);
			return result;
		}
	//주-게시물보기
	@RequestMapping("board_main_home.json")
	@ResponseBody
	public Map<String, Object> boardMainHome(
			@RequestParam("pageNo") int pageNo) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			System.out.println("home");
			PageBean pageBean = new PageBean(null, null, pageNo);
			List<Board> board = boardService.searchHome(pageBean);
			System.out.println(board);
			result.put("code", "ok");
			result.put("list", board);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "fail");
			result.put("msg", e.getMessage());
		}
	
		return result;
	}
	
	//주-페이지빈
		@RequestMapping("board_home_page.json")
		@ResponseBody
		public Map<String, Object> boardHomePage(
				@RequestParam("pageNo") int pageNo) {
			Map<String, Object> result = new HashMap<String, Object>();
			PageBean pageBean = new PageBean(null, null, pageNo);
			List<Board> board = boardService.searchHome(pageBean);
			int allCount = pageBean.getTotal();
			System.out.println("json:"+allCount);
			int interval = 10;
			int page = 0;
			if(allCount % interval == 0){
				page = allCount / interval;
			}else {
				if(allCount < 10)
					page = 1;
				else
					page = (allCount / interval) + 1;
			}
			System.out.println("page:" + page);
			List<String> pageResult = new ArrayList<>();
			for(int i=0; i < page; i++) {
				pageResult.add(i, ""+(i+1)+"");
			}
			System.out.println(board);
			System.out.println(pageResult);
			result.put("pageBean", page);
			return result;
		}
			
	//모두-게시물보기
	@RequestMapping("board_main_all.json")
	@ResponseBody
	public Map<String, Object> boardMainAll(
			@RequestParam("pageNo") int pageNo) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			System.out.println("all");
			PageBean pageBean = new PageBean(null, null, pageNo);
			List<Board> board = boardService.SearchAll(pageBean);
			System.out.println(board);
			result.put("code", "ok");
			result.put("list", board);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "fail");
			result.put("msg", e.getMessage());
		}
			return result;
	}
		
		//식-페이지빈
			@RequestMapping("board_all_page.json")
			@ResponseBody
			public Map<String, Object> boardAllPage(
					@RequestParam("pageNo") int pageNo) {
				Map<String, Object> result = new HashMap<String, Object>();
				PageBean pageBean = new PageBean(null, null, pageNo);
				List<Board> board = boardService.SearchAll(pageBean);
				int allCount = pageBean.getTotal();
				System.out.println("json:"+allCount);
				int interval = 10;
				int page = 0;
				if(allCount % interval == 0){
					page = allCount / interval;
				}else {
					if(allCount < 10)
						page = 1;
					else
						page = (allCount / interval) + 1;
				}
				System.out.println("page:" + page);
				List<String> pageResult = new ArrayList<>();
				for(int i=0; i < page; i++) {
					pageResult.add(i, ""+(i+1)+"");
				}
				System.out.println(board);
				System.out.println(pageResult);
				result.put("pageBean", page);
				return result;
			}
				
	
	//게시물 삭제
	@RequestMapping("detail_remove.json")
	@ResponseBody
	public Map<String, Object> detail_removeform(
			@RequestParam("board_no") int board_no
			) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			System.out.println("삭제 폼");
			Board board = boardService.search(board_no);
			boardService.remove(board_no);
			result.put("code", "ok");
			result.put("board", board);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "fail");
			result.put("msg", e.getMessage());
		}
		
		return result;
	}
	// 게시글 좋아요 수 증가
	@RequestMapping("count_like.json")
	@ResponseBody
	public Map<String, Object> count_like(
			@RequestParam("board_no") int board_no
			) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Board board = boardService.search(board_no);
			board.setBoard_like(board.getBoard_like() + 1);
			System.out.println("갯수 : "+board.getBoard_like());
			boardService.updateCount(board);
			result.put("code", "ok");
			result.put("board", board);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "fail");
			result.put("msg", e.getMessage());
		}
		
		return result;
	}
	
	// 조건 검색
		@RequestMapping("board_searchAll.json")
		@ResponseBody
		public Map<String, Object> board_searchAll(
				@RequestParam("key") String key,
				@RequestParam("word") String word
				) {
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				PageBean pageBean = new PageBean(key, word, 1);
				List<Board> board = boardService.SearchAll(pageBean);
				if(board.size() <= 0) {
					result.put("msg_ok", "yes");
				}
				result.put("code", "ok");
				result.put("list", board);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("code", "fail");
				result.put("msg", e.getMessage());
			}
			
			return result;
		}
}
