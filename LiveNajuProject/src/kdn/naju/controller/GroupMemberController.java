package kdn.naju.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kdn.naju.service.BoardService;
import kdn.naju.service.GroupMemberService;
import kdn.naju.service.MaptableService;
import kdn.naju.service.ReplyService;
import kdn.naju.util.PageBean;
import kdn.naju.vo.Board;
import kdn.naju.vo.BoardException;
import kdn.naju.vo.GroupMember;
import kdn.naju.vo.GroupMemberException;
import kdn.naju.vo.Member;
import kdn.naju.vo.Reply;

@Controller
public class GroupMemberController {

	@Autowired
	private GroupMemberService groupMemberService;
	@Autowired
	private MaptableService maptableService;
	
	//전체 게시물 보기
	@RequestMapping("count_up.json")
	@ResponseBody
	public Map<String, Object> boardMain(@RequestParam("mark_no") int mark_no,
			@RequestParam("max_count") int max_count,
			HttpSession session) { 	
		Map<String, Object> result = new HashMap<String, Object>();
		int prv_count = 0;
		int member_no = 0;
		try {
			prv_count= groupMemberService.getCount(mark_no);
			member_no = (int)session.getAttribute("session_id");
			System.out.println("member_no:" + member_no);
			GroupMember groupMember = new GroupMember(member_no, mark_no);
			if(prv_count >= max_count) {
				System.out.println("if문");
				result.put("cur_count", prv_count);
				throw new GroupMemberException("인원이 초과하여 그룹에 참여할 수 없습니다.");
			} else {
				System.out.println("else문");
				if(groupMemberService.search(groupMember) == null)
					groupMemberService.add(groupMember);
				else
					throw new GroupMemberException("이미 참여하셨습니다.");
			}
			int cur_count = groupMemberService.getCount(mark_no);
			System.out.println("prv_count:"+ prv_count+"cur_count:" + cur_count);
			List<Member> people = maptableService.searchPeopleList(mark_no);
			System.out.println("people : "+ people);
			result.put("person", people);
			result.put("cur_count", cur_count);
			result.put("code", "ok");
			result.put("msg", "해당 그룹에 참여 신청이 되었습니다.\n 인원이 충원될 경우 알림메세지가 전달됩니다.");
		} catch (GroupMemberException e) {
			e.printStackTrace();
			result.put("code", "fail");
			result.put("msg", e.getMessage());
		}
		
		return result;
	}
	
}
