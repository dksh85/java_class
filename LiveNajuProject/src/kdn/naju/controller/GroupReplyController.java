package kdn.naju.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kdn.naju.service.GroupReplyService;
import kdn.naju.util.PageBean;
import kdn.naju.vo.BoardException;
import kdn.naju.vo.GroupReply;

@Controller
public class GroupReplyController {

	@Autowired
	private GroupReplyService groupReplyService;
	
	//댓글 리스트 호출
	@RequestMapping("group_reply_list.json")
	@ResponseBody
	public Map<String, Object> reply_list(@RequestParam("mark_no") int mark_no) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			System.out.println("reply");
			PageBean pageBean = new PageBean(null, null, 1);
			List<GroupReply> reply = groupReplyService.searchGroupReply(pageBean, mark_no);
			System.out.println(reply);
			result.put("code", "ok");
			result.put("list", reply);
			/*model.addAttribute("list", board);
			model.addAttribute("content", "/board.jsp");*/
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "fail");
			result.put("msg", e.getMessage());
		}
		return result;
	}
	
	//댓글 등록
	@RequestMapping("group_reply_insert.json")
	@ResponseBody
	public Map<String, Object> reply_insert(
			@RequestParam("mark_no") int mark_no,
			@RequestParam("reply_content") String reply_content,
			HttpSession session
			) {
		Map<String, Object> result = new HashMap<String, Object>(); 
		try {
			System.out.println(mark_no);
			int member_no = (int)session.getAttribute("session_id");
			System.out.println(member_no);
			GroupReply reply = new GroupReply(reply_content, mark_no, member_no);
			groupReplyService.add(reply);
			result.put("code", "ok");
			result.put("content", "/board_detail.jsp");
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "fail");
			result.put("msg", e.getMessage());
		}
		return result;
	}
	
	//댓글 삭제
		@RequestMapping("group_reply_remove.json")
		@ResponseBody
		public Map<String, Object> detail_removeform(
				@RequestParam("reply_no") int reply_no
				) {
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				System.out.println("삭제 폼");
				GroupReply reply = groupReplyService.search(reply_no);
				groupReplyService.remove(reply_no);
				result.put("code", "ok");
				result.put("reply", reply);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("code", "fail");
				result.put("msg", e.getMessage());
			}
			return result;
		}
	
	// 댓글 좋아요 수 증가
		@RequestMapping("group_reply_like.json")
		@ResponseBody
		public Map<String, Object> reply_like(
				@RequestParam("reply_no") int reply_no
				) {
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				System.out.println("그룹 댓글 순서:"+reply_no);
				GroupReply reply = groupReplyService.search(reply_no);
				System.out.println("그룹 추출데이터 : "+reply);
				reply.setReply_like(reply.getReply_like() + 1);
				System.out.println("갯수 : "+reply.getReply_like());
				groupReplyService.updateCount(reply);
				result.put("code", "ok");
				result.put("reply", reply);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("code", "fail");
				result.put("msg", e.getMessage());
			}
			
			return result;
		}
		
		/*//댓글 보기
		@RequestMapping("group_reply.do")
		public String boardReply(
			@RequestParam("mark_no") int board_no,
			Model model,
			HttpSession session) {
		try {
			int session_id = 0;
			session_id =(int)session.getAttribute("session_id");
			if(session_id == 0) {
				throw new BoardException("로그인 후 이용해주세요.");
			}
			PageBean pageBean = new PageBean(null, null, 1);
			List<Reply> reply = replyService.searchReply(pageBean, board_no);
			model.addAttribute("list", reply);
			model.addAttribute("content", "/board_reply.jsp");
		} catch (BoardException e) {
			model.addAttribute("msg", e.getMessage());
			e.printStackTrace();
		}
		return "header_frame.jsp";
		}*/
}
