package kdn.naju.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kdn.naju.service.InfoService;
import kdn.naju.util.PageBean;
import kdn.naju.vo.Info;
import kdn.naju.vo.InfoException;

/**
 * 게시판 목록(info_main.do), 게시글 등록폼(info_form.do), 게시글 등록 후 목록(info_insert.do)
 */

@Controller
public class InfoController {

	@Autowired
	private InfoService infoService;
	
	
	@RequestMapping("info_main.do")
	public String infoMain(Model model) {
		PageBean pageBean = new PageBean(null, null, 1);
		List<Info> info = infoService.SearchAl(pageBean);
		model.addAttribute("list", info);
		model.addAttribute("content", "/info.jsp");
		return "header_frame.jsp";
	}
	
    
    @RequestMapping("info_form.do")
	public String info_form(Model model) {
		model.addAttribute("content", "/info_insert.jsp");
		return "header_frame.jsp"; 
	}
	
    
    @RequestMapping("info_insert.do")
            public String info_insert(
			@RequestParam("title") String title,
			@RequestParam("password") String password,
			@RequestParam("contents") String contents,
			Model model
			) {
            Info ninfo = new Info(title, contents, password);
	        System.out.println(ninfo);
			infoService.add(ninfo);
            model.addAttribute("info", ninfo);
			model.addAttribute("content", "/info_complete.html");
	
		
		return "header_frame.jsp";
	}

    
    
    	
    @RequestMapping("info_detail.do")
	public String info_detail(@RequestParam("info_no") int info_no, Model model) {
		//System.out.println(content);
		Info info = infoService.search(info_no);
		System.out.println("상세페이지:" + info);
		model.addAttribute("info", info);
		model.addAttribute("content", "/info_detail.jsp");
		return "header_frame.jsp";
	}
	
	@RequestMapping("info_updateform.do")
	public String info_updateform(
			@RequestParam("info_no") int info_no,
			Model model
			) {
		System.out.println("업데이트 폼");
		Info info = infoService.search(info_no);
		model.addAttribute("info", info);
		model.addAttribute("content", "/info_updateform.jsp");
		
		return "header_frame.jsp";
	}
	
	
	
	@RequestMapping("info_update.do")
	public String info_update(
			@RequestParam("info_no") int info_no,
			@RequestParam("title") String title,
			@RequestParam("password") String password,
			@RequestParam("contents") String contents,
			Model model
			) {
            try {
            	System.out.println(info_no);
            	System.out.println(title);
            	System.out.println(password);
            	System.out.println(contents);
			Info info = infoService.search(info_no);
			System.out.println("업데이트중:"+info_no);
		    if(info == null)
				throw new InfoException("등록된 게시물이 없습니다.");
			Info ninfo = new Info(info_no, title, contents, password);
			System.out.println("객체:" + ninfo);
			infoService.update(ninfo);
			ninfo=infoService.search(info_no);
			System.out.println("업데이트:"+ninfo);
			model.addAttribute("info", ninfo);
			model.addAttribute("content", "/info_detail.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "header_frame.jsp";
	}
		
	    
	    @RequestMapping("info_complete.do")
		public String info_complete(Model model) {
			model.addAttribute("content", "/info_main.jsp");
			return "header_frame.jsp"; 
		}
	    
	    
	    @RequestMapping("info_remove.do")
		public String info_remove(@RequestParam("info_no") int info_no, Model model) {
	    	Info info = infoService.search(info_no);
	    	infoService.remove(info_no);
			model.addAttribute("info", info);
			model.addAttribute("content", "/info_remove_ok.html");
			return "header_frame.jsp";
	    }

	    
	    
/*	//게시물 등록
	@RequestMapping("info_insert.json")
	@ResponseBody
	public Map<String, Object> info_insert(Info info) {
		Map<String, Object> result = new HashMap<String, Object>(); 
		//System.out.println("category:" + category + ", name:" + name + ", company : " + company +", title:" + title + ", password: " + password + ", contents : " + contents);
		//Board board = new Board(title, name, company, contents, password, category);
		try {
			infoService.add(info);
			result.put("code", "ok");
			result.put("content", "/info_detail.jsp");
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "fail");
			result.put("msg", e.getMessage());
		}
		return result;
	}
	
	//게시물 삭제
		@RequestMapping("info_remove.json") //
		@ResponseBody
		public Map<String, Object> info_remove(
				@RequestParam("info_no") int info_no
				) {
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				System.out.println("삭제 폼");
				Info info = infoService.search(info_no);
				infoService.remove(info_no);
				result.put("code", "ok");
				result.put("Info", info);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("code", "fail");
				result.put("msg", e.getMessage());
			}
			
			return result;
		}
		
	
	
	
	
	
	
	*/
	
	
	
	
	
}
	