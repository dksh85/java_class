package kdn.naju.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kdn.naju.service.BoardService;
import kdn.naju.service.ReplyService;
import kdn.naju.util.PageBean;
import kdn.naju.vo.Board;
import kdn.naju.vo.BoardException;
import kdn.naju.vo.Reply;

@Controller
public class ReplyController {

	@Autowired
	private ReplyService replyService;
	
	//댓글 리스트 호출
	@RequestMapping("reply_list.json")
	@ResponseBody
	public Map<String, Object> reply_list(@RequestParam("board_no") int board_no) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			System.out.println("reply");
			PageBean pageBean = new PageBean(null, null, 1);
			List<Reply> reply = replyService.searchReply(pageBean, board_no);
			System.out.println(reply);
			result.put("code", "ok");
			result.put("list", reply);
			/*model.addAttribute("list", board);
			model.addAttribute("content", "/board.jsp");*/
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "fail");
			result.put("msg", e.getMessage());
		}
		return result;
	}
	
	//댓글 등록
	@RequestMapping("reply_insert.json")
	@ResponseBody
	public Map<String, Object> reply_insert(
			@RequestParam("board_no") int board_no,
			@RequestParam("reply_content") String reply_content,
			HttpSession session
			) {
		Map<String, Object> result = new HashMap<String, Object>(); 
		//System.out.println("category:" + category + ", name:" + name + ", company : " + company +", title:" + title + ", password: " + password + ", contents : " + contents);
		//Board board = new Board(title, name, company, contents, password, category);
		try {
			//System.out.println(board);
			System.out.println(board_no);
			int member_no = (int)session.getAttribute("session_id");
			System.out.println(member_no);
			Reply reply = new Reply(reply_content, board_no, member_no);
			replyService.add(reply);
			result.put("code", "ok");
			result.put("content", "/board_detail.jsp");
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "fail");
			result.put("msg", e.getMessage());
		}
		return result;
	}
	
	//댓글 삭제
		@RequestMapping("reply_remove.json")
		@ResponseBody
		public Map<String, Object> detail_removeform(
				@RequestParam("reply_no") int reply_no
				) {
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				System.out.println("삭제 폼");
				Reply reply = replyService.search(reply_no);
				replyService.remove(reply_no);
				result.put("code", "ok");
				result.put("reply", reply);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("code", "fail");
				result.put("msg", e.getMessage());
			}
			return result;
		}
	
	// 댓글 좋아요 수 증가
		@RequestMapping("reply_like.json")
		@ResponseBody
		public Map<String, Object> reply_like(
				@RequestParam("reply_no") int reply_no
				) {
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				System.out.println("댓글 순서:"+reply_no);
				Reply reply = replyService.search(reply_no);
				System.out.println("추출데이터 : "+reply);
				reply.setReply_like(reply.getReply_like() + 1);
				System.out.println("갯수 : "+reply.getReply_like());
				replyService.updateCount(reply);
				result.put("code", "ok");
				result.put("reply", reply);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("code", "fail");
				result.put("msg", e.getMessage());
			}
			
			return result;
		}
		
		//댓글 보기
		@RequestMapping("board_reply.do")
		public String boardReply(
			@RequestParam("board_no") int board_no,
			Model model,
			HttpSession session) {
		try {
			int session_id = 0;
			session_id =(int)session.getAttribute("session_id");
			if(session_id == 0) {
				throw new BoardException("로그인 후 이용해주세요.");
			}
			PageBean pageBean = new PageBean(null, null, 1);
			List<Reply> reply = replyService.searchReply(pageBean, board_no);
			model.addAttribute("list", reply);
			model.addAttribute("content", "/board_reply.jsp");
		} catch (BoardException e) {
			model.addAttribute("msg", e.getMessage());
			e.printStackTrace();
		}
		return "header_frame.jsp";
		}
}
