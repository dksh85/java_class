package kdn.naju.controller;

import java.io.FileOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.fileupload.*;

@Controller
public class UploadController {
	@RequestMapping(value = "fileupload.do", method = RequestMethod.POST)
	public String upload(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("file") MultipartFile file) throws Exception {

		System.out.println(">>UploadController upload");
		System.out.println("title : " + request.getParameter("title"));
		System.out.println("user : " + request.getParameter("user"));

		System.out.println("file name : " + file.getOriginalFilename());
		if (file != null) {
			// String path =
			// FileConfig.getInstance().getProperty("WEB_ROOT_PATH") +
			// FileConfig.getInstance().getProperty("upload");
			String path = request.getSession().getServletContext().getRealPath("upload");
			System.out.println("path : " + path);

			byte[] data = file.getBytes();

			// FileOutputStream out = new
			// FileOutputStream("C:\\Users\\KOSTA\\workspace\\jqueryWEB\\webapps\\upload\\"+file.getOriginalFilename())
			// ;
			FileOutputStream out = new FileOutputStream(path + "/" + file.getOriginalFilename());
			out.write(data);

		}
		request.setAttribute("load", file.getOriginalFilename());

		return "main.jsp";
	}

}
