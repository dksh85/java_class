package kdn.naju.dao;

import java.util.List;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Board;
import kdn.naju.vo.Reply;

public interface BoardDao {
	public void add(Board board);
	public void update(Board board);
	public void updateCount(Board board);
	public void remove(int board_no);
	public Board search(int board_no);
	public List<Board> searchAll(PageBean pageBean);
	public List<Board> searchClothes(PageBean pageBean);
	public List<Board> searchEat(PageBean pageBean);
	public List<Board> searchHome(PageBean pageBean);
	public int getCount(PageBean pageBean);
	public int getCountClothes(PageBean pageBean);
	public int getCountEat(PageBean pageBean);
	public int getCountHome(PageBean pageBean);
}
