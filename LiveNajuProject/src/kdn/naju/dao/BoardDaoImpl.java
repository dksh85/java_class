package kdn.naju.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Board;
import kdn.naju.vo.Reply;
@Repository
public class BoardDaoImpl implements BoardDao {
	@Autowired
	private SqlSessionTemplate sqlTemplate;
	
	@Override
	public void add(Board board) {
		int row = sqlTemplate.insert("insertBoard", board);
	}

	@Override
	public void update(Board board) {
		int row = sqlTemplate.update("updateBoard", board);
	}
	
	@Override
	public void updateCount(Board board) {
		int row = sqlTemplate.update("updateCountBoard", board);
	}

	
	@Override
	public void remove(int board_no) {
		int row = sqlTemplate.delete("deleteBoard", board_no);
	}
	
	@Override
	public Board search(int board_no) {
		Board board = sqlTemplate.selectOne("selectOneBoard", board_no);
		return board;
	}

	@Override
	public List<Board> searchAll(PageBean pageBean) {
		List<Board> boardList = sqlTemplate.selectList("selectAll", pageBean);
		return boardList;
	}

	@Override
	public List<Board> searchClothes(PageBean pageBean) {
		List<Board> boardList = sqlTemplate.selectList("selectClothes", pageBean);
		return boardList;
	}

	@Override
	public List<Board> searchEat(PageBean pageBean) {
		List<Board> boardList = sqlTemplate.selectList("selectEat", pageBean);
		return boardList;
	}

	@Override
	public List<Board> searchHome(PageBean pageBean) {
		List<Board> boardList = sqlTemplate.selectList("selectHome", pageBean);
		return boardList;
	}

	@Override
	public int getCount(PageBean pageBean) {
		int count = sqlTemplate.selectOne("getCountBoard", pageBean);
		return count;
	}
	@Override
	public int getCountClothes(PageBean pageBean) {
		int count = sqlTemplate.selectOne("getCountClothes", pageBean);
		return count;
	}
	@Override
	public int getCountEat(PageBean pageBean) {
		int count = sqlTemplate.selectOne("getCountEat", pageBean);
		return count;
	}
	@Override
	public int getCountHome(PageBean pageBean) {
		int count = sqlTemplate.selectOne("getCountHome", pageBean);
		return count;
	}

}
