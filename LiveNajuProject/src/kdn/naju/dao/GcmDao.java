package kdn.naju.dao;

import java.util.ArrayList;
import java.util.List;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Gcm;

public interface GcmDao {
	public String getGCMNo(int memberId);
	public void add(Gcm gcm);
	public List<Gcm> search_keys(int groupId);
	
	List<Gcm> searchAll(PageBean pageBean);
}
