package kdn.naju.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Gcm;

@Repository
public class GcmDaoImpl implements GcmDao{
	
	@Autowired
	private SqlSessionTemplate sqlTemplate;
	
	@Override
	public String getGCMNo(int memberId){
		 String GcmKey = sqlTemplate.selectOne("getGCMNo", memberId);
		 return GcmKey;
	}
	
	public void add(Gcm gcm){
		sqlTemplate.insert("insertGCM", gcm);
	}
	
	public List<Gcm> searchAll(PageBean pageBean){
		List<Gcm> gcmList = sqlTemplate.selectList("GcmSelectAl", pageBean);
		return gcmList;
	}
	public List<Gcm> search_keys(int groupId){
		List<Gcm> gcmList = sqlTemplate.selectList("getGCMKey", groupId);
		System.out.println("in dao = "+ gcmList);
		return gcmList;
	}
}
