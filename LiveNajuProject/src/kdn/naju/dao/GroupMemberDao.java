package kdn.naju.dao;

import java.util.List;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Board;
import kdn.naju.vo.GroupMember;
import kdn.naju.vo.Reply;

public interface GroupMemberDao {
	public int getCount(int mark_no);

	public void add(GroupMember groupMember);

	public GroupMember search(GroupMember groupMember);
}
