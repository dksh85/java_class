package kdn.naju.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Board;
import kdn.naju.vo.GroupMember;
import kdn.naju.vo.Reply;
@Repository
public class GroupMemberDaoImpl implements GroupMemberDao {
	@Autowired
	private SqlSessionTemplate sqlTemplate;

	@Override
	public int getCount(int mark_no) {
		int cur_count = sqlTemplate.selectOne("getCount", mark_no);
		return cur_count;
	}

	@Override
	public void add(GroupMember groupMember) {
		sqlTemplate.insert("insertGroupMember", groupMember);
	}

	@Override
	public GroupMember search(GroupMember groupMember) {
		GroupMember checkGroup = sqlTemplate.selectOne("selectOneGroup", groupMember);
		return checkGroup;
	}
	

}
