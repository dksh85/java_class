package kdn.naju.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kdn.naju.util.PageBean;
import kdn.naju.vo.GroupReply;
import kdn.naju.vo.Reply;
@Repository
public class GroupReplyDaoImpl implements GroupReplyDao {
	@Autowired
	private SqlSessionTemplate sqlTemplate;
	
	@Override
	public void add(GroupReply reply) {
		int row = sqlTemplate.insert("insertGroupReply", reply);
	}

	@Override
	public void update(GroupReply reply) {
		int row = sqlTemplate.update("updateGroupReply", reply);
	}
	
	@Override
	public void updateCount(GroupReply reply) {
		int row = sqlTemplate.update("updateGroupCountReply", reply);
	}

	
	@Override
	public void remove(int reply_no) {
		int row = sqlTemplate.delete("deleteGroupReply", reply_no);
	}
	
	@Override
	public GroupReply search(int reply_no) {
		GroupReply reply = sqlTemplate.selectOne("selectOneGroupReply", reply_no);
		return reply;
	}

	@Override
	public List<GroupReply> searchReply(PageBean pageBean, int mark_no) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("mark_no" , mark_no);
		map.put("start", pageBean.getStart());
		map.put("end", pageBean.getEnd());
		List<GroupReply> replyList = sqlTemplate.selectList("selectGroupReply", map);
		return replyList;
	}
}
