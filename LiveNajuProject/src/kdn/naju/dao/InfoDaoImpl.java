package kdn.naju.dao;
import java.util.List;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Info;

@Repository
public class InfoDaoImpl implements InfoDao {
	@Autowired
	private SqlSessionTemplate sqlTemplate;

	@Override
	public void add(Info info) {
		// TODO Auto-generated method stub
		int row = sqlTemplate.insert("insertinfo", info);
		
	}

	@Override
	public void update(Info info) {
		// TODO Auto-generated method stub
		int row = sqlTemplate.update("updateinfo", info);
	}

	@Override
	public void remove(int info_no) {
		// TODO Auto-generated method stub
		int row = sqlTemplate.delete("deleteinfo", info_no);
	}

	@Override
	public Info search(int info_no) {
		// TODO Auto-generated method stub
		Info info = sqlTemplate.selectOne("selectOneinfo", info_no);
		return info;
	}

	@Override
	public List<Info> searchAl(PageBean pageBean) {
		List<Info> infoList = sqlTemplate.selectList("selectAl", pageBean);
		return infoList;
	}

	
	


}
