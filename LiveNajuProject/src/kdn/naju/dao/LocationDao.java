package kdn.naju.dao;

import java.util.List;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Location;

public interface LocationDao {

	void add(Location location);

	List<Location> searchAll(PageBean pageBean);

}
