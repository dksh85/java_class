package kdn.naju.dao;

public interface MainDao {
	public int activeCount(String member_company);
	public int activeCountAll();
}
