package kdn.naju.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
@Repository
public class MainDaoImpl implements MainDao {
	
	@Autowired
	SqlSessionTemplate sqlTemplate;

	@Override
	public int activeCount(String member_company) {
		int count = sqlTemplate.selectOne("activeCount", member_company);
		return count;
	}

	@Override
	public int activeCountAll() {
		int count = sqlTemplate.selectOne("activeAll");
		return count;
	}

}
