package kdn.naju.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Maptable;
import kdn.naju.vo.Member;

@Repository
public class MaptableDaoImpl implements MaptableDao {

	@Autowired
	private SqlSessionTemplate sessionTemplate;
	
	@Override
	public Maptable search(int mark_no) {
		Maptable maptable = sessionTemplate.selectOne("selectOneMap", mark_no);
		return maptable;
	}


	@Override
	public void addmark(Maptable maptable) {
		int rows = sessionTemplate.insert("insertMaptable", maptable);
		
		
	}

	@Override
	public void delete(int mark_no) {
		int rows = sessionTemplate.delete("deleteMaptable", mark_no);
		
	}
	
	

	@Override
	public List<Maptable> searchAll(PageBean pageBean) {
		String key = pageBean.getKey();
		String word = pageBean.getWord();
		
		List<Maptable> list = null;
		Map<String, Object> map = new HashMap<>();
		map.put("start", pageBean.getStart());
		map.put("end", pageBean.getEnd());
		map.put("key", key);
		map.put("word", word);

		list = sessionTemplate.selectList("selectAll_mark", map);
		
		return list;		
	}
	@Override
	public List<Maptable> selectMaptables(PageBean pageBean,double radius,double lat, double lng, int category_no) {
		String key = pageBean.getKey();
		String word = pageBean.getWord();
		
		List<Maptable> list = null;
		Map<String, Object> map = new HashMap<>();
		map.put("start", pageBean.getStart());
		map.put("end", pageBean.getEnd());
		map.put("key", key);
		map.put("word", word);
		map.put("radius", radius);
		map.put("lat", lat);
		map.put("lng", lng);
		map.put("category_no", category_no);
		
		list = sessionTemplate.selectList("selectMaptables", map);
		
		return list;		
	}


	@Override
	public Maptable selectgroupname(String groupname) {
	
		Maptable maptable = sessionTemplate.selectOne("selectOneGroupname", groupname);
		System.out.println("그룹네임으로 그룹찾기.."+maptable);
		return maptable;
		
		
	}
	
	@Override
	public List<Member> searchPeopleList(int mark_no) {
		List<Member> mapList = sessionTemplate.selectList("selectPeople", mark_no);
		System.out.println("왔나");
		System.out.println("mapList");
		return mapList;
	}


}
