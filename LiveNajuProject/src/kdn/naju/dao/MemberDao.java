package kdn.naju.dao;

import java.util.List;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Member;

public interface MemberDao {

	void add(Member member);

	Member search(int member_no);

	void delete(int member_no);

	List<Member> searchAll(PageBean pageBean);

	void update(Member member);

	Member search_id(String member_id);
}
