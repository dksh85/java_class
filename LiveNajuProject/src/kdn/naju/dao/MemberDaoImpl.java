package kdn.naju.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Member;
@Repository
public class MemberDaoImpl implements MemberDao {

	@Autowired
	private SqlSessionTemplate sessionTemplate;

	@Override
	public void add(Member member) {

		int rows = sessionTemplate.insert("insertMember", member);
		
	}

	@Override
	public Member search(int member_no) {

		Member member=null;
		member = sessionTemplate.selectOne("selectOne", member_no);
		return member;
	}

	@Override
	public void delete(int member_no) {
		int rows = sessionTemplate.delete("deleteMember", member_no);
		
	}

	@Override
	public List<Member> searchAll(PageBean pageBean) {
		String key = pageBean.getKey();
		String word = pageBean.getWord();
		
		List<Member> list = null;
		Map<String, Object> map = new HashMap<>();
		map.put("start", pageBean.getStart());
		map.put("end", pageBean.getEnd());
		map.put("key", key);
		map.put("word", word);

		list = sessionTemplate.selectList("selectAll", map);
		
		return list;		
	}

	@Override
	public void update(Member member) {


			int rows = sessionTemplate.update("updateMember", member);
		
	}

	@Override
	public Member search_id(String member_id) {
		
		Member member = sessionTemplate.selectOne("selectMember_id", member_id);
		System.out.println(member);
		
		return member;
	}
	
}
