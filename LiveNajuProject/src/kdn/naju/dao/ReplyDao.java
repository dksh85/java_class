package kdn.naju.dao;

import java.util.List;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Board;
import kdn.naju.vo.Reply;

public interface ReplyDao {
	public void add(Reply reply);
	public void update(Reply reply);
	public void updateCount(Reply reply);
	public void remove(int reply_no);
	public Reply search(int reply_no);
	public List<Reply> searchReply(PageBean pageBean, int board_no);
}
