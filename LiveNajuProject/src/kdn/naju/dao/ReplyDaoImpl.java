package kdn.naju.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Board;
import kdn.naju.vo.Reply;
@Repository
public class ReplyDaoImpl implements ReplyDao {
	@Autowired
	private SqlSessionTemplate sqlTemplate;
	
	@Override
	public void add(Reply reply) {
		int row = sqlTemplate.insert("insertReply", reply);
	}

	@Override
	public void update(Reply reply) {
		int row = sqlTemplate.update("updateReply", reply);
	}
	
	@Override
	public void updateCount(Reply reply) {
		int row = sqlTemplate.update("updateCountReply", reply);
	}

	
	@Override
	public void remove(int reply_no) {
		int row = sqlTemplate.delete("deleteReply", reply_no);
	}
	
	@Override
	public Reply search(int reply_no) {
		Reply reply = sqlTemplate.selectOne("selectOneReply", reply_no);
		return reply;
	}

	@Override
	public List<Reply> searchReply(PageBean pageBean, int board_no) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("board_no" , board_no);
		map.put("start", pageBean.getStart());
		map.put("end", pageBean.getEnd());
		List<Reply> replyList = sqlTemplate.selectList("selectReply", map);
		return replyList;
	}

}
