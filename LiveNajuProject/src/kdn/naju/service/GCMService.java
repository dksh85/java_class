package kdn.naju.service;

import java.util.List;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Gcm;
import push.model.vo.GCMVO;

public interface GCMService {
	public void add(Gcm gcm);
	public String searchGCMKey();
	public List<Gcm> SearchAllGCMKey(PageBean pageBean);
	public List<Gcm> SearchKeys(int group_id);
}
