package kdn.naju.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kdn.naju.dao.GcmDao;
import kdn.naju.util.PageBean;
import kdn.naju.vo.Gcm;
import push.model.vo.GCMVO;

@Service
public class GCMServiceImpl implements GCMService {

	@Autowired
	GcmDao gsmDao;

	@Override
	public void add(Gcm gcm) {
		gsmDao.add(gcm);
		System.out.println(gcm);
	}

	@Override
	public String searchGCMKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Gcm> SearchAllGCMKey(PageBean pageBean) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<Gcm> SearchKeys(int group_id){
		List<Gcm> gcmList = gsmDao.search_keys(group_id);
		return gcmList;
	}
	
}
