package kdn.naju.service;

import kdn.naju.vo.GroupMember;
import kdn.naju.vo.Maptable;

public interface GroupMemberService {
	public int getCount(int mark_no);

	public void add(GroupMember groupMember);

	public GroupMember search(GroupMember groupMember);
	
}
