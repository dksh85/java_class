package kdn.naju.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kdn.naju.dao.GroupMemberDao;
import kdn.naju.vo.GroupMember;

@Service
public class GroupMemberServiceImpl implements GroupMemberService {

	@Autowired
	private GroupMemberDao groupMemberDao;

	@Override
	public int getCount(int mark_no) {
		int cur_count = groupMemberDao.getCount(mark_no);
		return cur_count;
	}

	@Override
	public void add(GroupMember groupMember) {
		groupMemberDao.add(groupMember);
	}

	@Override
	public GroupMember search(GroupMember groupMember) {
		GroupMember checkGroup = groupMemberDao.search(groupMember);
		return checkGroup;
	}

	
	
}
