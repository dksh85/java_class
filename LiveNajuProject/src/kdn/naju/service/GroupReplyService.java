package kdn.naju.service;

import java.util.List;

import kdn.naju.util.PageBean;
import kdn.naju.vo.GroupReply;

public interface GroupReplyService {
	public void add(GroupReply reply);
	public void update(GroupReply reply);
	public void updateCount(GroupReply reply);
	public void remove(int reply_no);
	public GroupReply search(int reply_no);
	public List<GroupReply> searchGroupReply(PageBean pageBean, int mark_no);
}
