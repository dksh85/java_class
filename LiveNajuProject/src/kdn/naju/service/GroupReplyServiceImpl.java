package kdn.naju.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kdn.naju.dao.GroupReplyDao;
import kdn.naju.util.PageBean;
import kdn.naju.vo.GroupReply;
@Service
public class GroupReplyServiceImpl implements GroupReplyService {

	@Autowired
	private GroupReplyDao replyDao;
	
	@Override
	public void add(GroupReply reply) {
		replyDao.add(reply);
	}
	
	@Override
	public void update(GroupReply reply) {
		replyDao.update(reply);
	}
	
	@Override
	public void updateCount(GroupReply reply) {
		replyDao.updateCount(reply);
	}
	
	@Override
	public void remove(int reply_no) {
		replyDao.remove(reply_no);
	}

	@Override
	public GroupReply search(int reply_no) {
		GroupReply reply = replyDao.search(reply_no);
		return reply;
	}
	
	@Override
	public List<GroupReply> searchGroupReply(PageBean pageBean, int mark_no) {
		List<GroupReply> replyList = replyDao.searchReply(pageBean, mark_no);
		return replyList;
	}

}
