package kdn.naju.service;

import java.util.List;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Info;

public interface InfoService {


	public void add(Info info);
	public void update(Info info);
	public void remove(int info_no);
	public Info search(int info_no);
	public List<Info> SearchAl(PageBean pageBean);

}
