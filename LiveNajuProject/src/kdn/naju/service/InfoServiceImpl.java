package kdn.naju.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kdn.naju.dao.BoardDao;
import kdn.naju.dao.InfoDao;
import kdn.naju.util.PageBean;
import kdn.naju.vo.Board;
import kdn.naju.vo.BoardException;
import kdn.naju.vo.Info;

@Service
public class InfoServiceImpl implements InfoService{


	@Autowired
	private InfoDao infoDao;

	@Override
	public void add(Info info) {
		// TODO Auto-generated method stub
		infoDao.add(info);
		
	}

	@Override
	public void update(Info info) {
		// TODO Auto-generated method stub
		infoDao.update(info);
		
	}

	@Override
	public void remove(int info_no) {
		// TODO Auto-generated method stub
		infoDao.remove(info_no);
		
	}

	@Override
	public Info search(int info_no) {
		// TODO Auto-generated method stub
		Info info = infoDao.search(info_no);
		return info;
	}

	@Override
	public List<Info> SearchAl(PageBean pageBean) {
		List<Info> infoList = infoDao.searchAl(pageBean);
		return infoList;
	}


	


	
}