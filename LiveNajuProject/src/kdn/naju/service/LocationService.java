package kdn.naju.service;

import java.util.List;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Location;
import kdn.naju.vo.Member;

public interface LocationService {

    public void regist(Location location);
	
	public List<Location> searchAll(PageBean pageBean);

	public Member search(int location_no);

	
	
}
