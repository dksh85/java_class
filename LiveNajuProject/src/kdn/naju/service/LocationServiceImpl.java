package kdn.naju.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kdn.naju.dao.LocationDao;
import kdn.naju.util.PageBean;
import kdn.naju.vo.Location;
import kdn.naju.vo.Member;


@Service
public class LocationServiceImpl implements LocationService{

	@Autowired
	private LocationDao locationDao;
	
	@Override
	public void regist(Location location) {
		locationDao.add(location);
		
		
	}

	@Override
	public List<Location> searchAll(PageBean pageBean) {
		// TODO Auto-generated method stub
		return locationDao.searchAll(pageBean);
	}

	@Override
	public Member search(int location_no) {
		// TODO Auto-generated method stub
		return null;
	}

}
