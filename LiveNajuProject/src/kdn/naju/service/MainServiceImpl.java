package kdn.naju.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kdn.naju.dao.MainDao;
@Service
public class MainServiceImpl implements MainService {

	@Autowired
	MainDao	mainDao;
	
	@Override
	public int activeCount(String member_company) {
		int count = mainDao.activeCount(member_company);
		return count;
	}

	@Override
	public int activeCountAll() {
		int count = mainDao.activeCountAll();
		return count;
	}

}
