package kdn.naju.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kdn.naju.dao.MaptableDao;
import kdn.naju.util.PageBean;
import kdn.naju.vo.Maptable;
import kdn.naju.vo.Member;

@Service
public class MaptableServiceImpl implements MaptableService{

	@Autowired
	private MaptableDao Maptabledao;
	
	@Override
	public void regist(Maptable maptable) {
		
		Maptabledao.addmark(maptable);
	}

	@Override
	public void remove(int mark_no) {
		
		Maptabledao.delete(mark_no);
	}

	@Override
	public List<Maptable> searchAll(PageBean pageBean) {
		List<Maptable> list = Maptabledao.searchAll(pageBean);
		return list;
	}

	@Override
	public Maptable search(int mark_no) {
		Maptable maptable=Maptabledao.search(mark_no);
		return maptable;
	}

	@Override
	public List<Maptable> selectMaptables(PageBean pageBean, double radius, double lat, double lng, int category_no) {
		
		return Maptabledao.selectMaptables(pageBean, radius, lat, lng, category_no);
	}

	@Override
	public Maptable search(String groupname) {

		return Maptabledao.selectgroupname(groupname);
	}

	@Override
	public List<Member> searchPeopleList(int mark_no) {
		List<Member> mapList = Maptabledao.searchPeopleList(mark_no);
		return mapList;
	}

}
