package kdn.naju.service;

import java.util.List;

import kdn.naju.util.PageBean;
import kdn.naju.vo.Member;



public interface MemberService {

	public void regist(Member member);
	
	public void remove(int member_no);
	
	public List<Member> searchAll(PageBean pageBean);

	public Member search(int member_no);

	
	public void update(Member Member);

	public Member search_id(String member_id);
	
}
