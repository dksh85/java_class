package kdn.naju.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kdn.naju.dao.MemberDao;
import kdn.naju.util.PageBean;
import kdn.naju.vo.Member;
@Service
public class MemberServiceImpl implements MemberService{

	@Autowired
	private MemberDao memberDao;
	
	
	@Override
	public void regist(Member member) {
		memberDao.add(member);
		
	}

	@Override
	public List<Member> searchAll(PageBean pageBean) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Member search(int member_no) {
		// TODO Auto-generated method stub
		Member m=memberDao.search(member_no);
		return m;
	}

	@Override
	public void remove(int member_no) {

		memberDao.delete(member_no);
		
	}

	@Override
	public void update(Member member) {
		
		memberDao.update(member);
		
		
	}

	@Override
	public Member search_id(String member_id) {
		// TODO Auto-generated method stub
		return memberDao.search_id(member_id);
	}

}
