package kdn.naju.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kdn.naju.dao.ReplyDao;
import kdn.naju.util.PageBean;
import kdn.naju.vo.Reply;
@Service
public class ReplyServiceImpl implements ReplyService {

	@Autowired
	private ReplyDao replyDao;
	
	@Override
	public void add(Reply reply) {
		replyDao.add(reply);
	}
	
	@Override
	public void update(Reply reply) {
		replyDao.update(reply);
	}
	
	@Override
	public void updateCount(Reply reply) {
		replyDao.updateCount(reply);
	}
	
	@Override
	public void remove(int reply_no) {
		replyDao.remove(reply_no);
	}

	@Override
	public Reply search(int reply_no) {
		Reply reply = replyDao.search(reply_no);
		return reply;
	}
	
	@Override
	public List<Reply> searchReply(PageBean pageBean, int board_no) {
		List<Reply> replyList = replyDao.searchReply(pageBean, board_no);
		return replyList;
	}

}
