package kdn.naju.vo;

public class BoardException extends Exception {
	public BoardException(String msg) {
		super(msg);
	}
}
