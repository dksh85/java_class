package kdn.naju.vo;

public class Gcm {
	private int member_no;
	private String gcmNo;
	
	public Gcm() {
		// TODO Auto-generated constructor stub
	}

	public Gcm(int member_no, String gcmNo) {
		super();
		this.member_no = member_no;
		this.gcmNo = gcmNo;
	}

	public int getMember_no() {
		return member_no;
	}

	public void setMember_no(int member_no) {
		this.member_no = member_no;
	}

	public String getGcmNo() {
		return gcmNo;
	}

	public void setGcmNo(String gcmNo) {
		this.gcmNo = gcmNo;
	}

	@Override
	public String toString() {
		return "Gcm [member_no=" + member_no + ", gcmNo=" + gcmNo + "]";
	}
	

	
}
