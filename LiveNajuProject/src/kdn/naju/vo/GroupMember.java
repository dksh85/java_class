package kdn.naju.vo;

public class GroupMember {

		private int member_no;
		private int mark_no;
		
		
		public GroupMember() {
			// TODO Auto-generated constructor stub
		}
		
		
		
		
		public GroupMember( int member_no,int mark_no) {
			super();
			this.member_no = member_no;
			this.mark_no = mark_no;
		}




		public int getMark_no() {
			return mark_no;
		}




		public void setMark_no(int mark_no) {
			this.mark_no = mark_no;
		}




		public int getMember_no() {
			return member_no;
		}




		public void setMember_no(int member_no) {
			this.member_no = member_no;
		}




		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("GroupMember [member_no=");
			builder.append(member_no);
			builder.append(", mark_no=");
			builder.append(mark_no);
			builder.append("]");
			return builder.toString();
		}
	
		
		
		
}
