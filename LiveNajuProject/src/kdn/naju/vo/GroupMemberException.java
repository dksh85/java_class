package kdn.naju.vo;

public class GroupMemberException extends Exception {
	public GroupMemberException(String msg) {
		super(msg);
	}
}
