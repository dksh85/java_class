package kdn.naju.vo;


import java.util.Date;


public class Info{
	private int info_no;
	private String info_title;
	private String info_content;
	private String info_password;
	private String info_date;
	
	
	public Info() {}

	

	public Info(String info_title, String info_content, String info_password) {
		super();
		this.info_title = info_title;
		this.info_content = info_content;
		this.info_password = info_password;
	}
	
	public Info(int info_no, String info_title, String info_content, String info_password) {
		super();
		this.info_no = info_no;
		this.info_title = info_title;
		this.info_content = info_content;
		this.info_password = info_password;
	}



	public Info(int info_no, String info_title,String info_content
			,String info_password,String info_date) {
		super();
		this.info_no = info_no;
		this.info_title = info_title;
		this.info_content = info_content;
		this.info_password = info_password;
		this.info_date = info_date;
		
	}

	

	public int getInfo_no() {
		return info_no;
	}



	public void setInfo_no(int info_no) {
		this.info_no = info_no;
	}



	public String getInfo_title() {
		return info_title;
	}



	public void setInfo_title(String info_title) {
		this.info_title = info_title;
	}



	public String getInfo_content() {
		return info_content;
	}



	public void setInfo_content(String info_content) {
		this.info_content = info_content;
	}



	public String getInfo_password() {
		return info_password;
	}



	public void setInfo_password(String info_password) {
		this.info_password = info_password;
	}



	public String getInfo_date() {
		return info_date;
	}



	public void setInfo_date(String info_date) {
		this.info_date = info_date;
	}



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Info [info_no=");
		builder.append(info_no);
		builder.append(", info_title=");
		builder.append(info_title);
		builder.append(", info_content=");
		builder.append(info_content);
		builder.append(", info_password=");
		builder.append(info_password);
		builder.append(", info_date=");
		builder.append(info_date);
	
		builder.append("]");
		return builder.toString();
	}
	
	
}
