package kdn.naju.vo;

public class InfoException extends Exception {
	public InfoException(String msg) {
		super(msg);
	}
}
