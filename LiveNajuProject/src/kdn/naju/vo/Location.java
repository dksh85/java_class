package kdn.naju.vo;

public class Location {

	
	private int location_no;
	private String location_name;  
	private double lat ;  
	private double lng ;  
	private String addr;
	private String phone;  
	private String image;  
	private String image_icon; 
	
	public Location() {
	}

	public int getLocation_no() {
		return location_no;
	}

	public void setLocation_no(int location_no) {
		this.location_no = location_no;
	}

	public String getLocation_name() {
		return location_name;
	}

	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImage_icon() {
		return image_icon;
	}

	public void setImage_icon(String image_icon) {
		this.image_icon = image_icon;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Location [location_no=");
		builder.append(location_no);
		builder.append(", location_name=");
		builder.append(location_name);
		builder.append(", lat=");
		builder.append(lat);
		builder.append(", lng=");
		builder.append(lng);
		builder.append(", addr=");
		builder.append(addr);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", image=");
		builder.append(image);
		builder.append(", image_icon=");
		builder.append(image_icon);
		builder.append("]");
		return builder.toString();
	}

	
	
}
