package kdn.naju.vo;

public class Main {
	private int activeCount;
	private int kdn;
	private int kps;
	private int kpx;
	private int kepco;
	private int at;
	private int ekr;
	private int kca;
	
	public Main() {}
	
	public Main(int activeCount) {
		super();
		this.activeCount = activeCount;
	}
	
	public Main(int kdn, int kps, int kpx, int kepco, int at, int ekr, int kca) {
		super();
		this.kdn = kdn;
		this.kps = kps;
		this.kpx = kpx;
		this.kepco = kepco;
		this.at = at;
		this.ekr = ekr;
		this.kca = kca;
	}

	public int getActiveCount() {
		return activeCount;
	}

	public void setActiveCount(int activeCount) {
		this.activeCount = activeCount;
	}

	public int getKdn() {
		return kdn;
	}

	public void setKdn(int kdn) {
		this.kdn = kdn;
	}

	public int getKps() {
		return kps;
	}

	public void setKps(int kps) {
		this.kps = kps;
	}

	public int getKpx() {
		return kpx;
	}

	public void setKpx(int kpx) {
		this.kpx = kpx;
	}

	public int getKepco() {
		return kepco;
	}

	public void setKepco(int kepco) {
		this.kepco = kepco;
	}

	public int getAt() {
		return at;
	}

	public void setAt(int at) {
		this.at = at;
	}

	public int getEkr() {
		return ekr;
	}

	public void setEkr(int ekr) {
		this.ekr = ekr;
	}

	public int getKca() {
		return kca;
	}

	public void setKca(int kca) {
		this.kca = kca;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Main [activeCount=");
		builder.append(activeCount);
		builder.append(", kdn=");
		builder.append(kdn);
		builder.append(", kps=");
		builder.append(kps);
		builder.append(", kpx=");
		builder.append(kpx);
		builder.append(", kepco=");
		builder.append(kepco);
		builder.append(", at=");
		builder.append(at);
		builder.append(", ekr=");
		builder.append(ekr);
		builder.append(", kca=");
		builder.append(kca);
		builder.append("]");
		return builder.toString();
	}


	
	
}
