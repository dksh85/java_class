package kdn.naju.vo;

public class Maptable {

	
	private int mark_no;
	private String groupname;
	private double lat;
	private double lng;
	private String location_name;
	private String description;
	private String image;
	private int max_count;
	private String addr;
	private String phone;
	private int category_no;
	private int member_no;
	private String group_date;
	
	
	public Maptable() {
		// TODO Auto-generated constructor stub
	}

	public Maptable(int mark_no, String groupname, double lat, double lng, String location_name, String description,
			 int max_count, String addr, String phone, int category_no, int member_no) {
		super();
		this.mark_no = mark_no;
		this.groupname = groupname;
		this.lat = lat;
		this.lng = lng;
		this.location_name = location_name;
		this.description = description;
		this.max_count = max_count;
		this.addr = addr;
		this.phone = phone;
		this.category_no = category_no;
		this.member_no = member_no;
	}
	
	

	public Maptable(int mark_no, String groupname, double lat, double lng, String location_name, String description,
			String image, int max_count, String addr, String phone, int category_no, int member_no) {
		super();
		this.mark_no = mark_no;
		this.groupname = groupname;
		this.lat = lat;
		this.lng = lng;
		this.location_name = location_name;
		this.description = description;
		this.image = image;
		this.max_count = max_count;
		this.addr = addr;
		this.phone = phone;
		this.category_no = category_no;
		this.member_no = member_no;
	}

	




	public Maptable(String groupname, double lat, double lng, String location_name, String description, String image,
			int max_count, String addr, String phone, int category_no, int member_no, String group_date) {
		
		super();
		this.groupname = groupname;
		this.lat = lat;
		this.lng = lng;
		this.location_name = location_name;
		this.description = description;
		this.image = image;
		this.max_count = max_count;
		this.addr = addr;
		this.phone = phone;
		this.category_no = category_no;
		this.member_no = member_no;
		this.group_date = group_date;
	}

	public Maptable(int mark_no, String groupname, double lat, double lng, String location_name, String description,
			String image, int max_count, String addr, String phone, int category_no, int member_no, String group_date) {
		super();
		this.mark_no = mark_no;
		this.groupname = groupname;
		this.lat = lat;
		this.lng = lng;
		this.location_name = location_name;
		this.description = description;
		this.image = image;
		this.max_count = max_count;
		this.addr = addr;
		this.phone = phone;
		this.category_no = category_no;
		this.member_no = member_no;
		this.group_date = group_date;
	}



	public Maptable(String groupname2, double lat2, double lng2, String location_name2, String description2,
			String image2, String max_count2, String category_no2, String addr2, String phone2, int member_no2,
			String group_date2) {
		// TODO Auto-generated constructor stub
	}

	public int getMark_no() {
		return mark_no;
	}


	public void setMark_no(int mark_no) {
		this.mark_no = mark_no;
	}



	public String getGroupname() {
		return groupname;
	}


	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}


	public double getLat() {
		return lat;
	}


	public void setLat(double lat) {
		this.lat = lat;
	}



	public double getLng() {
		return lng;
	}


	public void setLng(double lng) {
		this.lng = lng;
	}



	public String getLocation_name() {
		return location_name;
	}


	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}

	public int getMax_count() {
		return max_count;
	}


	public void setMax_count(int max_count) {
		this.max_count = max_count;
	}


	public String getAddr() {
		return addr;
	}





	public void setAddr(String addr) {
		this.addr = addr;
	}


	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}



	public int getCategory_no() {
		return category_no;
	}

	public void setCategory_no(int category_no) {
		this.category_no = category_no;
	}


	public int getMember_no() {
		return member_no;
	}


	public void setMember_no(int member_no) {
		this.member_no = member_no;
	}

	public String getGroup_date() {
		return group_date;
	}

	public void setGroup_date(String group_date) {
		this.group_date = group_date;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Maptable [mark_no=");
		builder.append(mark_no);
		builder.append(", groupname=");
		builder.append(groupname);
		builder.append(", lat=");
		builder.append(lat);
		builder.append(", lng=");
		builder.append(lng);
		builder.append(", location_name=");
		builder.append(location_name);
		builder.append(", description=");
		builder.append(description);
		builder.append(", image=");
		builder.append(image);
		builder.append(", max_count=");
		builder.append(max_count);
		builder.append(", addr=");
		builder.append(addr);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", category_no=");
		builder.append(category_no);
		builder.append(", member_no=");
		builder.append(member_no);
		builder.append(", group_date=");
		builder.append(group_date);
		builder.append("]");
		return builder.toString();
	}





	
	
	
	
	
	
}
