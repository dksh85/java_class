package kdn.naju.vo;

public class MemberException extends Exception {

	public MemberException(String msg) {
		super(msg);
	}
}
