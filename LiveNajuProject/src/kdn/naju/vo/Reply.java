package kdn.naju.vo;

import java.util.Date;

public class Reply {
	private int reply_no;
	private String reply_content;
	private String reply_date;
	private int reply_like;
	private int board_no;
	private int member_no;
	private String member_name;
	
	public Reply() {}
	public Reply(String reply_content, int board_no, int member_no) {
		super();
		this.reply_content = reply_content;
		this.board_no = board_no;
		this.member_no = member_no;
		//this.member_name = member_name;
	}
	public Reply(String reply_content, int reply_like, String reply_date, int board_no, String member_name) {
		this.reply_content = reply_content;
		this.reply_like = reply_like;
		this.reply_date = reply_date;
		this.board_no = board_no;
		this.member_name = member_name;
	}
	public Reply(int reply_no, String reply_content, String reply_date, int reply_like, int board_no, int member_no, String member_name) {
		super();
		this.reply_no = reply_no;
		this.reply_content = reply_content;
		this.reply_date = reply_date;
		this.reply_like = reply_like;
		this.board_no = board_no;
		this.member_no = member_no;
		this.member_name = member_name;
	}
	public int getReply_no() {
		return reply_no;
	}
	public void setReply_no(int reply_no) {
		this.reply_no = reply_no;
	}
	public String getReply_content() {
		return reply_content;
	}
	public void setReply_content(String reply_content) {
		this.reply_content = reply_content;
	}
	public String getReply_date() {
		return reply_date;
	}
	public void setReply_date(String reply_date) {
		this.reply_date = reply_date;
	}
	public int getReply_like() {
		return reply_like;
	}
	public void setReply_like(int reply_like) {
		this.reply_like = reply_like;
	}
	public int getBoard_no() {
		return board_no;
	}
	public void setBoard_no(int board_no) {
		this.board_no = board_no;
	}
	public int getMember_no() {
		return member_no;
	}
	public void setMember_no(int member_no) {
		this.member_no = member_no;
	}
	public String getMember_name() {
		return member_name;
	}
	public void setMember_name(String member_name) {
		this.member_name = member_name;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Reply [reply_no=");
		builder.append(reply_no);
		builder.append(", reply_content=");
		builder.append(reply_content);
		builder.append(", reply_date=");
		builder.append(reply_date);
		builder.append(", reply_like=");
		builder.append(reply_like);
		builder.append(", board_no=");
		builder.append(board_no);
		builder.append(", member_no=");
		builder.append(member_no);
		builder.append(", member_name=");
		builder.append(member_name);
		builder.append("]");
		return builder.toString();
	}
	
	
}
