package com.kdn.member.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.kdn.member.domain.Member;
import com.kdn.member.domain.PageBean;

public interface MemberDao {
	public void add(Connection c, Member member) throws SQLException;		
	public void update(Connection c, Member member) throws SQLException;		
	public void remove(Connection c, String id) throws SQLException;		
	public Member search(Connection c, String id) throws SQLException;		
	public List<Member> searchAll(Connection c, PageBean id) throws SQLException;		
}
