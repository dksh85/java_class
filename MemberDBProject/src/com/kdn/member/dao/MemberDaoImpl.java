package com.kdn.member.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kdn.member.domain.Member;
import com.kdn.member.domain.PageBean;
import com.kdn.member.util.DBUtil;

public class MemberDaoImpl implements MemberDao {
	public void add(Connection con, Member member) throws SQLException {
		PreparedStatement stmt = null;
		try{
			String sql = "insert into member(id, password, name, email, address)"
					+ " values (?,?,?,?,?)";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, member.getId());
			stmt.setString(2, member.getPassword());
			stmt.setString(3, member.getName());
			stmt.setString(4, member.getEmail());
			stmt.setString(5, member.getAddress());
			
			stmt.executeUpdate();
		} finally {
			DBUtil.close(stmt);
		}
	}

	@Override
	public void update(Connection con, Member member) throws SQLException {
		PreparedStatement stmt = null;

		try{
			String sql = "update from member set password = ?, name = ?, email = ?, address = ?, withdraw = ?"
					+ " where id = ? ";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, member.getPassword());
			stmt.setString(2, member.getName());
			stmt.setString(3, member.getEmail());
			stmt.setString(4, member.getAddress());
			stmt.setString(5, member.getWithdraw());
			stmt.setString(6, member.getId());
			
		}
		finally{
			DBUtil.close(stmt);
		}
	}
	@Override
	public void remove(Connection con, String id) throws SQLException {
		PreparedStatement stmt = null;
		try{
			String sql = "delete from member where id = ? ";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, id);
			stmt.executeUpdate();
		}
		finally{
			DBUtil.close(stmt);
		}
	}

	@Override
	public Member search(Connection con, String id) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try{
			String sql = "select * from member where id = ? ";
			stmt = con.prepareStatement(sql);
			rs = stmt.executeQuery();
			if(rs.next()){
				return new Member(rs.getString("id"), rs.getString("password")
						, rs.getString("name"), rs.getString("email"), rs.getString("address")
						, rs.getString("withdraw"));
			}
			
		} finally {
			DBUtil.close(stmt);
		}
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Member> searchAll(Connection con, PageBean id) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try{
			String key = id.getKey();
			String word = id.getWord();
			StringBuilder sql =	new StringBuilder(100);
				sql.append("select a.* \n");
				sql.append(" from ( select row_number() over(order by name) ro \n");
				sql.append(" 		, id,  name, email, address \n ");
				sql.append(" 			from member \n");
				sql.append(" 			where 1 = 1 \n" );
				if(!key.equals("all") && !word.trim().equals("")){
					if( key.equals("name") ){
						sql.append(" and name like ?	");
					}
					else if (key.equals("email")){
						sql.append(" and email like ? 	");
					}
					else if (key.equals("address")){
						sql.append(" and address like ? 	");
					}
				}
				sql.append(" a		\n"); 
				sql.append(" where ro between ? and ?\n");  
				
				stmt = con.prepareStatement(sql.toString());

				int idx = 1;
				if(!key.equals("all") && !word.trim().equals("")){
					stmt.setString(idx++, "%"+word+"%");
				}
				
				stmt.setInt(idx++, id.getStart());
				stmt.setInt(idx++, id.getEnd());
				
				rs = stmt.executeQuery();
				ArrayList<Member> m = new ArrayList<Member>(id.getInterval());
				while(rs.next()){
					m.add(new Member(rs.getString("id")
								, rs.getString("name")
								, rs.getString("email")
								, rs.getString("address")));
				}
					
		} finally {
			DBUtil.close(stmt);
			DBUtil.close(rs);
		}
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
