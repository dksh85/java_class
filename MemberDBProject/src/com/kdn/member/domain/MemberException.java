package com.kdn.member.domain;

public class MemberException extends RuntimeException {
	public MemberException(String msg){
		super(msg);
	}
}