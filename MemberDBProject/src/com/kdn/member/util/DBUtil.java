package com.kdn.member.util;

import java.sql.*;
public class DBUtil {
	public static String url = "jdbc:oracle:thin:@localhost:1521:xe";
	public static String id ="scott";
	public static String password ="tiger";
	static{
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.out.println("driver를 로딩할 수 없습니다. ");
		}
	}
	public static Connection getConnection() throws SQLException{
		return  DriverManager.getConnection(url, id, password);
	}
	public static void rollback(Connection  con) {
		if(con != null){
		 	try {
		 	  con.rollback();
			} catch (Exception e) {
			}
	 }
	}
	public static void close(Connection  con){
		 if(con != null){
			 	try {
			 		if(!con.getAutoCommit()){
			 			con.commit();
			 			con.setAutoCommit(true);
			 		}
			 		con.close();
				} catch (Exception e) {
				}
		 }
	}
	public static void close(Statement  stmt){
		if(stmt != null){
			try {
				stmt.close();
			} catch (Exception e) {
			}
		}
	}
	public static void close(ResultSet  rs){
		if(rs != null){
			try {
				rs.close();
			} catch (Exception e) {
			}
		}
	}
}




















