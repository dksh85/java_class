package com.kdn.wjbasic03;

public class BookMgr {
	/** 다형성에 의해 모든 Sub 객체는 Super 타입으로 형변환이 되므로 Super타입의 변수만
	 * 선언한다.
	 * Magazine은 다형성에 의해 Book으로 형변환 되므로 Book[]만 선언하면 됨.
	 */
	private Book[] booklist;
	private int bookIndex;
	public BookMgr(){
		booklist = new Book[15];

	}
	/**
	 * isbn에 해당하는 책을 booklist와 maglist에서 검색해  찾은 책 정보를 
	 * 문자열로 리턴하는 기능 
	 * @param isbn  찾을 isbn
	 * @return      찾은 책의 정보 
	 */
	public String findBook(String isbn){
		for(int i =0; i < bookIndex; i++){
			if(booklist[i].getIsbn().equals(isbn)){
				return booklist[i].toString();
			}
		}
		
		return null;
	}
	/**
	 * Book을 booklist에 등록 하는 기능
	 * @param b 등록할 책 정보 
	 */
	public void add(Book b){
		if(bookIndex >= booklist.length){
			Book[] temp = new Book[booklist.length + 15];
			System.arraycopy(booklist, 0, temp, 0, booklist.length);
			booklist = temp;
		}
		booklist[bookIndex++] = b;
	}
	/**
	 * Magazine을 maglist에 등록하는 기능
	 * @param m
	 */
	
	
	/**
	 * 등록된 모든 Book 정보와 Magazine 정보를 출력하는 기능 
	 */
	public void printAll(){
		for(int i = 0; i < bookIndex; i++){
			System.out.println(booklist[i].toString());
		}

	}
	/**
	 * 등록된 모든 Book 정보만 출력하는 기능 
	 */
	public void printBook(){
		for(int i = 0; i < bookIndex; i++){
			if(!(booklist[i] instanceof Magazine))
				System.out.println(booklist[i].toString());
		}
	}
	/**
	 * 등록된 모든 Magazine 정보만 출력하는 기능 
	 */
	public void printMagazine(){
		for(int i = 0; i < bookIndex; i++){
			if(booklist[i] instanceof Magazine)
				System.out.println(booklist[i].toString());
		}
	}
}