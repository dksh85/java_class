package com.kdn.wjbasic03;

public class Magazine extends Book{
	private int year;
	private int month;
	
	public Magazine(){}
	
	public Magazine(String isbn, String title, String author, String publisher, int year,
			int month, int price, String desc) {
		super(isbn, title, author, publisher, price, desc);
		this.year = year;
		this.month = month;
	}
	
	public Magazine(String isbn, String title, String author, String publisher, int year,
			int month, int price) {
		super(isbn, title, author, publisher, price);
		this.year = year;
		this.month = month;
	}	

	public String getIsbn() {
		return super.getIsbn();
	}

	public void setIsbn(String isbn) {
		super.setIsbn(isbn);
	}



	public String getTitle() {
		return super.getTitle();
	}



	public void setTitle(String title) {
		super.setTitle(title);
	}



	public String getAuthor() {
		return super.getAuthor();
	}



	public void setAuthor(String author) {
		super.setAuthor(author);
	}

	public String getPublisher() {
		return super.getPublisher();
	}



	public void setPublisher(String publisher) {
		super.setPublisher(publisher);
	}

	public int getYear() {
		return year;
	}



	public void setYear(int year) {
		this.year = year;
	}



	public int getMonth() {
		return month;
	}



	public void setMonth(int month) {
		if (month < 1 && month > 12){
			System.out.println("잘못된 값입니다. 1월 부터 12월 사이로 설정하세요");
		}
		else this.month = month;
	}



	public int getPrice() {
		return super.getPrice();
	}



	public void setPrice(int price) {
		if (price < 0)
			System.out.println("잘못된 값입니다.");
		else super.setPrice(price);
	}

	public String getDesc() {
		return super.getDesc();
	}

	public void setDesc(String desc) {
		super.setDesc(desc);
	}



	public String toString(){
		return	super.toString()
				+ "\t | "+ year + "." + month;
	}	
}
