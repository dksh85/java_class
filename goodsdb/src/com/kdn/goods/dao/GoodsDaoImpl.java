package com.kdn.goods.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.kdn.goods.domain.Goods;
import com.kdn.goods.domain.PageBean;
import com.kdn.goods.util.DBUtil;

public class GoodsDaoImpl implements GoodsDao {

	@Override
	public void add(Connection con, Goods goods) throws SQLException {
		PreparedStatement stmt = null;
		try{
			String sql = "insert into Goods2(gno, brand, price, maker, image, cno) "
					+ " values (?,?,?,?,?,?) " ;
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, Integer.parseInt(goods.getGno()));
			stmt.setString(2, goods.getBrand());
			stmt.setInt(3, goods.getPrice());
			stmt.setString(4, goods.getMaker());
			stmt.setString(5, goods.getImage());
			stmt.setString(6, goods.getCno());
			stmt.executeUpdate();
			
		}finally{
			DBUtil.close(stmt);
		}

	}

	@Override
	public void update(Connection con, Goods goods) throws SQLException {
		// TODO Auto-generated method stu
		PreparedStatement stmt = null;
		try{
			String sql = "update Goods2 set  brand = ? , price =?, maker =?, image =?, cno =? "
					+ " where gno = ? ";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, goods.getBrand());
			stmt.setInt(2, goods.getPrice());
			stmt.setString(3, goods.getMaker());
			stmt.setString(4, goods.getImage());
			stmt.setString(5, goods.getCno());
			stmt.setInt(6, Integer.parseInt(goods.getGno()));
			stmt.executeUpdate();
			
		}finally{
			DBUtil.close(stmt);
		}

	}

	@Override
	public void remove(Connection con, String gno) throws SQLException {
		PreparedStatement stmt = null;
		
		try{
			String sql = "delete from goods2 where gno = ?";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, gno);
			stmt.executeUpdate();

		}finally{
			DBUtil.close(stmt);
		}
		// TODO Auto-generated method stub

	}

	@Override
	public Goods search(Connection con, String gno) throws SQLException {
		// TODO Auto-generated method stu	
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try{
			
			String sql = "select * from goods2 where gno = ?";
			stmt = con.prepareStatement(sql);
			
			stmt.setString(1, gno);
			rs  = stmt.executeQuery();
			
			if(rs.next()){
				return new Goods(rs.getString("GNO"), rs.getString("brand")
						, rs.getInt("price"), rs.getString("maker"),rs.getString("image")
						, rs.getString("cno"));
			}
			
		}finally{
			DBUtil.close(stmt);
		}
		return null;
	}

	@Override
	public List<Goods> searchAll(Connection con, PageBean bean) throws SQLException {
		// TODO Auto-generated method stub
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try{
			
			StringBuilder sql = new StringBuilder(100);
			sql.append("select a.* 					\n");
			sql.append("	from (select row_number() over(order by gno desc) ro, 		\n");
			sql.append("				gno, brand, maker, price, image, cno 			\n");
			sql.append("					from goods2						\n");
			sql.append("					where 1 = 1						\n");
			if(!bean.getKey().equals("all") && !bean.getWord().trim().equals("")){
				if (bean.getKey().equals("brand")){
					sql.append(" and brand like ? \n ");
				}
				else if (bean.getKey().equals("maker")){
					sql.append(" and maker like ? \n ");
				}
				else if (bean.getKey().equals("cno")){
					sql.append(" and maker = ? \n ");
				}
			}
			
			sql.append("			) a												\n");
			sql.append("		where ro between ? and ?					\n");
			
			int idx = 1;
			ArrayList<Goods> temp = new ArrayList<Goods>();
			stmt = con.prepareStatement(sql.toString());
			if(!bean.getKey().equals("all") && !bean.getWord().trim().equals("")){
				if (bean.getKey().equals("cno")){
					stmt.setInt(idx++, Integer.parseInt(bean.getWord()));
				}
				else {
					stmt.setString(idx++, "%" + bean.getWord() + "%");
					System.out.println("in second"+ "%" + bean.getWord() + "%");
				}
			}
			
			stmt.setInt(idx++, bean.getStart());
			stmt.setInt(idx++, bean.getEnd());
			
			rs = stmt.executeQuery();
			
			while(rs.next()){
				int ro = rs.getInt("ro");
				String gno = rs.getString("gno");
				String brand = rs.getString("brand");
				int price = rs.getInt("price");
				String maker = rs.getString("maker");
				String image  = rs.getString("image");
				String cno = rs.getString("cno");
				
				temp.add(new Goods(gno, brand, price, maker, image, cno));
			}
			return temp;
			
			
		}finally{
			DBUtil.close(stmt);
		}
	}

	@Override
	public int getCount(Connection con, PageBean bean) throws SQLException {
		// TODO Auto-generated method stu
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try{
			
			StringBuilder sql = new StringBuilder(100);
			sql.append("select count(*) cnt from GOODS2 where 1 = 1 \n");
			if(!bean.getKey().equals("all") && !bean.getWord().trim().equals("")){
				if (bean.getKey().equals("brand")){
					sql.append(" and brand like ? \n ");
				}
				else if (bean.getKey().equals("maker")){
					sql.append(" and maker like ? \n ");
				}
				else if (bean.getKey().equals("cno")){
					sql.append(" and cno = ? \n ");
				}
			}
			
			int idx = 1;
			ArrayList<Goods> temp = new ArrayList<Goods>();
			stmt = con.prepareStatement(sql.toString());
			if(!bean.getKey().equals("all") && !bean.getWord().trim().equals("")){
				if (bean.getKey().equals("cno")){
					stmt.setInt(idx++, Integer.parseInt(bean.getWord()));
				}
				else {
					stmt.setString(idx++, "%" + bean.getWord() + "%");
				}
			}
			
			rs = stmt.executeQuery();
			
			if(rs.next()){
				int ro = rs.getInt("cnt");
				return ro;
			}
			
			
		}finally{
			DBUtil.close(stmt);
		}
		return 0;
	}

}
