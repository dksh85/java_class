package com.kdn.goods.domain;

public class Goods {
	private String gno;
	private String brand;
	private int price;
	private String maker;
	private String image;
	private int quantity;
	private String cno;
	
	public Goods() {
	}

	public Goods(String gno, String brand, int price, String image) {
		this.gno = gno;
		this.brand = brand;
		this.price = price;
		this.image = image;
	}

	public Goods(String gno, String brand, int price, String maker, 
		 String image, String cno) {
		this.gno = gno;
		this.brand = brand;
		this.price = price;
		this.maker = maker;
		this.image = image;
		this.cno = cno;
	}

	public String getGno() {
		return gno;
	}

	public void setGno(String gno) {
		this.gno = gno;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getMaker() {
		return maker;
	}

	public void setMaker(String maker) {
		this.maker = maker;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public boolean equals(Object o){
		if(o instanceof Goods){
			Goods goods = (Goods) o;
			
			//상품번호가 같으면 같은 상품으로 처리
			if(brand != null && gno.equals(goods.gno)){
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder().append("Goods [gno=").append(gno)
				.append(", brand=").append(brand).append(", price=").append(price)
				.append(", maker=").append(maker)
				.append(", image=").append(image)
				.append(", cno=").append(cno)
				.append("]");
		return builder.toString();
	}

	
	
}
