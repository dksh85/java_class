package com.kdn.goods.domain;

public class GoodsException extends RuntimeException{
	public GoodsException(String msg){
		super(msg);
	}

}
