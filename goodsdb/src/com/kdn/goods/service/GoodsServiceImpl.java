package com.kdn.goods.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kdn.goods.dao.GoodsDao;
import com.kdn.goods.dao.GoodsDaoImpl;
import com.kdn.goods.domain.Goods;
import com.kdn.goods.domain.GoodsException;
import com.kdn.goods.domain.PageBean;
import com.kdn.goods.util.DBUtil;

public class GoodsServiceImpl implements GoodsService{
	private GoodsDao dao = new GoodsDaoImpl();
	/**
	 * 
	 * 상품정보를 DB에 저장하는 기능
	 * @param con
	 * @param goods 상품정보
	 * @throws SQLException
	 */
	
	public void add(Goods goods) {
		Connection con = null;
		try{
			con = DBUtil.getConnection();
			con.setAutoCommit(false);
			Goods find = dao.search(con, goods.getGno());
			if(find!=null){
				throw new GoodsException("이미 등록된 상품 번호입니다.");
			}
			dao.add(con, goods);
		}catch( GoodsException e){
			DBUtil.rollback(con);
			throw e;
		}catch(Exception e){
			DBUtil.rollback(con);
			throw new GoodsException("상품을 등록 중 오류 발생");
		}
		finally{
			DBUtil.close(con);
		}
	}
	/**
	 * 상품번호(gno)에 해당하는 상품의 정보를 수정 
	 * 상품번호를 제외한 모든 정보를 수정
	 * @param con
	 * @param goods
	 * @throws SQLException
	 */
	public void update(Goods goods){
		Connection con = null;
		try{
			con = DBUtil.getConnection();
			con.setAutoCommit(false);
			Goods find = dao.search(con, goods.getGno());
			if(find==null){
				throw new GoodsException("수정할 상품을 찾을 수 없습니다.");
			}
			dao.update(con, goods);
		}catch( GoodsException e){
			DBUtil.rollback(con);
			throw e;
		}catch(Exception e){
			DBUtil.rollback(con);
			throw new GoodsException("상품을 등록 중 오류 발생");
		}
		finally{
			DBUtil.close(con);
		}
	}
	/**
	 * 상품번호에 해당하는 상품을 삭제하는 기능
	 * @param con
	 * @param gno	삭제할 상품 번호
	 * @throws SQLException
	 */
	public void remove(String gno){
		Connection con = null;
		try{
			con = DBUtil.getConnection();
			con.setAutoCommit(false);
			Goods find = dao.search(con, gno); 
			if(find==null){
				throw new GoodsException("삭제할 상품을 찾을 수 없습니다.");
			}
			dao.remove(con, gno);
		}catch( GoodsException e){
			DBUtil.rollback(con);
			throw e;
		}catch(Exception e){
			DBUtil.rollback(con);
			throw new GoodsException("상품을 등록 중 오류 발생");
		}
		finally{
			DBUtil.close(con);
		}	
	}
	/**
	 * 상품번호에 해당하는 상품을 찾아 상품정보를 반환
	 * @param con
	 * @param gno	찾을 상품 번호
	 * @return		찾은 상품 정보
	 * @throws SQLException
	 */
	public Goods search(String gno) { 
		Connection con = null;
		try{
			con = DBUtil.getConnection();
			con.setAutoCommit(false);
			Goods find = dao.search(con, gno); 
			if(find==null){
				throw new GoodsException("검색한 상품을 찾을 수 없습니다.");
			}
			return find;
		}catch( GoodsException e){ //주석처리해도 됨
			DBUtil.rollback(con);
			throw e;
		}catch(Exception e){ //database 오류일 경우 잡힘
			DBUtil.rollback(con);
			throw new GoodsException("상품 검색중 오류발생");
		}
		finally{
			DBUtil.close(con);
		}	
	}
	/**
	 * 조회조건에 따른 상품 목록을 반환하는 기능
	 * @param con
	 * @param bean	조회 조건 :	상품이름, 제조사, 상품 분류
	 * 						페이지 정보(페이지 번호) <br/>
	 * @return		조회 조건에 해당하는 상품 목록
	 * @throws SQLException
	 */
	public List<Goods> searchAll(PageBean bean){ 
		Connection con = null;
		try{
			con = DBUtil.getConnection();
			con.setAutoCommit(false);
			return dao.searchAll(con, bean); 

		}catch(GoodsException e){
			DBUtil.rollback(con);
			throw e;
		}catch(Exception e){
			DBUtil.rollback(con);
			throw new GoodsException("searchAll 중 오류");
		}finally{
			DBUtil.close(con);
		}
	}
	/**
	 * 조회 조건에 따른 상품의 개수를 반환하는 기능
	 * @param con
	 * @param bean	조회 조건 :	상품이름, 제조사, 상품 분류
	 * 						페이지 정보(페이지 번호) <br/>
	 * @return		조회 조건에 해당하는 상품 개수
	 * @throws SQLException
	 */
	public int getCount(PageBean bean) { 
		return 0;
	}
	//서비스에서 sqlexception 을 해줘야 하기때문
}
