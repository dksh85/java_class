package chapter03;

/*
 * 		파일 이름 규칙
 * 		1. 한 파일에 여러개의 클래슬르 작성할 수 있다.
 * 		2. public class는 하나 이하로 작성할 수 있다.
 * 		2. public class를 작성했다면 class의 이름이 파일으로 작성해야 한다.
 * 		그렇지 않으면 컴파일 에러 발생
 */

public class HelloWorld {
	/** document 주석:
	 * 	API Document 만들때 설명으로 작성된.
	 *	- 클래스, 속성 , 매서드 앞에 작성한다. 
	 *	- javadoc [option] 파일이름.java
	 */
	
	int test;
	/**
	 * 	main : 프로그램의 시작점
	 * 
	 * 	public static void main(String[] ~) : 프로그램의 시작점
	 * 	시작점 작성시 유의 사항
	 * 	1. modifier는 항상 public static으로 선언
	 * 	2. 리턴티입은 void
	 *  3. 매서드 이름은 main
	 *  4. 인자는 String[]
	 */

	public static void main(String[] args) {
		System.out.print("kdn");
		// 콘솔에 출력하는 기능
		System.out.println("hello world!!!!");
		System.out.println("java"); 
	
		/*
		 *	System.out.printf("출력할 데이터 format", data1, ...);
		 *	%d :정수형
		 *	%f : 실수
		 *	%s : 문자열 
		 *	%b : 논리열
		 *	%c: 한문자
		 */
		System.out.printf(" %d * %d = %d\n", 3, 5, 3*5);
		
		//Formatter f;

	}

}
