package chapter04;

public class ContinueTest {
	public static void main(String[] args) {
		//1~10까지 데이터중 3의 배수는 출력하지 않음.
		for(int i = 1; i <= 10; i++){
			if(i%3 == 0) continue;
			System.out.print(i + " ");
		}
	}
}