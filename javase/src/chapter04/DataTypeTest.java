package chapter04;

public class DataTypeTest {
	
	public static void main(String[] args){
		/*
		 *  변수
		 *  DataType id; or DataType id [=value, id = value ...];
		 */
		
		byte b1 = 120, b2 = 3;
		
		//주의점 : byte, short는 연산시 int 형으로 바뀌므로 casting 하거나 int형으로 사용
//		byte b3 = b1 + b2;
		short s1 = 245, s2 = 4;
//		short s3 = s1 + s2;
		
		byte b4 = 124 + 2; //literal 자체로 하면 에러 안남.
		int i1 = b1 + b2;
		
		long l1 = 2;
//		long l2 = 2222222222; //literal도 검사함. int 형으로 체크
//		int를  벗어난 리터럴은 long 형임을 표시하기 위해 l, L을 표시한다.
		long l2 = 2222222222l, l3 = 2222222222l;
		

		float f1 = 3.14f, f2= 3.141f;
		float f3 = 65536.256f;
		float f4 = 2222222222.256f;
		System.out.println("f1 : " + f1);
		System.out.println("f3 : " + f3);
		System.out.println("f4 : " + f4);
		
		char c1 = 'h'; // ascii code
		char c2 = '\u0000'; // unicode
		
		boolean bl1 = true;
		boolean bl2 = false;

// typecasting test
		int i2 = b1, i3 = s1, i4 = c1+3;
		System.out.println("i4:" + i4);
		
		char c3 = (char)i4;
		System.out.println("c3:"+c3);
		
		float f5 = l2;
		
		byte b5 = (byte)(b1 + b2);
		
	}

}
