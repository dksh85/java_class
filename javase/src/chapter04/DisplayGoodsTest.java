package chapter04;

import java.util.Scanner;

public class DisplayGoodsTest {
	public static void main(String[] args) {
		Scanner	scan = new Scanner(System.in);
		System.out.println("총 상품의 개수를 입력하세요");
		int total = scan.nextInt();
		System.out.println("한 줄에 배치할 상품 개수를 입력하세요");
		int number = scan.nextInt();
		
		int line = total / number;
		if(total % number != 0)	line++;
		
		System.out.printf("총 상품 개수: %d 라인별 상품수 : %d 라인수 : %d\n",
				total, number, line);
		
		for(int i =1; i<=line; i++){
			for(int j = 1; j <= number; j++){
				System.out.print("@\t");
				total--;
				if(total ==0) break;
			}
			System.out.println();
		}
	}
}
