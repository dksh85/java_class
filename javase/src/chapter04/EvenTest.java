package chapter04;

import java.util.Scanner;

public class EvenTest {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		int data = scan.nextInt();

		if (data % 2 == 0)
			System.out.printf("%d는 짝수입니다.", data);
		else
			System.out.printf("%d는 홀수입니다", data);

		System.out.println();
		System.out.println("==============================");

		switch (data % 2) {
		case 0:
			System.out.printf("%d는 짝수입니다. ", data);
			break;

		default:
			System.out.printf("%d는 홀수입니다. ", data);
		}

	}
}
