package chapter04;

public class ForTest2 {
	public static void main(String[] args) {
		for(int i =2; i<10; i++){ //행
			for(int j = 1; j<10; j++){	//열
				System.out.printf("%d * %d = %d\t",i,j,i*j);
			}
			System.out.println();
		}
	
	System.out.println("=================================");	
		
		for(int i =1; i<10; i++){ //행
			for(int j = 2; j<10; j++){	//열
				System.out.printf("%d * %d = %d\t",j,i,i*j);
			}
			System.out.println();
		}
	}
	
}
