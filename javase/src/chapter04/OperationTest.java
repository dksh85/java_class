package chapter04;

public class OperationTest {
	public static void main(String[] args){
		int i = 10, j = 10;
		
		int result1 = 10 + ++i;
		int result2 = 10 + j++;
		
		System.out.printf("result1 : %d i:%d \n", result1, i);
		System.out.printf("result2 : %d j:%d \n", result2, j);
		
		boolean result3 = i<10 && i++>11;
		boolean result4 = j>10 || j++>11;

		System.out.printf("result3 : %b i:%d \n", result3, i);
		System.out.printf("result4 : %b j:%d \n", result4, j);

		boolean result5 = i<10 & i++>11;
		boolean result6 = j>10 | j++>11;

		System.out.printf("result5 : %b i:%d \n", result5, i);
		System.out.printf("result6 : %b j:%d \n", result6, j);
		
		//bit shift
		// -8 >> 2 하면 2^n 만큼 나눈 효과를 갖는다.
		// << 2^n 으로 곱한 효과
		int k = -8;
		System.out.println(Integer.toBinaryString(k));
		
		// >> 부호 비트가 유지 되면서 왼쪽에서 오른쪽으로 비트 이동
		//		2의 지수승으로 나눈 효과
		int result7 = k>>1; // -8 >> 2 = -4
		System.out.println(Integer.toBinaryString(result7));
		System.out.printf("result7 : %d \n", result7);
		
		// >>> 이동된 비트에 0이 추가됨
		result7 = k >>> 1;
		System.out.println(Integer.toBinaryString(result7));
		System.out.printf("result7 : %d \n", result7);
		
		//	<<	: 비트를 오른쪽에서 왼쪽으로 이동, 이동된 비트에 0이 추가됨.
		//	2의 지수승으로 곱한 효과
		result7 = k << 1;
		System.out.println(Integer.toBinaryString(result7));
		System.out.printf("result7 : %d \n", result7);
		
		
		//	3항 연산자
		//	조건 ? 조건이 참이면 수행 : 조건이 거짓이면 수행 ;
		//	초기값 설정
		
		String page = null;
		
		// Integer.parseInt( 정수형 포멧의 문자 데이터) : 문자 데이터를 정수로 변경
		int pageNo = page == null? 1: Integer.parseInt(page);
		
		System.out.printf("pageNo : %d\n", pageNo);
	}
}
