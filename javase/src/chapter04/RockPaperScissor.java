package chapter04;

import java.util.Scanner;

public class RockPaperScissor {

	public static void main(String[] args) {
		int game = (int)(Math.random()*3); //0~1 난수 발생
		Scanner scan = new Scanner(System.in);
		//0은 가위 1은 바위 2는 보

		System.out.println("0: 가위 1: 바위 2: 보");
		System.out.println("0 ~ 2 사이의 정수를 입력하세요");
		
		int user = scan.nextInt();
		if(game == 0){
				System.out.println("컴퓨터 :" + game);
			if(user == 0)
				System.out.println("비김");
			else if(user == 1)
				System.out.println("이겼음");
			else if(user == 2)
				System.out.println("짐");
		}
		if(game == 1){
				System.out.println("컴퓨터 :" + game);
			if(user == 0)
				System.out.println("짐");
			else if(user == 1)
				System.out.println("비김");
			else if(user == 2)
				System.out.println("이김");
		}
		if(game == 2){
				System.out.println("컴퓨터 :" + game);
			if(user == 0)
				System.out.println("이김");
			else if(user == 1)
				System.out.println("짐");
			else if(user == 2)
				System.out.println("비김");
		}
	}
}
