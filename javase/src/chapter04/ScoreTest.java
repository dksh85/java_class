package chapter04;

import java.util.Scanner;

public class ScoreTest {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("성적을 0~100으로 입력하세요");
		int data = scan.nextInt();
		
		if( data >= 0 && data <=100){ // 0~100 사이 값인지 검사.
			String result = "F";
			if(data >= 90) result = "A";
			else if(data >= 80) result = "B";
			else if(data >= 70) result = "C";
			else if(data >= 60) result = "D";
			else if(data >= 50) result = "F";
			System.out.printf("입력하신 점수 %d점은 %s 학점입니다.", data, result);
		}else{
			System.out.printf("0~100사이 정수를 입력하세요.");
		}
		
		String result;
		
		switch(data/10){
			case 9: result = "A";
				break;
			case 8: result = "B";
				break;
			case 7: result = "C";
				break;
			case 6: result = "D";
				break;
			default: result = "F";
		}
		System.out.printf("입력하신 점수 %d점은 %s 학점입니다.", data, result);
	}
}
