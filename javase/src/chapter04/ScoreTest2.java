package chapter04;

import java.util.Scanner;

public class ScoreTest2 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("성적을 0~100로 입력하세요");
		int data = scan.nextInt();
		if (data >= 0 && data <= 100) {
			String result = null;
			switch (data / 10) {
			case 10:
			case 9:
				result = "A";
				break;
			case 8:
				result = "B";
				break;
			case 7:
				result = "C";
				break;
			case 6:
				result = "D";
				break;
			default:
				result = "F";
			}
			System.out.printf("입력하신 점수 %d점은 %s 학점입니다.", data, result);
		}
	}
}
