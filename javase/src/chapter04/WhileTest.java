package chapter04;

public class WhileTest {
	public static void main(String[] args) {
		int sum=0;	// 난수를 저장하는 변수
		int count = 0;
		
		while(count++ < 5){
			sum += (int)(Math.random()*100 +1);
		}
		System.out.printf("sum: %d count : %d\n", sum,count);
		System.out.printf("1~100사이 난수 평균 : %d \n", sum/--count);
	}
}
