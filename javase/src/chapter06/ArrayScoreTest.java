package chapter06;

public class ArrayScoreTest {

	public static void main(String[] args) {
		String[] name = {"홍길동", "길동홍", "동글이", "디엔K"};
		int[] db = {100, 60, 95, 80};
		int[] java = {80, 90, 100, 85};
		int[] net = {60, 80, 80, 100};
		int[] total = new int[name.length];
		double[] avg = new double[name.length];
	
	
		double min = 100, max = 0, totalAvg=0;
		int minIndex=0, maxIndex=0;
		System.out.println("이름\t디비\t자바\t네트웍/t총점\t평균");
		for (int i = 0; i < avg.length; i++){
			total[i] = db[i] + net[i] + java[i];
			avg[i] = total[i] / 3.0;
			totalAvg += avg[i];
			if(min > avg[i]){
				min = avg[i];
				minIndex = i;
			}
			if(max < avg[i]){
				max = avg[i];
				maxIndex = i;
			}
			System.out.printf("%s\t%d\t%d\t%d\t%d\t%f\n"
								, name[i], db[i], java[i], net[i], total[i], avg[i]);
		}
		System.out.printf("평균 최저 학생 : %s 점수:%.2f\n"
						, name[minIndex], avg[minIndex]);
		System.out.printf("평균 최고 학생 : %s 점수:%.2f\n"
						, name[maxIndex], avg[maxIndex]);
		System.out.printf("전체 평균 점수 : %.2f\n", totalAvg/name.length);
	}
	//과목이 추가되는 경우는? 연산, 출력 부분이 바뀜
	//
}
