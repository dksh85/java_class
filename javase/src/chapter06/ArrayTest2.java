package chapter06;

import java.util.Arrays;

public class ArrayTest2 {
	public static void main(String[] args) {
		/**
		 * 
		 *	System.arraycopy(src, srcPos, dest, destPos, length);
		 *	: 배열 정보를 복사하는 기능의 함수
		 *	src : 복사할 원본
		 *	srcPos : 원본의 복사할 index를 지정
		 *	dest : 복사할 대상 (부본)
		 *	destPos : 부본의 복사할 index 지정
		 *	length : 복사할 개수
		 */
		
		int[] source = {0,1,2,3,4,5,6};
		int[] copy1 = new int [15];	//크기 느릴때
		System.arraycopy(source, 0, copy1, 0, source.length); //원본을 전체 복사
		for (int i = 0; i < copy1.length; i++){
			System.out.printf("%d ", copy1[i]);
		}
		System.out.println();
		
		int[] copy2 = new int[3];
		System.arraycopy(source, 2, copy2, 0, copy2.length);
		//2,3,4 부분복사
		for (int i = 0; i < copy2.length; i++) {
			System.out.printf("%d ", copy2[i]);
		}
		
		
		//Arrays.copyOf(original, newLength)
		//Array copy
	}
}
