package chapter06;

public class Arraytest1 {
	public static void main(String[] args) {
		int[] array1;
		array1 = new int[2];
		for (int i = 0; i < array1.length; i++) {
			System.out.printf("array1[%d]: %d", i, array1[i]);
		}
		System.out.println();
		
		
		MyDate[] d1 = new MyDate[2];
		d1[0] = new MyDate();
		d1[0].setYear(2000);
		d1[0].toString();
		
		MyDate d2 = new MyDate();

		for (int i = 0; i < d1.length; i++) {
			System.out.printf("d1[%d]:%s", i, d1[i]);
		}
		System.out.println();
		
		// 접근 id[index] = value;
		array1[0] = 10; array1[1] = 20;

		int[] array2 = {0,1,2,3,4,5};
		MyDate[] d3 = {new MyDate(), d2};
	}
	//선언 생성 할당을 동시에
}
