package chapter06;


class test{
	void accessTest(){
		Employee emp = new Employee();
		emp.empno = 10;
		emp.ename = "홍길동"; // 같은 패키지 이므로 접근 가능
//		emp.deptno = 10;
	}
}


/** 사원정보 */
public class Employee {
/** 사번 */
	public int empno;
/** 사원 이름 */
	String ename;
/** 부서 번호*/
	private int deptno;
/** 부서 이름 */
	String dname;
/** 급여 */
	int sal;
/** 입사일 */
	String hiredate;
/** 사원이 맡은 업무 */
	String job;

	
/**
 *	생성자 오버로딩 (Constructor Overloading)
 *	: 생성자 용도에 따라 여러 개의 생성자를 선언 할 수 있다.
 *		이때 생성자를 구별하기 위해 인자의 개수, 타입, 순서가 달라야 한다. 
 */

	Employee(int no, String name, String date){
		empno = no;
		ename = name;
		hiredate = date;
	}
/**
 *	this : 현재 사용중인 객체를 참조
 *	1. 속성과 로컬 변수를 구별하기 위해 속성앞에 this.을 붙인다 
 *	2. 코드 재사용성을 위해 인자가 다른 생성자를 호출 할 때 
 *		주의점) this(~) 생성자의 첫번째 명령에서만 사용
 *	3. 클래스 내부에서 해당 클래스의 객체가 필요할 경우
 */
	Employee(int empno, String ename, int deptno, String dname, int sal, String hiredate, String job) {
		this(empno, ename, hiredate); 	//	첫번째 명령에서만
		this.empno = empno; 
		this.ename = ename;
		this.deptno = deptno;
		this.dname = dname;
		this.sal = sal;
		this.hiredate = hiredate;
		this.job = job;
}
	public Employee(){}

	Employee(int no){
			empno = no;
	}
	
	/** 인자로 받은 객체와 현재 객체가 같은지 구별 */
	boolean equals(Employee emp){
		return this == emp;
	}
	
	void details(){
		System.out.printf("사원번호 : %d	이름:%s 부서번호:%d	급여:%d	입사일:%s \n"
							, empno, ename, deptno, sal, hiredate);
	}
}

