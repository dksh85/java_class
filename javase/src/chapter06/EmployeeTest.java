package chapter06;

public class EmployeeTest {
	public static void main(String[] args) {
		//객체생성방법
		//new 생성자명([인자값]);
		
		
		//속성, 함수 접근 방법:	객체.속성명		객체.함수명([인자값])
	
		new Employee().details();
		
		Employee emp1;	// Employee의 객체를 저장하는 변수 선언
		
		emp1 = new Employee(1, "홍길동", 10, "sales", 1000, "20160405", "SALESMAN");
//		emp1.empno = 1;
//		emp1.ename = "홍길동";
//		emp1.deptno = 10;
//		emp1.hiredate = "20160405";
//		emp1.job = "SI";
//		emp1.sal = 30000;
//		
		Employee emp2 = new Employee(2, "길동홍", 10, "sales", 1500, "20150305", "SALESMAN");
		emp1.details();
		emp1.equals(emp2);
		
		System.out.println();
		
	}
}
