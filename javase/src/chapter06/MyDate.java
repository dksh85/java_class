package chapter06;

public class MyDate {
	private int year, month, day;
	
	public void setYear(int year){
		if(year >= 1992){
			this.year = year;
		}else{
			System.out.println("잘못된 값입니다. 1992년 이후로 설정하세요");
		}
	}
	
	public void setMonth(int month){
		if(month < 1 || month > 12){
			System.out.println("잘못된 값입니다. 1월 부터 12월 사이로 설정하세요");
		}else{
			this.month = month;
		}
	}
	
	public void setDay(int day){
		if(day >= 1 && day <= 31){
			if(isValid(day)){
				this.day = day;
			}else{
				System.out.println("잘못된 값입니다. 다시 설정하세요");
			}
		}
		else{
			System.out.println("잘못된 값입니다. 1일 부터 31일 사이로 입력하세요");
		}
	}

	/** 작은 달에 대한 날짜의 유효성만 검사 */
	private boolean isValid(int day2) {
		if((month == 2 && day >28)  ||
				(month==4 || month ==6 || month == 9 || month == 11) && day > 30)
		return false;
		
		if(year == 1992 && month == 1 && day < 22) return false;
		
		return true;
	}

	public int getYear() {
		return year;
	}

	public int getMonth() {
		return month;
	}

	public int getDay() {
		return day;
	}
	
	public String toString(){
		if(year ==0 || month == 0 || day ==0){
			return "잘못된 날짜 형식입니다. 다시 설정하세요";
		}else{
			return year + "." + month + "." + day;
		}
	}
	
	public void setDate(int year, int month, int day){
		setYear(year);
		setMonth(month);
		setDay(day);
	}
	
}
