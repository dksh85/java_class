package chapter06;

public class MyDateTest {
	public static void main(String[] args) {
		MyDate d1 = new MyDate();
//		d1.year = -2016;
//		d1.month = 13;
//		d1.day = 32;
		d1.setYear(1991);
		d1.setMonth(13);
		d1.setDay(32);
		System.out.println(d1.toString());
		System.out.println("==================");
		d1.setDate(1992, 1, 23);
		System.out.println(d1.toString());
		System.out.println("==================");
		d1.setDate(2016, 4, 5);
		System.out.println(d1);
	}
}
