package chapter06;

public class MyMath {
	/**두개의 정수를 더하는 기능*/
	int plus(int i, int j){
//		int i = 10;
//		int j = 25;
//		System.out.println(i+j);
		return i+j;
	}
	
	/** 두개의 정수를 인자를 통해서 받아서 큰 값을 리턴 */
	
	/**
	 * method overloading
	 * : 동일한 이름의 함수를 여러개 정의
	 * 구별 : 인자 (인자의 개수, 타입, 순서)를 통해서 구별
	 * 
	 *   목적
	 *   - 기능은 같지만 인자가 다른 경우 함수를 또 정의해야 하는데
	 *   이때 같은 이름으로 함수를 정의함으로써 호출에 대한 편리성 부여
	 */
	
	int bigger(int i, int j){
		if (i > j) return i;
		else return j;
	}
}
