package chapter06;

/**
 * Pass By Value : 대입, 리턴, 인자 전달시 변수라면 변수의 메모리 값을 전달 한다.
 * 					
 *
 */

class Param{
	public int primitive(int data){
		return data+=5;
	}
	public void reference(MyDate date){
		date.setDate(2016, 4, 8);
	}
	
	public MyDate reference2(MyDate date){
		MyDate d1 = new MyDate();
		d1.setDate(date.getYear(), date.getMonth(), 5);
		return d1;
	}
}

public class PassByValueTest {
	public static void main(String[] args) {
		int i = 10;
		int j = i;
		MyDate d1 = new MyDate();

		Param p = new Param();
		p.primitive(i);
		System.out.printf("함수 호출 후 i: %d \n", i);
		i = p.primitive(i);
		System.out.printf("함수 호출 후, 리턴 받은 경우 i: %d \n", i);
		p.reference(d1);
		System.out.println("함수 호출 후 d1 리턴 안받음 : "+d1);


		i+= 10;
		System.out.printf("i: %d	j:%d\n", i, j);
		MyDate d2 = d1;
		d1.setDate(2015, 4, 6);
		System.out.println(d2);

	}
}
