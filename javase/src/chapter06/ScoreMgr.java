package chapter06;

public class ScoreMgr {
	private StudentScored[] list;
	private int idx; //저장 위치를 나타내는 index, 저장 개수를 표시
	public ScoreMgr(){
		list = new StudentScored[5];
	}
	
	public void add(StudentScored ss){
		if(idx >= list.length){
			StudentScored[] temp = new StudentScored[list.length+5];
			System.arraycopy(list, 0, temp, 0, list.length);
			list = temp;
		}
		//저장할 공간이 있는 경우
		list[idx++] = ss; //새로 입력받은 데이터를 배열에 저장하고 idx를 증가 시켜 다음 저장할 위치로 변경
	}

	public void findMax(){
		double max = 0;
		int maxIndex = 0;
		for (int i = 0; i< idx; i++){ // 저장한 개수까지 반복해서 max를 찾기
			if(max < list[i].getAvg()){
				max = list[i].getAvg();
				maxIndex = i;
			}
		}
		System.out.println(list[maxIndex]);
	}

	public void findMin(){
		double min = 100;
		int minIndex = 0;
		for (int i = 0; i< idx; i++){ // 저장한 개수까지 반복해서 max를 찾기
			if(min > list[i].getAvg()){
				min = list[i].getAvg();
				minIndex = i;
			}
		}
		System.out.println(list[minIndex]);
	}

	public void printScore(){
		for (int i = 0; i < idx; i++){
			System.out.println(list[i]);
		}
	}
	
	
	
}//end of class
