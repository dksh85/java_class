package chapter06;

public class ScoreMgrForStructure {
	private String[] names;
	private int[] 	dbs;
	private int[] 	javas;
	private int[] 	nets;
	private int[]		totals;
	private double[] avgs;
	private int idx;
	private int num=2;
	public ScoreMgrForStructure(){
		names = new String[num];
		dbs = new int[num];
		javas = new int[num];
		nets = new int[num];
		totals = new int[num];
		avgs = new double[num];
	}
	public void add(String name, int db, int java, int net){
		if(idx>=names.length){
			String[] nameTemp = new String[names.length+num]; 
			int[] dbTemp = new int[names.length+num]; 
			int[] javaTemp = new int[names.length+num]; 
			int[] netTemp = new int[names.length+num]; 
			int[] totalTemp = new int[names.length+num]; 
			double[] avgTemp = new double[names.length+num]; 
			System.arraycopy(names, 0, nameTemp, 0, names.length);
			System.arraycopy(dbs, 0, dbTemp, 0, names.length);
			System.arraycopy(javas, 0, javaTemp, 0, names.length);
			System.arraycopy(nets, 0, netTemp, 0, names.length);
			System.arraycopy(totals, 0, totalTemp, 0, names.length);
			System.arraycopy(avgs, 0, avgTemp, 0, names.length);
			names = nameTemp;
			dbs = dbTemp;
			javas = javaTemp;
			nets = netTemp;
			totals = totalTemp;
			avgs = avgTemp;
		}
		this.names[idx] = name;
		this.dbs[idx] = db;
		this.javas[idx] = java;
		this.nets[idx] = net;
		totals[idx] = dbs[idx]+javas[idx]+nets[idx];
		avgs[idx]  = totals[idx] / 3.0;
		idx++;
	}
	public void findMax(){
		double max = 0;
		int maxIndex = 0; 
		for (int i = 0; i <idx; i++) {
			if(max <avgs[i]){
				max = avgs[i];
				maxIndex = i;
			}
		}
		System.out.println("최고 평균 학생 정보");
		System.out.printf("%s\t%d\t%d\t%d\t%d\t%.2f\n",
			  names[maxIndex], dbs[maxIndex], javas[maxIndex],nets[maxIndex], totals[maxIndex], avgs[maxIndex]);
	}
	public void findMin(){
		double min = 100;
		int minIndex = 0; 
		for (int i = 0; i < idx; i++) {
			if(min <avgs[i]){
				min = avgs[i];
				minIndex = i;
			}
		}
		System.out.println("최저 평균 학생 정보");
		System.out.printf("%s\t%d\t%d\t%d\t%d\t%.2f\n",
				names[minIndex], dbs[minIndex], javas[minIndex],nets[minIndex], totals[minIndex], avgs[minIndex]);
	}
	public void totalAvg(){
		double totalAvg = 0;
		for (int i = 0; i <idx; i++) {
			totalAvg += avgs[i];
		}
		System.out.printf("전체 평균 점수 ::%.2f\n",  totalAvg/idx);
	}
	
	public void printScore(){
		for (int i = 0; i <idx; i++) {
			System.out.printf("%s\t%d\t%d\t%d\t%d\t%.2f\n",
				  names[i], dbs[i], javas[i],nets[i], totals[i], avgs[i]);
		}
	}
}






