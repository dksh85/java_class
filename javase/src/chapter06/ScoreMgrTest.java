package chapter06;

public class ScoreMgrTest {

	public static void main(String[] args) {
		ScoreMgr mgr = new ScoreMgr();
		
		mgr.add(new StudentScored("1111","홍길동", 80, 90, 85, 87));
		mgr.add(new StudentScored("1112","길동홍", 80, 70, 85, 77));
		mgr.add(new StudentScored("1113","동글이", 90, 100, 85, 89));
		
		mgr.printScore();
		System.out.println("=========================================");
		mgr.findMax();
		System.out.println("=========================================");
		mgr.findMin();
		System.out.println("=========================================");
		//mgr.totalAvg();;
	}

}
