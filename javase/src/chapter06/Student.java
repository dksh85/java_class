package chapter06;

public class Student {
	
	String hakbun;
	String name;
	String joomin;
	//학번( hakbun), 이름, 주민(joomin)
	
	//details() : 학생 정보를 출력하는 기능
	
	void details(){
		System.out.printf("학번 : %s		이름: %s		주민번호: %s",
								hakbun, name, joomin);
	}
}
