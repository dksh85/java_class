package chapter06;




/**
 *	VO (Value Object) == DTO (Data Transfer Object)
 *	값만 저장하는 객체 
 *
 */
public class StudentScored {
	private String hakbun;
	private String name;
	private int db;
	private int java;
	private int net;
	private int web;
	private int total;
	private double avg;
	
	public StudentScored(){
	}


	public StudentScored(String hakbun, String name, int db, int java, int net, int web) {
		super();
		this.hakbun = hakbun;
		this.name = name;
		this.db = db;
		this.java = java;
		this.net = net;
		this.web = web;
		total = db+java+net+web;
		avg = total/4.0;
	}

	
	public int getWeb() {
		return web;
	}


	public void setWeb(int web) {
		this.web = web;
	}


	public String getHakbun() {
		return hakbun;
	}



	public void setHakbun(String hakbun) {
		this.hakbun = hakbun;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public int getDb() {
		return db;
	}



	public void setDb(int db) {
		this.db = db;
	}



	public int getJava() {
		return java;
	}



	public void setJava(int java) {
		this.java = java;
	}



	public int getNet() {
		return net;
	}



	public void setNet(int net) {
		this.net = net;
	}



	public int getTotal() {
		return total;
	}



	public void setTotal(int total) {
		this.total = total;
	}



	public double getAvg() {
		return avg;
	}



	public void setAvg(double avg) {
		this.avg = avg;
	}



	@Override
	public String toString() {
		return "[hakbun=" + hakbun + ", name=" + name + ", db=" + db + ", java=" + java + ", net=" + net
				+ ", web =" + "total=" + total + ", avg=" + avg + "]";
	}

	
}
