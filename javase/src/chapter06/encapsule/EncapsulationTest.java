package chapter06.encapsule;

import chapter06.Employee;

public class EncapsulationTest {
	public static void main(String[] args) {
		Employee emp = new Employee();
// empno는 public 으로 선언되어 있으므로 다른 패키지의 상속관계가 없는 경우도 접근 가능
		emp.empno = 10; 
//	ename은 접근제한자를 생략했기 때문에 다른 패키지에서는 접근 불가.	
//	emp.ename = '홍길동';
//	접근 안됨.
//		emp.deptno = 10;
	}
}
