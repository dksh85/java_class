package chapter07;

import java.util.Calendar;

/**
 *	abstract : 추상화 
 *		클래스 : 객체 생성불가
 *				상속 전용으로 사용, 다형성 전용으로 사용
 *		메서드 : 메서드 작성시 구현부가 없음
 *				=> sub에 메서드 override를 강제로 위임.
 *
 */

abstract class Abst{}

abstract class Animal{
	String name;
	String kind;
	int age;
	Animal(){}

	public Animal(String name, String kind, int age) {
		super();
		this.name = name;
		this.kind = kind;
		this.age = age;
	}

	public void test(){
		System.out.println("test...");
	}
	public abstract void bark();
	public abstract void special();
}

class Dogs extends Animal{
	
	public Dogs() {
		super();
	}

	public Dogs(String name, String kind, int age) {
		super(name, kind, age);
	}

	@Override
	public void bark() {
		System.out.println("멍멍");
	}
	
	public void special(){ keep(); }
	
	public void keep(){
		Calendar cal = Calendar.getInstance();
		int time = cal.get(Calendar.HOUR_OF_DAY);
		if (time >= 7 && time<=22){
			System.out.println("잘 지켜요~~~");
		}
		else{
			System.out.println("쿨쿨 zzz");
		}
	}
}

class Duck extends Animal{
	public Duck(){}

	public Duck(String name, String kind, int age) {
		super(name, kind, age);
	}
	
	public void bark(){
		System.out.println("꽥꽥");
	}
	
	public void special(){
		fly();
	}
	
	public void fly(){
		if(kind!=null && kind.equals("집오리")){
			System.out.println("오리는 날 수 없다 엄마에게 혼났죠");
		}
		else{
			System.out.println("날아올라~~");
		}
	}
}

/*	추상 메서드를 포함한 추상 클래스를 상속 받았지만 
 * 	현재 단계에서는 구현하기 어려울 때 sub에 위임하기 위해 추상 클래스로 선언한다. 
 */
abstract class Mammal extends Animal{
}

public class AbstractTest {
	public static void main(String[] args) {
		//	추상 클래스이므로 객체 생성 불가
		//Abst  a = new Abst();
		//추상 클래스는 객체를 생성할 수 없지만 배열객체는 생성할 수 있다. ==> 다형성
		Animal[] animal = new Animal[2];
		animal[0] = new Dogs();
		animal[1] = new Duck("채리필터", "집오리", 17);
		
		for(int i = 0; i < animal.length; i++){
			animal[i].bark();
			animal[i].special();
			/*
			if(animal[i] instanceof Duck){
				Duck d = (Duck) animal[i];
				d.fly();
			}else if (animal[i] instanceof Dogs){
				Dogs d = (Dogs)animal[i];
				d.keep();
			}
			*/
		}
	}
}



