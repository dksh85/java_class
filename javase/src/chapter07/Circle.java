package chapter07;

public class Circle extends Shape{
	
	Circle(){
	}
	
	
	public Circle(double width, double height) {
		super(width, height);
	}


	public double area(){
		double r = getWidth()/2;
		return r*r*3.14;
	}
	
	public String toString(){
		return "원 - " + super.toString();
	}
}
