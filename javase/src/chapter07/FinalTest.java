package chapter07;


/**
 *	final : 변경 불가
 *		class : 상속 불가 (객체변경 불가)
 *		method : 재정의 금지(Override 금지)
 *		variable : 값 변경 불가
 *		 
 *
 */

//class SubString extends String{} 사용불가
//class SubString extends System{} 사용불가
//public final class String{}으로 선언되어 있음.

//**** method overriding ****
class Super{
	public final void test(){ //method overriding 불가
		System.out.println("test");
	}
}

class Sub extends Super{
//	public void test(){} //error
}

//**** variable ****
public class FinalTest {
	public static void main(String[] args) {
		final double PI = 3.14;
		//PI = 3.141; //값 변경 불가
	}
}
