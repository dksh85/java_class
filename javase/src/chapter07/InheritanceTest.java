package chapter07;

public class InheritanceTest {
	public static void main(String[] args) {
		Student s1 = new Student("홍길동", "160406-3014111", "20160301");
		System.out.println(s1.toString());
		
		Teacher t1 = new Teacher("아이유", "900318-2010201", "노래");
		System.out.println(t1);
	}
}
