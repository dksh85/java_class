package chapter07;

public class Person {
	String name;
	String joomin;

	public Person(){}

	public Person(String name, String joomin) {
		super();
		this.name = name;
		this.joomin = joomin;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJoomin() {
		return joomin;
	}

	public void setJoomin(String joomin) {
		this.joomin = joomin;
	}
	
	@Override
	public String toString() {
		return "name=" + name + ", joomin=" + joomin;
	}
	
}
