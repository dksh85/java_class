package chapter07;

import chapter06.MyDate;

public class PolymorphismTest {
	public static void main(String[] args) {
		/*
		 * 	Reference 다형성
		 * 	: Super 타입의 변수는 Sub 타입의 객체를 참조 할 수 있다.
		 * 	==> 모든 sub 객체는 super 타입으로 자동 형변환 된다.
		 * 
		 */
		
		Person p1 = new Person();
		p1 = new Student();		//다형성
		p1 = new Teacher();		//다형성
		
		
		/* 다형성 안되는 것들
		MyDate d1 = new Person();	// 상속 관계가 아니기 때문에 컴파일 오류
		Student s1 = new Person(); // 상속 관계라도 자식은 부모를 참조할 수 없다.
		Student s2 = new Teacher();
		Teacher t1 = new Student();
		*/
		
		/**
		 * 	Reference type casting : 다형성 관계에서만 가능 
		 * 		upcasting(자동 형변환)	: sub 타입의 객체가 super 타입으로 형변환
		 * 		down casting(명시적 형변환): super타입의 변수가 sub 타입으로 형변환
		 * 								==> 다형성 관계에서만 가능
		 */
		
		Student	s1 = new Student();
		Person	p2 = s1;	// up casting(자동 형변환)
		Teacher t1 = new Teacher();
		p2 = t1;
		
		Teacher t2 = (Teacher)p2;	// down casting
		// down casting 아님 ==> ClassCastingException 발생
		//	Student s3 = (Student)p2;			
		Person p3 = new Person();
		//Teacher t3 = (Teacher)p3; // 자식으로 부모생성 오류

		/*shadown Impact : 다형성 관계에서 sub에 새로 추가된 속성, 메서드에 접근 불가
		 * 
		 */
		
		Person p4 = new Student();
		//p4.getHackbun();	//Person에 선언되어 있지 않기 때문에 사용 불가

		//down casting : sub에 추가된 속성, 메서드를 사용하기 위해서
		Student s4 = (Student) p4;
		s4.getHakbun();

		/*
		 * Virtual Invocation : 다형성 관계에서 재정의된 메서드(Override)가 호출됨.
		 */

		System.out.println(p4.toString());

		/*
		 * 	instanceof	: 객체 타입 비교 하는 연산자
		 * 	- 해당 타입이면 true 아니면 false를 
		 * 	형식]	객체 instanceof Type
		 * 
		 * 
		 * 주의점 : intanceof로 타입 비교시 super 타입을 먼저 비교하면
		 * 			모든 sub는 super로 자동 형변환되므로 super 타입을 먼저 비교하면
		 * 			true가 된다.
		 * 			
		 */
		
		
		Person p5 = new Student();
		if (p5 instanceof Student) {
			Student s5 = (Student) p5;
			System.out.println("학번: ");
			System.out.println(s5.getHakbun());
		}
		else if(p5 instanceof Teacher){
			Teacher t5 = (Teacher) p5;
			System.out.println("p5는 티쳐입니다");
		}

		System.out.println("main end.");
	}
}