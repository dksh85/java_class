package chapter07;

public class Rectangle extends Shape{

	public Rectangle() {
		super();
	}

	public Rectangle(double width, double height) {
		super(width, height);
	}
	
	public double area(){
		return getWidth()*getHeight();
	}
	
	public String toString(){
		return "사각형 - " + super.toString();
	}
	
}
