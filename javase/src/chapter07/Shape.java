package chapter07;

public abstract class Shape {
	private double width;
	private double height;
	public Shape(){}
	
	public Shape(double width, double height) {
		super();
		this.width = width;
		this.height = height;
	}
	
	public abstract double area();
	
	@Override
	public String toString() {
		return "width=" + width + ", height=" + height +" 도형의 너비 : " + area();	
		// hook 기법 =>
	}



	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	
}
