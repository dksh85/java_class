package chapter07;

public class ShapeTest {
	public static void main(String[] args) {
		Shape[] shape = new Shape[3];
		shape[0] = new Circle(5,5);
		shape[1] = new Triangle(5,5);
		shape[2] = new Rectangle(5,5);
		for(int i = 0; i< shape.length; i++){
			System.out.println(shape[i]);
		}
	}
}
