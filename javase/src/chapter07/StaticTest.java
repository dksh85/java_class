package chapter07;



/**
 *	static : 정적인 특성 부여
 *		- 클래스가 로딩될 때 한번 처리됨. 
 *		속성 : 클래스가 로딩될 때 메모리(method Area)에 딱한번 할당 됨.
 *			-	Class Variable, static-variable
 *			-	객체 생성 없이 클래스명으로 접근 가능
 *			ClassName.attribute
 *			-	모든 객체가 공유하는 공유 변수가 됨.
 *		method : 클래스가 로딩될 때 한번 링킹됨.
 *			==> 객체 생성없이 클래스명으로 메서드 호출
 *			ClassName.methodName([args]);
 *			주의점: : non-static 속성, 메서드에 접근 불가.
 *
 *		static block : 클래스가 로딩될 대 한번 수행되는 블럭
 *			목적 : static한 속성 초기화, 프로그램에서 단 한번 수행되는 기능
 *
 *		instance block : 객체가 생성될 때 마다 생성자 보다 먼저 수행됨.
 *			목적 : 어떤 생성자로 객체를 생성하던 동일한 기능을 수행할 경우
 *				 instance block에서 작성한다.	
 */

class Count{
	static int scount;
	int icount;
	public Count(){
		System.out.println("생성자 수행됨. ");
		scount++;
		icount++;
	}
	
	static{
		System.out.println("static block 수행됨");
	}
	
	{ //instance block
		System.out.println("instance block 수행됨");
	}

	//주의점: : non-static 속성, 메서드에 접근 불가.
	public static void sprint(){
		System.out.printf("scount:%d\n",scount);
//		System.out.printf("icount:%d\n",icount);
	}
	
	public void print(){
		System.out.printf("scount:%d icount:%d \n", scount, icount);
	}
}

public class StaticTest {
	public static void main(String[] args) {
		Count.scount++;
		System.out.println(Count.scount);
		
		Count c1 = new Count();
		c1.print();
		
		Count c2 = new Count();
		c2.print();
		Count.sprint();
	}
}