package chapter07;

public class Teacher extends Person {
	String subject;
	public Teacher(){}
	public Teacher(String name, String joomin, String subject){
		this.name = name;
		this.joomin = joomin;
		this.subject = subject;
	}

	/*
	@Override
	public String toString() {
		return "Person [name=" + name + ", joomin=" + joomin + ", subject=" + subject + "]";
	}
	*/
	
	@Override
	public String toString(){
		return super.toString() + ", subject= " + subject; 
	}
}
