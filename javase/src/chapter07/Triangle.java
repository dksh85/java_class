package chapter07;

public class Triangle extends Shape{
	public Triangle(double width, double height){
		super(width, height);
	}
	
	public double area(){
		return getWidth()*getHeight()/2;
	}
	
	public String toString(){
		return "삼각형 + " + super.toString();
	}
}
