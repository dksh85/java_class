package chapter08;

interface SuperInter extends Trans, Cloneable{
	void test();
}

interface Flyer{ //final 못붙임 public 또는 생략만 가능
	public void fly();
}

class SuperClass{
	public void start(){
		System.out.println("뛰어 갈 텐데.... ");
	}
	public void fly(){
		System.out.println("날아 갈 텐데.... ");
	}
}

class SuperMan extends SuperClass implements SuperInter, Flyer{
	public void stop(){
		System.out.println("동작 그만");
	}
	public void test(){
		System.out.println("test");
	}
}

public class InterfaceTest {
	public static void main(String[] args){
		//interface는 추상클래스처럼 객체 생성 불가
		//Flyer f = new Flyer();
		//배열, 자식참조 가능
		//interface는 추상클래스 처럼 배열 객체 생성, 다형성에 sub를 참조 할 수 있다.
		
		SuperMan clark = new SuperMan();
		
		Flyer f = clark;
		Trans t = clark;
		SuperInter s = clark;
		Cloneable c = clark;
	}
}
