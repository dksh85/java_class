package chapter08;

public interface Trans {
	int MOVING=10;
	public static final int STOP = 20;
	public abstract void start();
	void stop(); //public abstract

}
