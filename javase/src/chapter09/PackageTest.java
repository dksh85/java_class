package chapter09;

import chapter06.MyDate;

/**
 * 패키지 : 자바 클래스를 관리하기 위해 선언
 * 	- 패키지는 컴파일시 디렉토리로 변경됨.
 * 	- 클래스 이름은 package명.클래스명이 됨.
 * 	- 같은 패키지 안에서는 클래스 이름으로 참조할 수 있지만 패키지가 
 * 		다른경우 패키지 이름까지 작성해야 Compliler, JVM이 인식할 수 있다.
 * 	- 클래스 참조
 * 		- 패키지가 같은 경우 		클래스이름
 * 		- 패키지가 다른 경우
 * 			1. 패키지명.클래스명으로 사용
 * 			2. import로 미리 등록
 * 				2.1 import 패키지명.클래스명
 * 				2.2	import 패키지명.*;		//해당 패키지의 클래스만 import 한다.
 * 	- 패키지명 규칙
 * 		1. 일반 식별자와 같음
 * 		2. java로 시작할 수 없다.
 * 		3. 패키지명 사이에 공백 불가
 * 	- java.lang 패키지는 컴파일러가 import한다.
 * 
 * 	- 식별
 * 		클래스 : 같은 패키지 내에서 클래스 이름을 동일하세 선언할 수 없다.
 * 		메세드 : 클래스 내에서 메서드 선언시 동일한 이름으로 선언할 경우
 * 				인자의 개수, 인자의 타입, 인자의 순서가 달라야 한다.
 * 		속성	:	클래스 내에서 동일한 이름의 속성을 선언할 수 없다.
 * 		로컬변수:	블럭내에서 동일한 이름의 변수를 선언할 수 없다.
 *		
 *		속성과 로컬변수의 이름은 동일할 수 있으나 구별하기 위해 속성 앞에 this.을 붙인다. 	
 *
 */

import chapter07.Circle;
import chapter06.*;
import chapter06.encapsule.*;
public class PackageTest {
	public static void main(String[] args) {
		chapter06.MyDate d = new MyDate();
		
	}
}