package chapter10;

class Super{
	public int div(int i, int j) throws MyException{
		if( j == 0 ) throw new MyException("0으로 나눌 수 없습니다");
		return i/j;
	}
}
// 부모측보다 자식 exception을 보내거나 안보내거나 할 수 있다.
// 하지만 부모측 exception보다 부모 exception이 던져지면 안된다.
class Sub extends Super{
	public int div(int i, int j){ //throws Exception{
		System.out.printf(" %d / %d = %d ", i, i, i/j);
		return i/j; // 이런 경우 아무 exception이 보내지지 않는다.
	}
}

public class Exception4 {
	public static void main(String[] args) {
		Super s = new Sub();
		
		try{
			s.div(256, 2);
		}
		catch (MyException e){ // MyException 보다 상위 클래스는 잡지 못함 Exception과 같이
			e.printStackTrace();
		}
	}
}
