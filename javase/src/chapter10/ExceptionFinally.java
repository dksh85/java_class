package chapter10;

public class ExceptionFinally {
public static void main(String[] args) {
		
		int data = 0;
		int result = 0;
		try{
			result = 256/Integer.parseInt(args[0]);
		} /*catch (ArithmeticException e){
			System.out.println("0을 입력하셨습니다. 기본 처리됩니다.");
			result = 1;
		}*/ catch(NumberFormatException e){
			System.out.println("정수를 입력하지 않았습니다. 기본 처리됩니다.");
			result = 2;
			return;
		} catch(ArrayIndexOutOfBoundsException e){
			System.out.println("데이터를 입력하지 않았습니다. 기본 처리됩니다.");
			result = 3;
		} /*catch(Exception e){
			System.out.println("오류 발생 기본 처리 됩니다.");
			System.out.println(e.getMessage()); // / by zero 가 아닌 한글로 출력하게 만들어 보기 상속으로
			result = 1;
		} */ finally{
			/* finally : try문이 수행되면 반드시 수행된 문으로 꼭 수행되야 하는 코드를 작성
			 * 			ex) 자원반납 : File, Network, DB...
			 * 
			 * 	1. 오류가 발생하지 않아도 수행됨.
			 * 	2. 오류가 발생해도 수행됨.
			 * 	3. 오류가 발생했지만 처리가 되지 않은 경우에도 수행됨.
			 * 	4. return 문이 있어도 수행됨.
			 */
			System.out.println("Finally 수행됨");
		}

		System.out.printf("결과 : %d\n", result);
	}
}
