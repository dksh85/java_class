package chapter10;

public class ExceptionTest {
	public static void main(String[] args) {
		String[] greet = {"hello", "hi", "Good Morning"};
		
		for( int i = 0; i <=3; i++ ){
			try{
				System.out.printf("인사%d: %s\n", i, greet[i]);
			}catch(ArrayIndexOutOfBoundsException ai){
				System.out.println(ai.getMessage());
				// 오류 메세지와 오류가 발생한 위치 정보를 출력
				ai.printStackTrace();
			}
		}
		System.out.println("main end ... ");
	}
}
