package chapter10;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

class MyUtile{
	/**
	 * 예외 선언적 처리
	 * 	: 메서드를 호출 한 곳에서 예외 처리하도록 위임
	 * 	방식]
	 * 	[modifier] return_type methodName([args])[throws 예외, ...]{
	 * 
	 * }
	 * 
	 * 목적
	 * 1. 예외 처리를 호출한 곳으로 위임함으로써 호출한 곳의 상황에 맞게 처리하도록 예외처리의 다양성을 줌.
	 * 2. 여러 메서드에 걸처 오류가 발생하지만 처리 방법이 정해져 있다면 오류를 던져서 한꺼번에 공통 처리. 
	 * 3. 메서드를 수행한 정상적인 결과는 return을 통해 전달하고
	 * 		비정상적인 상황인 Exception을 통해 전달.
	 */
		// 예외처리 다양하게, 오류마다 처리하면 코드가 길어지기 때문에 공통처리를 한다.
	public void load(String fileName) throws FileNotFoundException{
		FileInputStream is = null;
		is = new FileInputStream(fileName);
		//checked exception
	}
	
	public int div(int a, int b) throws MyException{
		//throw 예외객체 : 예외를 발생 시킴.
		if( b == 0) throw new MyException("0으로 나눌수 없습니다.");
		return a/b;
		//return 하거나 throw 하거나 
	}
}

public class ExceptionTest3 {
	public static void main(String[] args) {
		MyUtile m = new MyUtile();
		try {
			m.load("c:/aaa.txt"); //checked
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try{
		 m.div(256, 0); //runtime
		}catch (MyException e){
			e.printStackTrace();
		}
	}
}
