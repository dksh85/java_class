package chapter10;

/**
 *  사용자 정의 Exception :
 *  - 프로그램에 맞게 Exception을 만들어서 사용
 *  - UnChecked Exception : 
 *  	- RuntimeException 이나 RuntimeException을 상속받은 sub를 상속받아 작성
 *  - Checked Exception : 
 *  	- RuntimeException 이나 RuntimeException을 상속받은 sub를 제외하고 상속받아 작성
 *  	ex) Exception , 
 *
 */

public class MyException extends Exception {
	public MyException(){
		
	}

	public MyException(String msg){
		super(msg); // getMessage()에 의해 메세지를 추출 할 수 있다.
	}
	
}
