package chapter11;

class Test{}
public class ObjectTest {
	public static void main(String[] args) {
		System.out.println(new Test().toString());	//toString()가 호출된다.
		Person p1 = new Person("kdn", "11");
		Person p2 = new Person("kdn", "11");
		System.out.printf("p1==p2 : %b\n", p1.equals(p2));
	}
}