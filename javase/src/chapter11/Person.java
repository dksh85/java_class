package chapter11;

public class Person {
	String name;
	String joomin;

	public Person(){}

	public Person(String name, String joomin) {
		super();
		this.name = name;
		this.joomin = joomin;
	}
	
	/**
	 *	toString()	:	객체의 내용을 문자열로 리턴하는 기능 
	 *		- String 타입에 객체를 +로 연결할 때 toString()가 자동 호출됨
	 *		- System.out 으로 객체가 출력될 때 호출됨
	 *		- 필요에 따라 override 한다.
	 */
	

	/**
	 *	equals(Object obj)	:	객체 내용 비교를 위한 메서드 
	 *		== 는 메모리의 값을 비교하므로 객체는 참조값(hashcode)를 비교하게 되서
	 *		같은 객체가 아니면 false.
	 *		내용 비교를 위해서 equals(Object obj)를 override한다.
	 * 
	 */
	
//	@Override
//	public boolean equals(Object obj) {
//		if( obj !=null && obj instanceof Person){
//			Person p = (Person) obj;
//			if( name.equals(p.name) && joomin.equals(p.joomin)){
//				return true;
//			}
//		}
//		return false;
//	}
	
	
	/**
	 *	hashcode()	:	객체의 hashcode를 리턴하는 기능
	 *		-	Set API를 사용할 때 hashcode를 override 해야 한다.
	 *		-	EJB에서 객체 비교할 때
	 * 
	 */

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJoomin() {
		return joomin;
	}

	public void setJoomin(String joomin) {
		this.joomin = joomin;
	}
	
	@Override
	public String toString() {
		return "name=" + name + ", joomin=" + joomin;
	}
	
}
