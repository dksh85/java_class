package chapter11;

public class StringTest {
	public static void main(String[] args) {
		/**
		 *  String :
		 *   - 유일하게 + 연산이 허용되는 레퍼런스(참조0 타입
		 *   ==> String + any type 	: 문자열에 연결
		 *   
		 *   - 유일하게 new를 사용하지 않고 객체 사용
		 *   	- new 사용하지 않은 경우 :	method area에 Constance Pool 에 상수로 저장됨.
		 *   	- new String([argsd]) : heap에 객체가 생성됨.
		 */
		String str1 = "hello";
		String str2 = "hello";
		String str3 = new String("hello");
		String str4 = new String("hello");
		
		System.out.printf("str1 == str2 : %b\n", str1 == str2);
		System.out.printf("str2 == str3 : %b\n", str1 == str3);
		System.out.printf("str3 == str4 : %b\n", str3 == str4);
		
		String[] token = "hello world java kdn".split(" ");
		
		/*1.5 
			for(type 변수 : 배열 or 컬렉션)
		 *	: 배열 or 컬렉션의 데이터를 처음부터 끝까지 한 개씩 추출 
		 *	일반 for 문보다 성능이 좋은 조건, increment 없기 때문
		 */
		for(int i = 0; i < token.length; i++){
			String t = token[i];
		}
		
		for (String data : token){
			System.out.println(data);
		}
		

		str1+=" world"; 	// 기존 객체를 변경하지 않고 새로 생성 stack이 아닌 heap 에다 넣음 (new)
		str2+=" world";
		System.out.printf("str1 == str2 : %b\n", str1==str2);
		
		StringBuffer sb1; //synchronize가 있음 동기화 되어 있음
		StringBuilder sb2 = new StringBuilder(100);
		//동기화 안되어 있음
		sb2.append("hello");
		sb2.append(" world");
		sb2.append(" java");
		
		String str5 = sb2.toString(); 	//문자열로 바꾸기
	}
}