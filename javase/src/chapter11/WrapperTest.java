package chapter11;

public class WrapperTest {
	public static void main(String[] args) {
		/**
		 *  Wrapper : 8가지 Primitive 타입을 객체화
		 *  
		 */
		
		//1.4버전 객체화	new 래퍼클래스생성자(값);
		Integer inum = new Integer(256);
		Double	dPI = new Double(3.14);
		
		//1.4버전 	레퍼를 primitive로 변경 xxxValue();
		int num = inum.intValue();
		double pi = dPI.doubleValue();
		
		//1.5버전 부터는 자동으로 변화됨
		Integer price = 25000;		//auto boxing
		int iPrice = price;			//auto outboxing
		
		//String 타입의 숫자 데이터를 primitive로 변경
		// parseXXX(String	숫자데이터) 
		// ==> format이 다를 경우 NumberFormatException
		System.out.println(Integer.parseInt("256"));
		System.out.println(Double.parseDouble("256"));
		System.out.println(Boolean.parseBoolean("true"));
		
	}
}
