package chapter12;

import java.util.Calendar;
import java.util.Date;

public class DateTest {
	public static void main(String[] args) {
		Date d1 = new Date();
		System.out.println(d1.toLocaleString());
		System.out.printf("년 : %d\n", d1.getYear());
		System.out.printf("월 : %d\n", d1.getMonth());
		System.out.printf("일 : %d\n", d1.getDate());
		System.out.printf("시간 : %d\n", d1.getHours());
		System.out.printf("분 : %d\n", d1.getMinutes());
		System.out.printf("초 : %d\n", d1.getSeconds());
		
		Calendar today = Calendar.getInstance();
		System.out.printf("년 : %d\n", today.get(Calendar.YEAR));
		System.out.printf("월 : %d\n", today.get(Calendar.MONTH)+1);
		System.out.printf("일 : %d\n", today.get(Calendar.DATE));
		System.out.printf("시간 : %d\n", today.get(Calendar.HOUR_OF_DAY));
		System.out.printf("분 : %d\n", today.get(Calendar.MINUTE));
		System.out.printf("초 : %d\n", today.get(Calendar.SECOND));
		
		int hour = today.get(Calendar.HOUR_OF_DAY);
		if (hour>=8 && hour < 13){
			System.out.println("Good morning");
		}else if(hour >=13 && hour < 16){
			System.out.println("Good afternoon");
		}else{
			System.out.println("Good evening");
		}
	}
}
