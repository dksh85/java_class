package chapter12;
/**
 *	Generic : 속성의 타입을 객체 생성시 결정
 *		- 타입대신 A~Z 의 Generic을 사용
 */

class Goods<T,K>{
	private T price;
	private K unit;
	public Goods(){
		
	}
	
	public Goods(T price, K unit){
		this.price = price;
		this.unit = unit;
	}
	
	public T getPrice(){
		return price;
	}
	
	public K getUnit(){
		return unit;
	}

	public void setPrice(T price) {
		this.price = price;
	}

	public void setUnit(K unit) {
		this.unit = unit;
	}
	
	
}


public class GenericTest{
	public static void main(String[] args) {
		Goods<Integer, Integer> g1 = new Goods<Integer, Integer> (3000,10);
		Goods<String, Double> g2 = new Goods<String, Double> ("3000",5.5);
	}
}

