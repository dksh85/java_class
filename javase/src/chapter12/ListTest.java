package chapter12;

import java.util.*;

/**
 * List : 데이터를 목록처럼 정리하는 자료구조
 * 	- 저장한 순서대로 저장됨.
 * 	- 순차적으로 저장함 (중간에 띄어넘기 저장 안됨 => IndexOutofBoundsException 발생)
 * 	- 중복 저장  
 * 	- 종류
 * 		ArrayList	: 배열 객체	,	객체 생성시 배열 size를 지정하면 좋다. 
 * 		LinkedList	: 더블 링크드 리스트, 객체 생성시 size를 설정 할 수 없다.
 * 	- indexOf(~)	lastIndexOf(~)	contains(~)		remove(~)
 * 	   메서드들은 
 * 
 * 	
 *
 */


public class ListTest {
	public static void main(String[] args) {
		Vector v;
		ArrayList a1;
		LinkedList l1;
		
		
		a1 = new ArrayList();
		a1.add(1);
		a1.add(3.14);
		a1.add("hello");
		a1.add("hello");
		a1.add(2, new Person("aaa", "111"));
//		a1.add(6, new Person("aaa", "111"));
//		index는 순차적으로 유지가 되야한는데 그렇지 않은 경우 Exception 발생
//		a1.add(6, new Person("aaa", "1111"));
		System.out.println(a1);

		//추출 할 때
		Object o = a1.get(3);
		
		//1.5버전 부터 Generic 사용
		ArrayList<Person> people = new ArrayList<Person>();
		people.add(new Person("kdn", "1971"));
		people.add(new Person("홍길동", "1971"));
		
		Person p1 = people.get(1);	//형변환 없이 추출
		int index = people.indexOf(new Person("kdn", "1971"));
		System.out.printf("index : %d \n", index); //equals 함수 오버라이딩
		
		for (Person person : people){
			System.out.println(person);
		}
		
		Iterator<Person> p = people.iterator();
		System.out.println("iterator 출력");
		while(p.hasNext()){
			System.out.println(p.next());
		}
		
	}
}
