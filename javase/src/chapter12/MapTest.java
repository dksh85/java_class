package chapter12;

import java.util.*;

public class MapTest {
	public static void main(String[] args){
		Map map1 = new HashMap();
		map1.put("1",  "hello");
		map1.put(2, "world");
		map1.put(3, new Person("kdn", "홍길동"));
		map1.put(2, "java");
		System.out.println(map1);
		
		Map emp1 = new HashMap();
		emp1.put("empno", "1");
		emp1.put("ename", "홍길동");
		emp1.put("salary", 3000);
		emp1.put("address", "서울");
		
		//1.5 Generic 적용
		//Map<String, Person> map2 = new HashMap()<String, Student>;
		//선언시에는 안되고 저장시에는 된다.
		Map<String, Person> map2 = new HashMap<String, Person>();
		map2.put("1111", new Person("홍길동", "1111"));
		
		Person p = map2.get("2222");
	
		//키값만 추출
		Iterator<String> keys = map2.keySet().iterator();
		while(keys.hasNext()){
			String key = keys.next();
			Person value = map2.get(key);

			System.out.printf("%s:%s \n", key, value);
		}
		
		
		
		
		//value 값만 추출
		
		Collection<Person> values = map2.values();
		for (Person person : values){
			System.out.println(person);
		}
	}
}