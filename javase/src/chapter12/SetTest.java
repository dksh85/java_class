package chapter12;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetTest {
	public static void main(String[] args) {
		Set s1 = new HashSet();
		s1.add(1);
		s1.add("hello");
		s1.add(3.14);
		s1.add("hello");
		System.out.println(s1);

		Iterator kyes = s1.iterator();
		while (kyes.hasNext()){
			Object object = kyes.next();
			System.out.println(object);
			
			
		}
		
		
		/*
		 * 컬렉션에 Generic 적용
		 * Collection<Type> id = new Collection<Type>();
		 *  - 지정한 type만 저장할 수 있다. 
		 *  - 추출할 때 지정한 type으로 추출되므로 형변환이 필요 없다.
		 */
		Set<Person> s2 = new HashSet<Person>();
		s2.add(new Person("aaa", "1111"));
		s2.add(new Person("bbb", "1111"));
		s2.add(new Person("ccc", "1111"));
		s2.add(new Person("aaa", "1111"));
		
		System.out.println(s2);
	}
}
