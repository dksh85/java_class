package chapter12;

public class Student extends Person{
	String hakbun;
	public Student(){
		//super(null, null);
	}
	public Student(String name, String joomin, String hakbun){
		
		/*
		 * 	생성자는 상속 되지 않지만 코드 재사용을 위해 호출가능
		 * 	super([인자...])
		 * 	주의점 : 생성자의 첫번째 명령에서만 호출 가능
		 */
		super(name, joomin);
		//this.name = name;
		//this.joomin = joomin;
		this.hakbun = hakbun;
	}
	
	
	
	/**
	 * method Override(함수 재정의):t 
	 * 부모로부터 상속받은 메서드와 동일한 이름, 동일한 인자, 동일한 리턴타입의
	 * 함수를 작성
	 * 
	 * 규칙 :
	 * 		1. 메서드 이름과, 인자는 반드시 같아야 함.
	 * 		2. 리턴타입은 동일하게 선언하거나 sub 클래스를 리턴. 
	 * 			(리턴 타입이 리퍼런스 타입이라면 sub를 리턴)
	 * 		3. access modifier는 같거나 넓은 범위로 (다형성 때문)
	 * 		4. Exception throws할 때 같거나, sub 클래스를 throws 하거나
	 * 			throws를 하지 않는다.
	 * 			==> 부모 Exception을 throws 하지 않았는데 자식이 throw하거나
	 * 			 부모에서 throw한 Exception의 super를 던지면 컴파일 에러 발생
	 * 
	 * 목적 :
	 * 		1. 부모로부터 상속 받은 메서드와 이름과 인자가 같으므로 메서드 호출하는 방식이 같고
	 * 			리턴 타입이 같으므로 메서드 호출후 처리 방법이 같게됨.
	 * 			==> 변경사항을 기존 코드를 수정하지 않고 반영할 수 있음.
	 * 
	 * 		2. 부모인지 자식인지 구별하지 않고 기능이 같다면 같은 메서드 이름으로 호출 할 수 있다.
	 * 			==> 호출에 대한 편리성
	 * 
	 * 		3. Virtual Invocation
	 * 			다형성 관계에서는 재정의된(override)된 메서드가 호출됨.
	 * 			==>	추가된 기능을 기존 프로그램 수정을 최소화 해서 반영 시킬 수 있다.
	 * 
	 * 		4. Shadow Impact를 해결
	 * 			- Shadow Impact : 다형성 관계에서 sub에 추가된 속성, 함수에 접근 불가.
	 * 			
	 */
	
	/*
	@Override
	public String toString(){
		return "name= " + name + " joomin = " + joomin + " hakbun = " + hakbun;
	}
	*/

	@Override
	public String toString(){
		/*	super : 상속 받은 메서드 중 재정의(method override)를 통해 무시된 메서드를 
		 * 			호출 할 때 super를 통해 접근
		 * 		super.methodName([args])
		 */
		
		return super.toString() + ", hakbun=" + hakbun;
	}
	public String getHakbun() {
		return hakbun;
	}
	public void setHakbun(String hakbun) {
		this.hakbun = hakbun;
	}
	
	
}
