package chapter12;

import java.util.Arrays;

public class UtilTest {
	public static void main(String[] args) {
		int[] array1 = {5,1,2,3,7,4,9};
		
		Arrays.sort(array1);
		for(int i : array1){
			System.out.println(i);
		}
		
		Person[] people = new Person[5];
		people[0] = new Person("bbb", "111");
		people[1] = new Person("ddd", "111");
		people[2] = new Person("aaa", "111");
		people[3]= new Person("kkk", "111");
		people[4] = new Person("ccc", "111");

		Arrays.sort(people);
		for(Person person : people){
			System.out.println(person);
		}
	}
}
