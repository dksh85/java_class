package wjbasic03_inherit;

public class BookMgr {
	/** 
	 * - 다형성을 배열에 적용 
	 * 다형성에 의해 모든 Sub 객체는 Super 타입으로 형변환이 되므로 Super타입의 변수만
	 * 선언한다.
	 * Magazine은 다형성에 의해 Book으로 형변환 되므로 Book[]만 선언하면 됨.
	 */
	private Book[] booklist;
	private int bookIndex;
	public BookMgr(){
		booklist = new Book[15];
	
	}
	/**
		 *	다형성을 리턴 타입에 적용
		 *	다형성에 의해 모든 Sub 객체는 Super 타입으로 형변환이 되므로 리턴 타입을
		 *	Super 타입으로 선언한다. 
		 */
	public Book findBook(String isbn){
		for(int i =0; i < bookIndex; i++){
			if(booklist[i].getIsbn().equals(isbn)){
				return booklist[i];
			}
		}
		return null;
	}
	
	/**
	 * 	다형성을 메서드 인자에 적용
	 * 	다형성
	 * @param b
	 */
	
	
	public void add(Book b){
		if(b != null){
			Book find = findBook(b.getIsbn());
			if(find!=null){
				System.out.println("이미 등록된 도서입니다.");
			}
			else{
				if(bookIndex >= booklist.length){
					Book[] temp = new Book[booklist.length + 15];
					System.arraycopy(booklist, 0, temp, 0, booklist.length);
					booklist = temp;
				}
			}
			booklist[bookIndex++] = b;
		}
	}

	/**
	 * Magazine을 maglist에 등록하는 기능
	 * @param m
	 */
	
	public void printAll(){
		for(int i = 0; i < bookIndex; i++){
			//다형성 관계에서 재정의(Override)된 메서드가 호출된다.
			System.out.println(booklist[i].toString());
		}
		System.out.println();
	}
	/**
	 * 등록된 모든 Book 정보만 출력하는 기능 
	 */
	public void printBook(){
		for(int i = 0; i < bookIndex; i++){
			if (!(booklist[i] instanceof Magazine)) {
				System.out.println(booklist[i].toString());
			}
		}
	}
	/**
	 * 등록된 모든 Magazine 정보만 출력하는 기능 
	 */
	public void printMagazine(){
		for (int i = 0; i< bookIndex; i++){
			if(booklist[i] instanceof Magazine){
				System.out.println(booklist[i]);
			}
		}
		
	}
}




