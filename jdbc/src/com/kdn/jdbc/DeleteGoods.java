package com.kdn.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.kdn.util.DBUtil;

public class DeleteGoods {
	public static void main(String[] args) {
		Connection con = null;
		PreparedStatement stmt = null;
		
		try {
			con = DBUtil.getConnection();
			String sql = " delete from goods where gno = ? ";
			stmt = con.prepareStatement(sql);
			// setXXX(int ?를 위한 index, value) //
			stmt.setInt(1, 5);		//두번째 ?에 데이터로 setting

			// executeUpdate() : insert, update, delete문을 수행하는 메서드
			int rows = stmt.executeUpdate(); 
			if(rows < 1){
				System.out.println("db delete 실패");
			}else{
				System.out.println("db delete 성공");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			DBUtil.close(stmt);
			DBUtil.close(con);
		}
	}
}
