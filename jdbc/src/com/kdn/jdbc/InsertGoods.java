package com.kdn.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.kdn.util.DBUtil;

public class InsertGoods {
	public static void main(String[] args) {
		Connection con = null;
		PreparedStatement stmt = null;
		
		try {
			con = DBUtil.getConnection();
			String sql = " insert into goods (gno, brand, maker, price, image, cno) "
					+ " values(?,?,?,?,?,?) ";
			stmt = con.prepareStatement(sql);
			// setXXX(int ?를 위한 index, value) //
			stmt.setInt(1, 5);				//첫번째 ?에 데이터로 4를 setting
			stmt.setString(2, "오렌지");		//두번째 ?에 데이터로 setting
			stmt.setString(3, "썬키스트");		
			stmt.setInt(4, 1500);
			stmt.setString(5, "image.jpg");
			stmt.setInt(6, 10);

			// executeUpdate() : insert, update, delete문을 수행하는 메서드
			int rows = stmt.executeUpdate(); 
			if(rows < 1){
				System.out.println("db insert 실패");
			}else{
				System.out.println("db insert 성공");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			DBUtil.close(stmt);
			DBUtil.close(con);
		}
	}
}
