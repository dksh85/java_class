package com.kdn.jdbc;

import java.sql.*;
import com.kdn.util.*;
public class SelectGoods {
	public static void main(String[] args) {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try{
			con = DBUtil.getConnection(); // DB 서버에 연결
			//쿼리를 수행할 객체 생성
			stmt = con.prepareStatement(" select * from goods where gno = ? ");
			stmt.setInt(1, 3);
			
			rs = stmt.executeQuery(); // 쿼리 수행
			System.out.println("상품번호\t상품이름\t제조사\t가격");
			while(rs.next()){
				String gno = rs.getString("gno");
				String brand = rs.getString("brand");
				String maker = rs.getString("maker");
				int price = rs.getInt("price");
				
				System.out.printf("%s\t%s\t\t%s\t%d\n", gno, brand, maker, price);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DBUtil.close(rs);
			DBUtil.close(stmt);
			DBUtil.close(con);
		}
	}
}
