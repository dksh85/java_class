package com.kdn.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.kdn.util.DBUtil;

public class UpdateGoods {
	public static void main(String[] args) {
		Connection con = null;
		PreparedStatement stmt = null;
		
		try {
			con = DBUtil.getConnection();
			String sql = " update goods set brand = ?, maker=?, price=?, image=?, cno=? "
					+ " where gno =? ";
			stmt = con.prepareStatement(sql);
			// setXXX(int ?를 위한 index, value) //
			stmt.setString(1, "오렌지2");		//두번째 ?에 데이터로 setting
			stmt.setString(2, "썬키스트");		
			stmt.setInt(3, 1500);
			stmt.setString(4, "오렌지2.jpg");
			stmt.setInt(5, 10);
			stmt.setInt(6, 4);

			// executeUpdate() : insert, update, delete문을 수행하는 메서드
			int rows = stmt.executeUpdate(); 
			if(rows < 1){
				System.out.println("db insert 실패");
			}else{
				System.out.println("db insert 성공");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			DBUtil.close(stmt);
			DBUtil.close(con);
		}
	}
}
