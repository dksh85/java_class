package com.kdn.employee;

public class Employee {
	private String empNo;
	private String ename;
	private String address;
	private int salary;
	
	public Employee(){}

	public Employee(String empNo, String ename, int salary, String address) {
		super();
		this.empNo = empNo;
		this.ename = ename;
		this.address = address;
		this.salary = salary;
	}

	public boolean equals(Object o) {
		if (o != null) {
			if (o instanceof Employee) {
				Employee temp = (Employee) o;
				if (temp.getEmpNo().equals(this.empNo))
					return true;
			}
		}
		return false;
	}
	

	public String toString() {
		return "empNo=" + empNo + ", ename=" + ename + ", address=" + address + ", salary=" + salary;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	

}
