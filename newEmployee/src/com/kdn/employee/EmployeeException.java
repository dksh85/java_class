package com.kdn.employee;

public class EmployeeException extends RuntimeException{
	public EmployeeException(String str){
		super(str);
	}
}
