package com.kdn.employee.dao;

import java.util.List;

import com.kdn.employee.Employee;

public interface EmployeeDao {
	public Employee findEmployee(String empno);
	public void addEmployee(Employee emp);
	public void removeEmployee(String empNo);
	public void updateEmployee(Employee emp);
	public List<Employee> searchAll();
	public void close();
}
