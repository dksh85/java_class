package com.kdn.employee.dao;

import java.util.LinkedList;
import java.util.List;

import com.kdn.employee.Employee;
import com.kdn.employee.EmployeeException;

public class EmployeeDaoForList implements EmployeeDao{
	private LinkedList<Employee> emplist;
	public EmployeeDaoForList() {
		emplist = new LinkedList<Employee>();
	}
	@Override
	public Employee findEmployee(String empno) {
		int i = findIndex(empno);
		return emplist.get(i);
	}

	@Override
	public void addEmployee(Employee emp) {
		if(emp !=null){
			int t = emplist.indexOf(emp);
			if(t > -1){
				String temp = String.format("해당 직원 %s은 이미 존재합니다.", emp.getEmpNo());
				throw new EmployeeException(temp);
			}
			else emplist.add(emp);
		}
	}
	@Override
	public void removeEmployee(String empNo) {
		emplist.remove(findIndex(empNo));
	}
	@Override
	public void updateEmployee(Employee emp) {
		int temp = findIndex(emp.getEmpNo());
		emplist.set(temp, emp);
	}
	@Override
	public List<Employee> searchAll() {
		return emplist;
	}
	@Override
	public void close() {
		System.exit(0);
		// TODO Auto-generated method stub
		
	}
	
	public int findIndex(String empNo){
		for(int i = 0; i < emplist.size(); i++){
			if (empNo != null){
				if (empNo.equals(emplist.get(i).getEmpNo())){
					return i;
				}
			}
		}
		String str = String.format("직원번호 %s 는 존재하지 않습니다", empNo );
		throw new EmployeeException(str);
	}
}
