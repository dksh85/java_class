package test;

public class ContinueTest {
	public static void main(String[] args) {
		
		for(int i = 0; i < 10; i++){
			if(i == 4) continue;
			
//			System.out.printf("%d ", i);
		}
		
		int i = 0;

		while(i < 10){
			i++;
			if(i == 4) continue;
			System.out.printf("%d ", i);
		}

		i = 0;
		do{
			i++;
			if(i == 4) continue;
			System.out.printf("%d ", i);
		}while(i <10);
	}
}
