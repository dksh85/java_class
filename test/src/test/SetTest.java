package test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class Tu<T,S>{
	T util;
	S s;

	Tu(){
		
	}
	
	Tu(T util, S s){
		this.util = util;
		this.s = s;
	}
	
	T getUtil(){
		return util;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Tu [util=").append(util).append(", s=").append(s).append("]");
		return builder.toString();
	}
	
	
}

public class SetTest {
	public static void main(String[] args) {
		Set<Integer> temp = new HashSet<Integer>();
		
		temp.add(1);
		temp.add(2);
		
		System.out.println(temp);
		
		int[] arr = { 9,8,6,5,3,6,7};
		Arrays.sort(arr);
		
		for(int i : arr){
			System.out.println(i);
		}

		
		Tu<Integer, Double> aa = new Tu<Integer, Double>(1, 3.14);
		System.out.println(aa.toString());
	}

}
