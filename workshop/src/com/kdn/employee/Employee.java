package com.kdn.employee;

import java.io.Serializable;

public class Employee implements Serializable{
	private String empNo;
	private String ename;
	private String address;
	private int salary;
	
	public Employee(){}

	public Employee(String empNo, String ename, int salary, String address) {
		super();
		this.empNo = empNo;
		this.ename = ename;
		this.address = address;
		this.salary = salary;
	}
	
	public boolean equals(Object o){
		if(o instanceof Employee){
			Employee emp = (Employee) o;
			
			if(empNo.equals(emp.getEmpNo())){
				return true;
			}
		}
		return false;
	}

	public String toString() {
		return "empNo=" + empNo + ", ename=" + ename + ", address=" + address + ", salary=" + salary;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	

}
