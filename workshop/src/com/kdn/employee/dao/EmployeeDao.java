package com.kdn.employee.dao;

import java.util.List;

import com.kdn.employee.Employee;
import com.kdn.employee.EmployeeException;

public interface EmployeeDao {
	public void close();
	public List<Employee> searchAll();
	public void addEmployee(Employee emp); 
	public void updateEmployee(Employee emp)throws EmployeeException ;
	public void removeEmployee(String empno);
	public Employee findEmployee(String empno);
}
