package com.kdn.employee.dao;

import java.util.LinkedList;
import java.util.List;

import com.kdn.employee.Employee;
import com.kdn.employee.EmployeeException;

public class EmployeeDaoForList implements EmployeeDao{
	private LinkedList<Employee> emplist;
	
	public EmployeeDaoForList(){
		emplist = new LinkedList<Employee>();
	}

	public EmployeeDaoForList(LinkedList<Employee> emplist){
		emplist = new LinkedList<Employee>();
	}

	@Override
	public void close() {
		System.exit(0);
	}

	@Override
	public List<Employee> searchAll() {
		return emplist;
	}
	
	@Override
	public void addEmployee(Employee emp) {
		if(emp != null){
			int index = emplist.indexOf(emp);
			if(index > -1){
				String msg = String.format("직원 %s는 이미 등록되어 있습니다", emp.getEmpNo());
				throw new EmployeeException(msg);
			}
			emplist.add(emp);
		}else{
			throw new EmployeeException("등록할 직원이름을 입력하세요");
		}
	}

	@Override
	public void updateEmployee(Employee emp) {
		if(emp != null){
			int index = emplist.indexOf(emp);
			if(index < 0){
				String msg = String.format("사원번호 %s를 찾을수 없습니다", emp.getEmpNo());
				throw new EmployeeException(msg);
			}
			emplist.set(index, emp);
		}
		else {
			throw new EmployeeException("수정할 정보를 입력하세요");
		}
	}
	
	public int findIndex(String empno){
		if (empno != null){
			int size = emplist.size();
			for(int i = 0; i < size; i++){
				Employee emp = emplist.get(i);
				if (empno.equals(emp.getEmpNo())){
					return i;
				}
			}
		}
		String msg = String.format("직원번호 %s를 찾을 수 없습니다", empno);	
		throw new EmployeeException(msg);
		}

	@Override
	public void removeEmployee(String empno) {
		emplist.remove(findIndex(empno));
	}

	@Override
	public Employee findEmployee(String empno) {
		return emplist.get(findIndex(empno));
	}
}
