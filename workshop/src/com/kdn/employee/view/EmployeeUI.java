package com.kdn.employee.view;
import java.awt.BorderLayout;
import java.awt.CheckboxGroup;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.kdn.employee.Employee;
import com.kdn.employee.EmployeeException;
import com.kdn.employee.dao.EmployeeDao;
public class EmployeeUI {
	private JFrame main;
	private MessageDialog  dialog;
	private JButton insertBt, updateBt, deleteBt, findBt,clearBt, exitBt;
	private JTextField empnoTf, nameTf, salaryTf, addressTf;
	private JTable  empTable;
	private DefaultTableModel empModel;
	private JScrollPane empPan;
	private JPanel addInfoPan;
	private String[] header ={"사원번호","사원이름","급여","주소"};
	private EmployeeDao  model;
	
	private ActionListener  buttonHandler = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			Object source = e.getSource();   
			try{
				if(source == clearBt) clear();
				else if(source == exitBt)model.close();
				else {
					String empno = empnoTf.getText();
					if(empno.trim().length()<1){
						empnoTf.requestFocus();
						dialog.show("사원번호를 입력하세요");
						return;
					}
					if(source==insertBt)  insert();
					else if(source == findBt) findEmployee();
					else if(source == updateBt) update();
					else if(source == deleteBt) delete();
				}
			}catch(NumberFormatException  nfe){
				dialog.show("급여는 정수로 입력하세요");
			}catch(EmployeeException  eu){
				dialog.show(eu.getMessage());
			}
		}
	};
	
	private MouseAdapter listHandler = new MouseAdapter() {
		public void mouseClicked(java.awt.event.MouseEvent e) {
			System.out.println(empTable.getValueAt(empTable.getSelectedRow(), 0));
		};
	};
	public void findEmployee(){
		Employee  emp = model.findEmployee(empnoTf.getText());
		nameTf.setText(emp.getEname());
		salaryTf.setText(""+emp.getSalary());
		addressTf.setText(emp.getAddress());
	}
	
	public void setModel(EmployeeDao  model){ 
		this.model = model;
		showList();
	}
	public void delete(){
		String empno = empnoTf.getText();
		model.removeEmployee(empno);
		showList();
		clear();
	}
	public void update(){
		String empno = empnoTf.getText();
		String ename =nameTf.getText();
		String salary = salaryTf.getText();
		String address = addressTf.getText();
		model.updateEmployee(new Employee( empno,ename, Integer.parseInt(salary),address));
		showList();
		clear();
	}
	public void insert(){
		String empno = empnoTf.getText();
		String ename =nameTf.getText();
		String salary = salaryTf.getText();
		String address = addressTf.getText();
		model.addEmployee(new Employee( empno,ename, Integer.parseInt(salary),address));
		showList();
		clear();
	}
	public void clear(){
		empnoTf.setText("");
		nameTf.setText("");
		salaryTf.setText("");
		addressTf.setText("");
	}
	
	public EmployeeUI(){
		main = new JFrame("사원관리");
		dialog = new MessageDialog(main);
		insertBt = new JButton("등록");			updateBt = new JButton("수정");
		deleteBt = new JButton("삭제");			findBt = new JButton("검색");
		exitBt = new JButton("종료");
		clearBt = new JButton("clear");
		
		empnoTf = new JTextField();				nameTf= new JTextField();
		salaryTf = new JTextField();			addressTf = new JTextField();		
		
		JLabel empnoL = new JLabel("사원번호");			
		JLabel nameL = new JLabel("사원이름");
		JLabel salaryL = new JLabel("급    여");			
		JLabel addressL = new JLabel("주    소");			
		JLabel listL = new JLabel("사원 목록", JLabel.CENTER);
		JLabel mgrL = new JLabel("사원정보 관리",JLabel.CENTER);
		
		CheckboxGroup  search = new CheckboxGroup();
		
		empModel = new DefaultTableModel(header, 10);
		empTable = new JTable(empModel);
		empPan = new JScrollPane(empTable);

		insertBt.addActionListener(buttonHandler) ;
		findBt.addActionListener(buttonHandler);
		deleteBt.addActionListener(buttonHandler);
		updateBt.addActionListener(buttonHandler);
		exitBt.addActionListener(buttonHandler);
		clearBt.addActionListener(buttonHandler);
		empTable.addMouseListener(listHandler);
		JPanel updatePan = new JPanel(new GridLayout(8,1,5,5));  //���� ȭ���� ���� Pan
		JPanel checkPan = new JPanel(new GridLayout(1,3,5,5));
		JPanel empnoPan = new JPanel(new GridLayout(1,2,5,5));
		JPanel enamePan = new JPanel(new GridLayout(1,2,5,5));
		JPanel salPan = new JPanel(new GridLayout(1,2,5,5));
		JPanel addressPan = new JPanel(new GridLayout(1,2,5,5));
		addInfoPan = new JPanel(new GridLayout(1,2,5,5));
		JPanel buttonPan1 = new JPanel(new GridLayout(1,3,5,5));
		JPanel buttonPan2 = new JPanel(new GridLayout(1,3,5,5));
		
		empnoPan.add(empnoL);
		empnoPan.add(empnoTf);
		enamePan.add(nameL);			
		enamePan.add(nameTf);
		salPan.add(salaryL);			
		salPan.add(salaryTf);
		addressPan.add(addressL);			
		addressPan.add(addressTf);
		
		buttonPan1.add(insertBt);		
		buttonPan1.add(updateBt);
		buttonPan1.add(deleteBt);		
		buttonPan2.add(findBt);
		buttonPan2.add(clearBt);
		buttonPan2.add(exitBt);
		
		
		updatePan.add(checkPan);			
		updatePan.add(empnoPan);		
		updatePan.add(enamePan);
		updatePan.add(salPan);
		updatePan.add(addressPan);
		updatePan.add(addInfoPan);
		updatePan.add(buttonPan1);
		updatePan.add(buttonPan2);
		
		
		JPanel left = new JPanel(new BorderLayout(5,5));
		left.add(mgrL,"North");			left.add(updatePan,"Center");
		
		JPanel right = new JPanel(new BorderLayout(5, 5));
		right.add(listL,"North");			right.add(empPan);
		
		main.setLayout(new GridLayout(1, 2, 5, 5));
		main.add(left);					main.add(right);
		main.setSize(650, 300);
		main.setVisible(true);
		main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void showList(){
		List<Employee> list = model.searchAll();
		if(list!=null){
			String[][]data = new String[list.size()][4];
			for (int i=0; i<list.size(); i++){
				Employee emp = list.get(i);
				data[i][0]  = emp.getEmpNo();
				data[i][1]  = emp.getEname();
				data[i][2]  = ""+emp.getSalary();
				data[i][3]  = ""+emp.getAddress();
			}
			empModel.setDataVector(data, header);
		}
	}
}


















