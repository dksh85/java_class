package com.kdn.wjbasic01;

public class Brokerage {
	public static void buyAndSell(int price){
		double result;
		if(price < 50000000) {
			result = 0.006 * price; 
			if(result > 250000) result = 250000;
		}
		else if(price < 200000000){  
			result = 0.005 * price;
			if(result > 800000) result = 800000;
		}
		else if(price < 600000000) result = 0.004 * price;
		else result = 0.009* price;
		
		System.out.printf("수수료 : %d\n", (int)result);
	}
	
	public static void rent(int price){
		double result;
		if(price < 20000000){
			result = 0.005 * price;
			if (result > 70000)
				result = 70000;
		}
		else if(price < 50000000){ 
			result = 0.005 * price;
			if (result > 200000) result = 200000;
				
		}
		else if(price < 100000000){ 
			result = 0.004 * price;
			if (result > 300000) result = 3000000;
		}
		else if(price < 300000000) result = 0.003 * price;
		else result = 0.008 * price;

		System.out.printf("수수료 : %d\n", (int)result);
	}
	public static void main(String[] args) {
		System.out.println("주택 매매 : 600000000");
		buyAndSell(600000000);
		System.out.println("주택 임대: 900000000");
		rent(90000000);
	}
}