package com.kdn.wjbasic01;

public class Prime {
	public static void main(String[] args) {
		int count_num = 0;
		System.out.printf("소수 : ");

		for(int i = 1; i <= 100; i++){
			int count = 0;
			for(int j = 1; j <= i; j++){
				if (i % j ==0 ) count++;
			}

			if(count == 2) {
				System.out.printf("%d ", i);
				count_num++;
			}
		}
		System.out.println();
		System.out.println("소수는 " + count_num + "개");
	}
}
