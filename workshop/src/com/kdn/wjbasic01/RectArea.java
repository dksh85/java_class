package com.kdn.wjbasic01;

public class RectArea {
	public static void main(String[] args) {
		int facetWidth=5;
		System.out.printf("정사각형 넓이 = %d", facetWidth* facetWidth);
		
		float a = 3.14f;
		System.out.println("정사각형 넓이 = " + a );

		int b = (int)(Math.random() * 100);
		System.out.println("정사각형 넓이 = " + b );

	}
}