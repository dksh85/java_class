package com.kdn.wjbasic02;

public class BookTest {
	public static void main(String[] args) {
		Book b1 = new Book();
		Book b2 = new Book();
		Magazine m = new Magazine();
		
		b1.setIsbn(21424);
		b2.setIsbn(35355);
		m.setIsbn(35535);

		b1.setTitle("Java Pro");
		b2.setTitle("OOAD 분석, 설계");
		m.setTitle("Java World");

		b1.setAuthor("김하나");
		b2.setAuthor("소나무");
		m.setAuthor("편집부");

		b1.setPublisher("Jaen.kr");
		b2.setPublisher("Jaen.kr");
		m.setPublisher("androidjava.com");
		
		b1.setPrice(15000);
		b2.setPrice(30000);
		m.setPrice(7000);
		
		b1.setDesc("Java 기본 문법");
		b2.setDesc("");
		m.setDesc("");
		
		m.setYear(2013);
		m.setMonth(2);
		
		System.out.println( "********************** 도서 목록 ********************************");
		System.out.println(b1.toString()); 		
		System.out.println(b2.toString()); 
		System.out.println(m.toString()); 

	}
}
