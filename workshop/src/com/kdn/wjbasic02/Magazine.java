package com.kdn.wjbasic02;

public class Magazine {
	private int isbn;
	private String title;
	private String author;
	private String publisher;
	private int year;
	private int month;
	private int price;
	private String desc;
	
	public Magazine(){}
	
	
	
	public int getIsbn() {
		return isbn;
	}



	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getAuthor() {
		return author;
	}



	public void setAuthor(String author) {
		this.author = author;
	}



	public String getPublisher() {
		return publisher;
	}



	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}



	public int getYear() {
		return year;
	}



	public void setYear(int year) {
		this.year = year;
	}



	public int getMonth() {
		return month;
	}



	public void setMonth(int month) {
		if (month < 1 && month > 12){
			System.out.println("잘못된 값입니다. 1월 부터 12월 사이로 설정하세요");
		}
		else this.month = month;
	}



	public int getPrice() {
		return price;
	}



	public void setPrice(int price) {
		if (price < 0)
			System.out.println("잘못된 값입니다.");
		else this.price = price;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}



	public String toString(){
		return isbn + "\t | " + title 
				+ "\t | " + author + "\t | " + publisher
				+ "\t | "+ price +  " | " + desc
				+ "\t | "+ year + "." + month;
	}	
}
