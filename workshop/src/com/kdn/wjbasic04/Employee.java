package com.kdn.wjbasic04;

public class Employee {
	private String empNo;
	private String ename;
	private String address;
	private int salary;
	
	public Employee(){}

	public Employee(String empNo, String ename, String address, int salary) {
		super();
		this.empNo = empNo;
		this.ename = ename;
		this.address = address;
		this.salary = salary;
	}

	public String toString() {
		return "empNo=" + empNo + ", ename=" + ename + ", address=" + address + ", salary=" + salary;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	

}
