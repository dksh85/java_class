package com.kdn.wjbasic04;

public class EmployeeMgr {
	private Employee[] employeeList;
	private int employeeIndex;
				
	public EmployeeMgr(){
		employeeList= new Employee[10];
		employeeIndex = 0;
	}
	private int findIndex(String empNo){
		for(int i = 0; i < employeeIndex; i++){
			if((employeeIndex != 0) && (employeeList[i].getEmpNo()).equals(empNo)){
				return i;
			}
		}
		return -1;
	}
	
	public Employee findEmployee(String empNo){
		return employeeList[findIndex(empNo)];
	}
	
	public void add(Employee emp){
		if(emp != null){
			if(findIndex(emp.getEmpNo()) == -1){
				if(employeeIndex >= employeeList.length){
					Employee[] temp = new Employee[employeeList.length + 10];
					System.arraycopy(employeeList, 0, temp, 0, employeeList.length);
					employeeList = temp;
				}
				employeeList[employeeIndex++] = emp;
			}
			else System.out.println("이미등록된 직원입니다");
		}
	}
	
	public void printAll(){
		for(int i =0; i < employeeIndex; i++){
			System.out.println(employeeList[i].toString());
		}
	}
	
	public void printManager(){
		for(int i =0; i < employeeIndex; i++){
			if(employeeList[i] instanceof Manager){
				System.out.println(employeeList[i].toString());
			}
		}
	}
	
	public void printEngineer(){
		for(int i =0; i < employeeIndex; i++){
			if(employeeList[i] instanceof Engineer){
				System.out.println(employeeList[i].toString());
			}
		}
	}

	public void printEmployee(){
		for(int i =0; i < employeeIndex; i++){
			if(!(employeeList[i] instanceof Manager) &&
					!(employeeList[i] instanceof Engineer)){
				System.out.println(employeeList[i].toString());
			}
		}
	}

	public void update(Employee emp){
		int empI = findIndex(emp.getEmpNo());
		if(empI != -1)
			employeeList[empI] = emp;
		else System.out.println("사원없음");
	}
	
	public void delete(String empNo){
		int empI = findIndex(empNo);
		if(empI != -1){
			employeeList[empI] = null;
			employeeList[empI] = employeeList[employeeIndex-1];
			employeeIndex--;
		}
	}
}
