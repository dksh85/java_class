package com.kdn.wjbasic04;

public class EmployeeTest {
	public static void main(String[] args) {
		EmployeeMgr mgr = new EmployeeMgr();
		mgr.add(new Employee("emp1", "kdn","나주시", 3000));
		mgr.add(new Employee("emp1", "홍길동","제주시", 3000));
		mgr.add(new Manager("emp2", "관리중","서울시", 7000,"ceo"));
		mgr.add(new Engineer("emp3", "kdg","서울시", 5000,"java"));
		mgr.add(new Employee("emp4", "작성해","부산", 3000));
		mgr.add(new Engineer("emp5", "알파고","미국", 5000,"인고지능 바둑"));
		mgr.add(new Employee("emp6", "길동홍","서울", 3000));
		System.out.println("=================등록된 모든 직원 정보 리스트 ====================");
		mgr.printAll();
		System.out.println("=================등록된 관리 직원 정보 리스트 ====================");
		mgr.printManager();
		System.out.println("=================등록된 기술 직원 정보 리스트 ====================");
		mgr.printEngineer();
		System.out.println("=================등록된 사무 직원 정보 리스트 ====================");
		mgr.printEmployee();
		System.out.println("=================사원 찾기 ====================");
		System.out.println("emp1 정보 :"+mgr.findEmployee("emp1"));
		System.out.println("emp2 정보 :"+mgr.findEmployee("emp2"));
		System.out.println("=================사원 정보 수정 ====================");
		mgr.update(new Engineer("emp3", "kdg","서울시 성북구", 5000,"java,db,web"));
		System.out.println("emp3의 수정 된 정보 :"+mgr.findEmployee("emp3"));
		System.out.println("=================사원 정보 삭제 ====================");
		mgr.delete("emp3");
		mgr.printAll();
	}
}






