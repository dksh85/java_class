package com.kdn.wjbasic04;

public class Engineer extends Employee{
	private String skill;

	public Engineer() {
		super();
	}

	public Engineer(String empNo, String ename, String address, int salary, String skill) {
		super(empNo, ename, address, salary);
		this.skill = skill;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	@Override
	public String toString() {
		return super.toString() + ", skill = " + skill;
	}

	@Override
	public String getEmpNo() {
		return super.getEmpNo();
	}

	@Override
	public void setEmpNo(String empNo) {
		super.setEmpNo(empNo);
	}

	@Override
	public String getEname() {
		return super.getEname();
	}

	@Override
	public void setEname(String ename) {
		super.setEname(ename);
	}

	@Override
	public String getAddress() {
		return super.getAddress();
	}

	@Override
	public void setAddress(String address) {
		super.setAddress(address);
	}

	@Override
	public int getSalary() {
		return super.getSalary();
	}

	@Override
	public void setSalary(int salary) {
		super.setSalary(salary);
	}
	
	
}
