package com.kdn.wjbasic04;

public class Manager extends Employee{
	private String position;

	public Manager() {
		super();
	}

	public Manager(String empNo, String ename, String address, int salary, String position) {
		super(empNo, ename, address, salary);
		this.position = position;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return super.toString() + ", position = " + position;
	}

	@Override
	public String getEmpNo() {
		return super.getEmpNo();
	}

	@Override
	public void setEmpNo(String empNo) {
		super.setEmpNo(empNo);
	}

	@Override
	public String getEname() {
		return super.getEname();
	}

	@Override
	public void setEname(String ename) {
		super.setEname(ename);
	}

	@Override
	public String getAddress() {
		return super.getAddress();
	}

	@Override
	public void setAddress(String address) {
		super.setAddress(address);
	}

	@Override
	public int getSalary() {
		return super.getSalary();
	}

	@Override
	public void setSalary(int salary) {
		super.setSalary(salary);
	}
	
	
	
	
}
