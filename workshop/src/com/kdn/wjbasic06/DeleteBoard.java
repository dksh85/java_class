package com.kdn.wjbasic06;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DeleteBoard {
	public static void main(String[] args) {
		Connection con = null;
		PreparedStatement stmt = null;
		
		try {
			con = DBUtil.getConnection();
			String sql = " delete from board "
					+ " where no =? ";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, 5);

			// executeUpdate() : insert, update, delete문을 수행하는 메서드
			int rows = stmt.executeUpdate(); 
			if(rows < 1){
				System.out.println("db delete 실패");
			}else{
				System.out.println("db delete 성공");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			DBUtil.close(stmt);
			DBUtil.close(con);
		}
	}
}
