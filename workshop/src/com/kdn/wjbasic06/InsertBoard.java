package com.kdn.wjbasic06;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class InsertBoard {
	public static void main(String[] args) {
		Connection con = null;
		PreparedStatement stmt = null;
		
		PreparedStatement stmtc = null;
		
		try{
			con = DBUtil.getConnection();
			
			String sql = " insert into board(no, id, title, regdate, contents)"
						+ " values (board_no.nextval,?,?,sysdate,?) ";
			stmt = con.prepareStatement(sql);

			stmt.setString(1,"처음 입력하다");
			stmt.setString(2,"kdn");
			stmt.setString(3,"JDBC 첫번째 연습");
			
			int rows = stmt.executeUpdate(); 
			if(rows < 1){
				System.out.println("db insert 실패");
			}else{
				System.out.println("db insert 성공");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		finally{
			DBUtil.close(stmt);
			DBUtil.close(con);
		}
	}
}
