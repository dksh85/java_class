package com.kdn.wjbasic06;

import java.sql.*;
public class SearchBoard {
	public static void main(String[] args) {
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try{
			con = DBUtil.getConnection(); // DB 서버에 연결
			//쿼리를 수행할 객체 생성
			stmt = con.prepareStatement(" select * from board where no = ? " );
			stmt.setInt(1, 5);
			
			rs = stmt.executeQuery(); // 쿼리 수행
			System.out.println("no\tid\ttitle\tregdate\tcontents");
			while(rs.next()){
				int no = rs.getInt("no");
				String id = rs.getString("id");
				String title = rs.getString("title");
				String regdate = rs.getString("regdate");
				String contents = rs.getString("contents");
				
				System.out.printf("%d\t%s\t%s\t\t%s\t%s\n", no, id, title, regdate, contents);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			DBUtil.close(rs);
			DBUtil.close(stmt);
			DBUtil.close(con);
		}
	}
}
