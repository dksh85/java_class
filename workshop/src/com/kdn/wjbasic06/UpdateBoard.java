package com.kdn.wjbasic06;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UpdateBoard {
	public static void main(String[] args) {
		Connection con = null;
		PreparedStatement stmt = null;
		
		try {
			con = DBUtil.getConnection();
			String sql = " update board set title = ? "
					+ " where no =? ";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, "수정됨");		//두번째 ?에 데이터로 setting
			stmt.setInt(2, 5);

			// executeUpdate() : insert, update, delete문을 수행하는 메서드
			int rows = stmt.executeUpdate(); 
			if(rows < 1){
				System.out.println("db update 실패");
			}else{
				System.out.println("db update 성공");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			DBUtil.close(stmt);
			DBUtil.close(con);
		}
	}
}
